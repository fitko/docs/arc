import translations from '../../static/locales/de/common.json'

const useTranslation = () => {
    const translate = (translationKey: any): string => {
        const keys = translationKey.split('.')
        let translation: any = translations

        for (const key of keys) {
            translation = translation?.[key] ?? translationKey
        }
        if (translation == translationKey) {
            console.log(`Warning: i18n - Translation ${translationKey} not found.`)
        }

        return translation
    }

    return { translate }
}

export default useTranslation
