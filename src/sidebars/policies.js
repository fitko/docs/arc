module.exports = {
  policiesSidebar: [
    {
      type: 'category',
      label: 'Vorgaben / Richtlinien',
      link: {
        type: 'doc',
        id: 'policies/index',
      },
      items: [
        'policies/barrierefreie-informationstechnikverordnung',
        'policies/eckpunkte-umsetzung-ozg',
        'policies/efa-mindestanforderungen',
        'policies/efa-mindestanforderungen-betrieb',
        'policies/foederale-it-architekturrichtlinien',
        'policies/informationssicherheitsleitlinie',
        'policies/ozg',
        'policies/servicestandard-ozg-umsetzung',
        'policies/verwaltungsabkommen-ozg'
      ]
    },
  ]
}