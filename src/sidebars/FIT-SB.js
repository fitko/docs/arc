module.exports = {
    sbSidebar: [
        'fit-sb/index',
        {
            type: 'category',
            label: 'FIT-SB-Leitfaden',
            link: {
                type: 'doc',
                id: 'fit-sb/processes/index',
            },
            items: [
                {
                    type: 'category',
                    label: 'Grundlegendes',
                    link: {
                        type: 'doc',
                        id: 'fit-sb/processes/basics/index',
                    },
                    items: [
                        'fit-sb/processes/basics/specials',
                        'fit-sb/processes/basics/basics_sidebar',
                        {
                            type: 'category',
                            label: 'Artefakte',
                            link: {
                                type: 'doc',
                                id: 'fit-sb/processes/basics/artefacts/index',
                            },
                            items: [
                                'fit-sb/processes/basics/artefacts/save', 
                                'fit-sb/processes/basics/artefacts/find',
                            ]
                        },
                        'fit-sb/processes/basics/openDesk/index',
                    ]
                },        
                {
                type: 'category',
                label: 'Für FIT-SB-Mitglieder',
                link: {
                    type: 'doc',
                    id: 'fit-sb/processes/members/index',
                },
                items: [
                    'fit-sb/processes/members/first_steps',
                    {
                        type: 'category',
                        label: 'FIT-SB-Sitzungen',
                        link: {
                            type: 'doc',
                            id: 'fit-sb/processes/members/meetings/index',
                        },
                        items: [
                            'fit-sb/processes/members/meetings/submit',
                            'fit-sb/processes/members/meetings/add', 
                            'fit-sb/processes/members/meetings/discuss',
                            'fit-sb/processes/members/meetings/working-packages', 
                        ]
                    },
                    /* 'fit-sb/processes/members/standards/index', */
                    /*
                    {
                        type: 'category',
                        label: 'FIT-Standards',
                        link: {
                            type: 'doc',
                            id: 'fit-sb/processes/members/standards/index',
                        },
                        items: [
                            'fit-sb/processes/members/standards/lifecycle',
                        ]
                    },
                    */
                ]
            },
            {
                type: 'category',
                label: 'Für die FIT-SB-Geschäftsstelle',
                link: {
                    type: 'doc',
                    id: 'fit-sb/processes/gs/index',
                },
                items: [
                    // 'fit-sb/processes/gs/incoming_requests',
                    {
                        type: 'category',
                        label: 'FIT-SB-Sitzungen',
                        link: {
                            type: 'doc',
                            id: 'fit-sb/processes/gs/meetings/index',
                        },
                        items: [
                            'fit-sb/processes/gs/meetings/prepare', 
                            'fit-sb/processes/gs/meetings/conduct', 
                            'fit-sb/processes/gs/meetings/follow-up_work'
                        ]
                    },
                    'fit-sb/processes/gs/fit-standards',
    
                ]
            },
            {
                type: 'category',
                label: 'Anwendungsszenarien',
                link: {
                    type: 'doc',
                    id: 'fit-sb/processes/examples/index',
                },
                items: [
                    'fit-sb/processes/examples/info_sb-sitzung',
                    'fit-sb/processes/examples/info_themenfeld',
                ]
            },
            ]
        },
        'fit-sb/index_fit_standards',
        {
            type: 'category',
            label: 'Glossar',
            link: {
                type: 'doc',
                id: 'fit-sb/glossary',
            },
            items: [
              {
                "type": "link",
                "href": "/fit-sb/glossary#anforderungsmanagement",
                "label": "Anforderungsmanagement"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#aufgabenmanagement",
                  "label": "Aufgabenmanagement"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#bedarfsmelder",
                  "label": "Bedarfsmelder"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#bedarfstraeger",
                  "label": "Bedarfsträger"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#bedarfsvertreter",
                  "label": "Bedarfsvertreter"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#betreiber-eines-foederalen-it-standards",
                  "label": "Betreiber eines Föderalen IT-Standards"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#betriebskonzept",
                  "label": "Betriebskonzept"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#definition-of-done",
                  "label": "Definition of Done"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#defintion-of-ready",
                  "label": "Defintion of Ready"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#dod",
                  "label": "DoD"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#dor",
                  "label": "DoR"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#fit-ab",
                  "label": "FIT-AB"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#fit-sb",
                  "label": "FIT-SB"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#fit-standard",
                  "label": "FIT-Standard"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#fitko",
                  "label": "FITKO"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#foederale-it-kooperation",
                  "label": "Föderale IT-Kooperation"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#foederale-it-standards",
                  "label": "Föderale IT-Standards"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#foederales-entwicklungsportal",
                  "label": "Föderales Entwicklungsportal"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#foederales-it-architekturboard",
                  "label": "Föderales IT Architekturboard"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#it-architekturboard",
                  "label": "IT-Architekturboard"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#it-plr",
                  "label": "IT-PLR"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#it-planungsrat",
                  "label": "IT-Planungsrat"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#it-staatsvertrag",
                  "label": "IT-Staatsvertrag"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#informationsplattform",
                  "label": "Informationsplattform"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#lebenszyklus-eines-it-standards",
                  "label": "Lebenszyklus eines IT-Standards"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#ready-for-decommissioning",
                  "label": "Ready for Decommissioning"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#ready-for-service",
                  "label": "Ready for Service"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#rfd",
                  "label": "RfD"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#rfs",
                  "label": "RfS"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#stdag",
                  "label": "STDAG"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#stdag-prozess",
                  "label": "STDAG-Prozess"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#stakeholdermanagement",
                  "label": "Stakeholdermanagement"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#standardisierungsagenda",
                  "label": "Standardisierungsagenda"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#standardisierungsbedarf",
                  "label": "Standardisierungsbedarf"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#standardisierungsboard",
                  "label": "Standardisierungsboard"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#taskmanagement",
                  "label": "Taskmanagement"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#transitionsplan",
                  "label": "Transitionsplan"
              },
              {
                  "type": "link",
                  "href": "/fit-sb/glossary#umsetzungsteam",
                  "label": "Umsetzungsteam"
              }
            ]
        },
     
    
        'fit-sb/changelog',
    ],
}