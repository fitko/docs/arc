module.exports = {
  abSidebar: [
    'fit-ab/index',
    {
        type: 'category',
        label: 'Rahmenbedingungen',
        link: {
          type: 'doc',
          id: 'fit-ab/rahmenbedingungen/index',
        },
        items: [
          'fit-ab/rahmenbedingungen/handlungsrahmen', 
          'fit-ab/rahmenbedingungen/architectural-relevance', 
          'fit-ab/rahmenbedingungen/zusammensetzung',
            {
                type: 'category',
                label: 'Aufgaben und Arbeitsformate',
                link: {
                    type: 'doc',
                    id: 'fit-ab/rahmenbedingungen/aufgaben_und_arbeitsformate/index',
                },
                items: [
                    'fit-ab/rahmenbedingungen/aufgaben_und_arbeitsformate/aufgaben_fit-am',
                    'fit-ab/rahmenbedingungen/aufgaben_und_arbeitsformate/arbeitsformate_fit-am',
                    {
                        type: 'category',
                        label: 'FIT-AGs',
                        link: {
                            type: 'doc',
                            id: 'fit-ab/rahmenbedingungen/aufgaben_und_arbeitsformate/fit-ags/index',
                        },
                        items: [
                            'fit-ab/rahmenbedingungen/aufgaben_und_arbeitsformate/fit-ags/prozess_planung',
                            'fit-ab/rahmenbedingungen/aufgaben_und_arbeitsformate/fit-ags/prozess_steuerung'
                        ]
                    }
                ]
            }
        ]
    },
    'fit-ab/arbeitsergebnisse',
    {
        type: 'category',
        label: 'Sitzungen',
        link: {
          type: 'doc',
          id: 'fit-ab/sitzungen/index',
        },
        items: [
          'fit-ab/sitzungen/top',
          'fit-ab/sitzungen/sitzungstermine'
        ]
    },
    'fit-ab/beschluesse',
    'fit-ab/undertakings',
    'fit-ab/kontakt'
  ]
}
