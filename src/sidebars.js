const policiesSidebar = require('./sidebars/policies');
const abSidebar = require('./sidebars/FIT-AB');
const sbSidebar = require('./sidebars/FIT-SB');

module.exports = {
  ...policiesSidebar,
  ...abSidebar,
  ...sbSidebar,
};