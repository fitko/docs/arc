/** @type {import('@docusaurus/types').DocusaurusConfig} */

const baseUrl = process.env.DOCUSAURUS_BASE_URL || '/'
const gitBranch = process.env.GIT_BRANCH || 'main'
const gitlabUrl = process.env.GITLAB_SERVER_URL || 'https://gitlab.opencode.de'
const gitlabProjectPath = process.env.GITLAB_PROJECT_PATH


module.exports = {
  title: 'Dokumentation zur Föderalen IT',
  url: 'https://docs.fitko.de',
  baseUrl,
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'throw',
  favicon: 'favicon.png',
  customFields: {
    feedbackUrl: `${gitlabUrl}/${gitlabProjectPath}/-/issues`,
    sourceCodeUrl: `${gitlabUrl}/${gitlabProjectPath}`,
  },
  i18n: {
    defaultLocale: 'de',
    locales: ['de'],
  },
  themes: ['@docusaurus/theme-mermaid'],
  markdown: {
    mermaid: true,
  },
  themeConfig: {
    docs: {
      sidebar: {
        hideable: true,
      },
    },
    mermaid: {
      options: {
        startOnLoad: true,
        sequence: { showSequenceNumbers: true },
        theme: 'base',
        themeVariables: {
          textColor: 'rgb(0,0,0)',
          primaryBorderColor: 'rgb(87,87,87)',
          lineColor: 'rgb(87,87,87)',
          mainBkg: 'rgb(255,200,25)',
          sequenceNumberColor: 'rgb(255,255,255)',
        },
      },
    },
    colorMode: {
      disableSwitch: false,
    },
    navbar: {
      title: 'Dokumentation zur Föderalen IT',
      items: [
        {to: 'policies/', label: 'Vorgaben/Richtlinien', position: 'left'},
        {to: 'fit-ab/', label: 'FIT-Architekturboard', position: 'left'},
        {to: 'fit-sb/', label: 'FIT-Standardisierungsboard', position: 'left'},
      ],
    },
    footer: {
      style: 'light',
      copyright: `Diese Seite ist Teil des <a href="https://docs.fitko.de/">Föderalen Entwicklungsportals</a>. Verantwortlich für die Inhalte der Seite sind die jeweiligen Autoren. Wenn nicht anders vermerkt, sind die Inhalte dieser Webseite lizenziert unter der <a href="https://creativecommons.org/licenses/by/4.0/deed.de">Creative Commons Namensnennung 4.0 International Public License (CC BY 4.0)</a>. Die technische Infrastruktur wird betrieben durch die FITKO. Es gilt das <a href="https://www.fitko.de/impressum">Impressum der FITKO</a> und die <a href="https://fitko.de/datenschutz">Datenschutzerklärung der FITKO</a> mit der Maßgabe, dass kein Tracking durchgeführt wird und keine Cookies gesetzt werden.`,
    },
  },
  plugins: [
    '@cmfcmf/docusaurus-search-local',
    'docusaurus-plugin-sass',
    async function myPlugin(context, options) {
      return {
        name: "docusaurus-tailwindcss",
        configurePostCss(postcssOptions) {
          // Appends TailwindCSS and AutoPrefixer.
          postcssOptions.plugins.push(require("tailwindcss"));
          postcssOptions.plugins.push(require("autoprefixer"));
          return postcssOptions;
        },
      };
    },
  ],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // id: 'arc', // omitted => default instance
          sidebarPath: require.resolve('./src/sidebars.js'),
          editUrl: ({ version, versionDocsDirPath, docPath }) =>
            `${gitlabUrl}/-/ide/project/${gitlabProjectPath}/edit/${gitBranch}/-/${versionDocsDirPath}/${docPath}`,
          routeBasePath: '/',
          breadcrumbs: false,
        },
        theme: {
          customCss: [
            require.resolve('./src/css/custom.scss'),
            require.resolve('./src/css/fit-sb.css'),
          ],
        },
      },
    ],
  ],
}
