---
title: Änderungshistorie
toc_max_heading_level: 4
---

Alle wesentlichen Änderungen an dieser Online-Dokumentation werden auf dieser Seite dokumentiert.

Das Format dieser Datei basiert auf [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## 2025-02-14

### Hinzugefügt

- Arbeitspaketansichten zur besseren Übersicht in der Dokumentation beschrieben, einschließlich der Ansichten **01-Aktuelle FIT-SB-Sitzung**, **02-Alle offenen FIT-SB-Sitzungsthemen** und **03-Alle FIT-SB-Sitzungsthemen**.
- Detaillierte Beschreibungen der Sitzungsphasen (Vorbereitung, Durchführung, Nachbereitung) aus Sicht der Geschäftsstelle hinzugefügt.
- Neue Screenshots der Arbeitspaketansichten ergänzt und in der Dokumentation verlinkt.
- Erweiterte Erklärungen zur Pflege der FIT-Standards und deren Übertragung an die Informationsplattform.

### Geändert

- Dokumentation der FIT-Standards überarbeitet, inklusive aktualisierter Links zum Projekt **05_FIT-Standards** und klarerer Prozessbeschreibung.
- Überarbeitung der Checklisten und Prozessschritte für die Sitzungsvorbereitung (z.B. Agendaerstellung) und -durchführung.
- Anpassungen der Timeline-SVG-Grafiken für verbesserte Darstellung in Dark-Mode-Umgebungen.
- Textliche Präzisierungen und Formatierungsanpassungen in mehreren Prozessbeschreibungen.

## 2025-02-11

### Geändert

- Checklisten „FIT-SB-Sitzungen“ für FIT-SB-GS aktualisiert (Vorbereitung, Durchführung, Nachbereitung).  

## 2025-01-28

### Hinzugefügt

- Beschreibung „Sitzungsunterlagen hinzufügen“ ergänzt

## 2025-01-20

### Hinzugefügt

- Beschreibungen „Projektübersicht“ ergänzt
- Ergänzungen wegen Auslagerung FIT-Standards in separates Projekt

### Geändert

- Anpassungen wegen openDesk V1.0 (z. B. Screenshots)
- Textanpassungen „FIT-SB-Sitzungen“

## 2024-11-14

### Initiale Version veröffentlicht

<!--

## [Unveröffentlicht]

### Hinzugefügt
- ...

### Geändert

- ...

### Gefixt

- ...

### Entfernt

- ...

-->
