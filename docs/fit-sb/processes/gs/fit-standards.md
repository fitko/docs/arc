---
title: FIT-Standards
---

Die FIT-SB-Geschäftsstelle ist für die Pflege der FIT-Standards im Projekt [**05_FIT-Standards**](https://project.fitko.opendesk.live/projects/12/) sowie die Übertragung der dort hinterlegten Informationen an die [Informationsplattform](https://docs.fitko.de/fit-standards/) verantwortlich.

Ausführliche Informationen zum Projekt **05_FIT-Standards** finden Sie [auf dieser Seite](../../index_fit_standards).  
Dazu gehören

- die spezifischen Arbeitspakettypen für FIT-Standards und deren Organisation
- die im Projekt **05_FIT-Standards** verfügbaren Boards und Arbeitspaketlisten sowie deren Anwendungsgebiete.


Für die Übertragung der im Projekt **05_FIT-Standards** hinterlegten Daten an die Informationsplattform steht ein entsprechender Mechanismus zur Verfügung, der auf [dieser Wiki-Seite](https://gitlab.opencode.de/fitko/docs/portal/-/wikis/OpenProject-Infoplattform/Aktualisierung-der-Informationsplattform) des betreffenden openCode-Projekts detailliert beschrieben ist.
