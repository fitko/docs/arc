---
title: Bearbeitung eingehender Anfragen
---

:::note[Seite im Aufbau 🚧]

Wir arbeiten für Sie daran, diese Seite mit aktuellen Informationen zu füllen. Bitte schauen Sie später noch einmal vorbei, um weitere Inhalte zu sehen.  
Vielen Dank für Ihre Geduld!

:::

## Einführung

Auf dieser Seite geht es um das **Standardisierungsmanagement**, konkret den Umgang der FIT-SB-Geschäftsstelle (GS) mit eingehenden Anfragen aller Art.

Die nachfolgende Abbildung vermittelt einen Überblick über den gesamten Prozess:

[![Überblick Prozess](/images/fit-standardisierungsboard/proc-incoming-req1.png)](/images/fit-standardisierungsboard/proc-incoming-req1.png)

Wie man bereits an der Abbildung erkennen kann, folgt die Bearbeitung einzelnen Prozessschritten. Die Prozessschritte lauten im einzelnen:

1. [Eingang](#eingang-einer-anfrage)
1. [Klassifizieren und Bewerten](#klassifizieren-bewerten-priorisieren)
1. [Bearbeiten](#bearbeiten)
1. [Anfrage abschließen](#anfrage-abschließen)
1. [Information bereitstellen](#information-bereitstellen)

Die Inhalte und die Vorgehensweise bei der Bearbeitung der einzelnen Prozessschritte ist nachfolgend beschrieben.

## Eingang einer Anfrage

Anfragen erreichen die GS auf verschiedenen Wegen:

* Per E-Mail
* Per Telefonanruf (Telefonnotiz)
* Durch Zuruf (Gesprächsnotiz)

In allen Fällen erfolgt zunächst eine Sichtung der Anliegens durch die GS.

### Prüfung auf Bagatelle

Die GS nimmt zunächst eine Prüfung auf eine //Bagatelle// vor. Hierzu wird noch keine Aufgabe angelegt. Als Bagetelle werden Anfragen behandelt, welche

* mit einem Link oder der Benennung eines Ansprechpartners beantwortet werden können.
* nicht für Andere relevant sind.
* nicht häufiger auftreten
* in sehr kurzer Zeit bearbeitet werden können.

Sind alle vier Bedingungen erfüllt, handelt es sich um eine Bagatelle. In diesem Fall wird das Anliegen erledigt, ohne ein Aufgabenpaket zu erstellen. Der Prozess ist damit beendet.

## Arbeitspaket anlegen

Handelt es sich __nicht__ um eine Bagatelle, wird in OpenProject (OP) ein Arbeitspaket angelegt. Hierbei sind neben dem Ausfüllen der Pflichtfelder auch geeignete Schlagworte zu setzen, um das spätere Auffinden zu erleichtern.

### E-Mail und Anhänge in Nextcloud speichern

Vorhandene Artefakte (z. B. E-Mail, Dateianlagen) werden in Nextcloud (NC) gespeichert. Hierbei sind ebenfalls Schlagworte zuzuweisen.

Abschließend sind die in Nextcloud gespeicherten Artefakte mit dem in OP erstellten Aufgabenpaket zu verknüpfen (Reiter **Dateien** > Abschnitt **Nextcloud** > **Vorhandene Dateien verknüpfen**)

## Klassifizieren, bewerten, priorisieren

Der Prozessschritt Bewerten und Klassifizieren dient der Organisation der Bearbeitung und der Einordnung damit der Vorgang anschließend aufgefunden und mit anderen Vorgängen in Verbindung gebracht werden kann.

### Klassifizieren

In diesem Prozessschritt (siehe Abbildung unten) wird das eingegangene Artefakt zunächst darauf untersucht, welcher Klasse es zuzuordnen ist.

Die Klassifizierung und Führung des Vorgangs in der FITKO-weit vereinbarten Ordnerstruktur dient als Hilfsmittel zum Finden von Informationen unabhängig von einer globalen Suche.
Die Einteilung der Klassen wird regelmäßig auf Aktualität und Relevanz überprüft.

Derzeit sind folgende Klassen/Themenfeldern definiert:

* bekanntes (vorhandenes) oder neues Themenfeld
* Standard-übergreifende Themen
* Themen, die einen konkreten FIT-Standard betreffen
* Themen, die den Standardisierungsprozess betreffen
* Themen, die Aktivitäten der Geschäftsstelle des Standardisierungsboard betreffen. dies kann organisatorische Themen, wie die Organisation der Sitzungstermine, Protokolle oder Ähnliches betreffen.

[![Klassifizieren und Bewerten](/images/fit-standardisierungsboard/proc-incoming-req2.png)](/images/fit-standardisierungsboard/proc-incoming-req2.png)

#### Neues Themenfeld

Wird im Rahmen der Klassifierung erkannt, dass es sich um ein neues Themenfeld handelt, muss - zusätzlich zum bereits erstellten Arbeitspaket - noch ein entsprechendes Epic (FIT-SB) für dieses Themenfeld angelegt werden. Diesem Epic wird  das Epic 04_Themenfelder als Eltern-Element zugeordnet. 
Das neu erstellte Epic stellt gleichzeitig das ELtern-Element für das angelegte Arbeitspaket dar.

### Verschlagworten

Um die Suche nach definierten Schlagworten zu erleichtern, können in NC //Schlagworte// genutzt werden. OpenProject bietet dies aus Bordmitteln nicht an. Arbeitspakete vom Typ Aufgabe (FIT-SB) verfügen jedoch über ein Custom Field vom Typ //liste//. In dieser Liste sind geeignete Schlagworte definiert, aus denen ein oder mehrere Schlagworte selektiert und dem Arbeitspaket zugeordnet werden können.

Im Gegensatz zu der Situation in NC (Schlagworte sollten aus den vorhandenen ausgewählt werden, können jedoch beliebig vergeben werden) können Schlagworte in OP nur von Nutzern mit Admin-Rechten definiert werden.

Eine Verbindung der Schlagwortlisten, bzw. den vergebenen Schlagworten ist zwischen OP und NC nicht vorgesehen.

## Bewerten

Im Prozessabschnitt _Bewerten_ werden die eingehenden Anfragen nach folgenden Kriterien eingestuft:
* [Priorität](#priorisieren-von-aufgaben-und-epics-in-openproject)
* [Aufwand](#aufwand-abschätzen-über-den-gesamten-prozess)
* [Bearbeiter](#bearbeiter-festlegen)
* [Datenart](#daten)

### Priorisieren von Aufgaben und Epics in OpenProject

In OpenProject werden ausschließlich Aufgaben und Epics priorisiert; andere Objekte bleiben davon unberührt. Die Priorisierung erfolgt durch den Ersteller der Aufgabe oder des Epics, ggfs. in Abstimmung mit anderen.

Die Aufgaben werden anhand der Eisenhower-Matrix nach Wichtigkeit und Dringlichkeit bewertet:

* Wichtigkeit: Beitrag zur Zielerreichung, Abhängigkeit anderer Personen (Stufen: gering/mittel/hoch).
* Dringlichkeit: Erledigung in naher Zukunft nötig, vorhandene Deadline, Übertragbarkeit an andere möglich (Stufen: gering/mittel/hoch).

### Prioritätsstufen

Vier Prioritätsstufen ergeben sich aus den Bewertungen der Wichtigkeit und Dringlichkeit:

1. Niedrig
1. Normal
1. Hoch
1. Sofort

[![Prioritätenmatrix](/images/fit-standardisierungsboard/proc-incoming-req3.png)](/images/fit-standardisierungsboard/proc-incoming-req3.png)

Die Berechnung der Priorität erfolgt wie folgt:

* Priorität = Wichtigkeit x Dringlichkeit

In kritischen Fällen und öffentlichkeitswirksamen Vorgängen wird die Priorität automatisch auf „Sofort“ gesetzt.

Bei Bedarf können Wichtigkeit und Dringlichkeit unterschiedlich gewichtet werden bspw.:

* Priorität = Gewichtung x Wichtigkeit x Gewichtung x Dringlichkeit

Die Priorität wird manuell berechnet, um die Aufgabenmaske für den Anwender möglichst übersichtlich zu halten. Zudem wird davon ausgegangen, dass der Ersteller der Aufgabe in vielen Fällen die Priorität direkt selbst einschätzen kann.

#### Visuelle Darstellung/Ansichten

In OP-Arbeitspaketen sind die vier Prioritätsstufen im Pflichtfeld **Priorität** hinterlegt.

Neue Epics und Aufgaben werden standardmäßig mit der Priorität „Normal“ angelegt. Dies wird im Tagesgeschäft nur geändert, wenn nach der Bewertung eines Artefakts für deren Aufgabe/Epic eine andere Priorität festgestellt wird.

Während der Bearbeitung einer Aufgabe/eines Epics kann die Priorität jederzeit angepasst werden.

In den für das FIT-SB vorgeschlagenen Arbeitspaketlisten (Ansichten auf Arbeitspakete) wird die „Priorität“ als Spalte hinzugenommen.

Ein Board mit der Darstellung der Arbeitspakete nach Priorität wird für das FIT-SB im ersten Schritt nicht vorgesehen.

### Aufwand abschätzen (über den gesamten Prozess)

Dieser Prozessschritt  wird derzeit überspriungen. in den korrespondierenden Feldern in OP-Aufgabenpaketen werden keine EIngaben vorgenommen.

### Bearbeiter festlegen

In diesem Prozessschritt werden Verantwortliche und Bearbeiter zugewiesen. Gegebenenfalls sind weitere einzubindende Stellen zu informieren, beispielsweise FITKO, Standardisierungsboard, Betreiber, Umsetzungsteam.

### Daten

Es wird analysiert, wie die Daten der eingehenden Anfrage einzustufen sind.
Dabei wird unterschieden zwischen
* unstrukturierten Daten
* halbstrukturierten Daten
* strukturierten Daten

## Bearbeiten

Bei Beginn der Bearbeitung einer Aufgabe wird deren Status zunächst auf //In Bearbeitung// gesetzt. Im weiteren Verlauf der Bearbeitung ist der jeweilige Arbeitsfortschritt, beziehungsweise das jeweilige Arbeitsergebnis im Arbeitspaket zu dokumentieren (Reiter **Aktivität**).

:::info

Im Verlauf der Bearbeitung kann es erforderlich sein, Dritte einzubinden. 

:::

## Anfrage abschließen

### Abnahme/Review des Ergebnisses durch Auftraggeber

:::info

Sofern _Verantwortlicher_ und _Bearbeiter_ identisch sind, entfällt dieser Schritt.

:::

Für die Abnahme/Review ist das Arbeitsergebnis dem Verantwortlichen des Arbeitspakets zur Verfügung zu stellen, d.h. der Auftraggeber ist über die vollständige Bearbeitung zu informieren (durch Erwähnung in **Aktivität**, separate E-Mail, o.ä.).

## Information bereitstellen

Ergebnisse eines Arbeitspakets werden nicht nur in OpenProject festgehalten, sondern als Artefakte im openDesk-Modul Dateien (NextCloud) gespeichert.

Dies kann auf verschiedenen Wegen geschehen:

* **Aus dem Modul Projekte (OpenProject)**
 Im Reiter Dateien (rechte Seitenleiste) können Artefakte in der Nextcloud gespeichert werden. Dabei wird eine Verbindung zwischen dem Artefakt und dem Arbeitspaket hergestellt.
* Zum **Speichern im Modul Dateien (Nextcloud)** können verschiedene Vorgehensweisen genutzt werden
    * **Weboberfläche**: Laden von Artefakten mit der Weboberfläche. Dazu ziehen Sie die betreffende Datei in das Browser-Fenster.
    * **WebDAV**: Laden in das passende Verzeichnis mit dem Windows-Explorer mit der WebDAV-Verbindung (im Menüpunkt Dateien-Einstellungen beschrieben).
    * **Nextcloud-App**: Die Nextcloud App hängt das Dateiverzeichnis der Nextcloud in das lokale Dateiverzeichnis ein. So können Dateien vom lokalen Rechner in beiden Richtungen kopiert und verschoben werden.
