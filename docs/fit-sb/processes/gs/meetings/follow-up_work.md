---
title: Eine Sitzung nachbereiten
---

Auf dieser Seite erfahren Sie, welche Schritte die FIT-SB-Geschäftsstelle während der Nachbereitung einer FIT-SB-Sitzung unternimmt und wie diese im Detail umgesetzt werden.

:::note[Hinweis]

Wenn in der Spalte **Ausführung** keine abweichende Angabe steht, befinden wir uns beim Ausführen der Schritte in OpenProject im [Projekt **itPLR-24-024_FIT-SB**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/).

:::

|Schritte|Ausführung|
|-|-|
|Formulieren Sie die während der Sitzung in den TOPs festgehaltenen Notizen detailliert aus.|Um das Notizfeld eines mit der Besprechung verknüpften Elements zu bearbeiten, klicken Sie auf die drei Punkte rechts neben dem Benutzerfeld und wählen Sie **Bearbeiten**. Erfassen Sie dort Ihre Notizen (Bemerkungen, anfallende Aufgaben, getroffene Beschlüsse,...).|
|Dokumentieren Sie Beschlüsse in den jeweiligen Sitzungsthemen.|Beschlüsse werden zusätzlich im jeweiligen Sitzungsthema festgehalten. Öffnen Sie dazu das Sitzungsthema direkt aus der Besprechung, wechseln Sie im rechten Bildschirmbereich zum Register **Besprechungen** und dort auf das Unterregister **Vergangen**. Kopieren Sie den relevanten Textabschnitt zum Beschluss von hier in das Feld **Beschluss** oder passen Sie die Formulierung entsprechend an.|
|Erstellen Sie einen Protokollentwurf zur Weitergabe.|Übertragen Sie die in der Besprechung hinterlegten Notizen manuell in die Word-Protokollvorlage.|
|Senden Sie den Protokollentwurf mit der Sitzungsunterlage und Anlagen an die Sitzungsteilnehmer.|Exportieren Sie die Word-Datei als PDF und versenden Sie den Protokollentwurf über die Mailingliste. Fügen Sie der E-Mail den Protokollentwurf, die Sitzungsunterlage und gegebenenfalls weitere Anlagen bei.|
|Finalisieren Sie das Protokoll.|Falls die Sitzungsteilnehmer Einwände äußern, passen Sie den Protokollentwurf an, indem Sie sowohl die Notizen der Tagesordnungspunkte/Sitzungsthemen in der Besprechung als auch die übertragenen Einträge in der Word-Datei aktualisieren.|
|Senden Sie das finale Protokoll an die Sitzungsteilnehmer.|Exportieren Sie die Word-Datei erneut als PDF und versenden Sie das finale Protokoll über die Mailingliste. Fügen Sie der E-Mail ggfs. erneut die Sitzungsunterlage bzw. weitere Anlagen bei.|
|Bereinigen Sie die Sitzungsthemen.|Setzen Sie abgeschlossene Sitzungsthemen auf den Status **Abgeschlossen**, solche, die in der nächsten Besprechung erneut behandelt werden, auf **Nächste Sitzung**, und Themen, die später wieder aufgegriffen werden sollen, auf **Themenspeicher**.|
|Ordnen Sie die Sitzungsunterlagen.|Nextcloud: Erstellen Sie im Sitzungsordner einen neuen Unterordner namens **Einzeldateien** und verschieben Sie alle einzelnen Sitzungsunterlagen dorthin.|
|Lassen Sie die Sitzungsunterlage und das Protokoll veröffentlichen.|Leiten Sie die Sitzungsunterlage und das Protokoll zur Veröffentlichung [auf der FITKO-Webseite](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-standardisierungsboard) an die dafür zuständige Abteilung weiter.|

:::note[Hinweis]

Informationen zu Fristen finden Sie [hier](/docs/fit-sb/processes/members/meetings/index.md#fristen).

:::