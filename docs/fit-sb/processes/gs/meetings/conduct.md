---
title: Eine Sitzung durchführen
---

Auf dieser Seite erfahren Sie, welche Schritte die FIT-SB-Geschäftsstelle zur Durchführung einer FIT-SB-Sitzung unternimmt und wie diese im Detail umgesetzt werden.

:::note[Hinweis]

Wenn in der Spalte **Ausführung** keine abweichende Angabe steht, befinden wir uns beim Ausführen der Schritte in OpenProject im [Projekt **itPLR-24-024_FIT-SB**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/).

:::

|Schritte|Ausführung|
|-|-|
|Präsentieren Sie die aktuelle Besprechung den Sitzungsteilnehmern.|Arbeiten Sie mit zwei Monitoren und teilen Sie die aktuelle Besprechung in OpenProject über einen Bildschirm (Monitor 1) mit den Sitzungsteilnehmern. <br/> Vor Ort: Nutzen Sie einen Beamer, um Ihren Bildschirm für die Teilnehmer sichtbar zu machen. <br/> Remote: Teilen Sie Ihren Bildschirm über Jitsi.|
|Bereiten Sie sich für parallele Aktivitäten vor.|Öffnen Sie die Besprechung parallel auf dem nicht geteilten Monitor (Monitor 2).|
|Besprechen Sie jeden Agendapunkt mit den Sitzungsteilnehmern.|Monitor 1: Rufen Sie nacheinander die mit der Besprechung verknüpften Sitzungsthemen oder Aufgaben des Agendapunktes auf.|
|Teilen Sie zugehörige Anhänge mit den Teilnehmern.|Monitor 2: Öffnen Sie zu Sitzungsthemen oder Aufgaben zugehörige Anhänge auf Ihrem nicht geteilten Monitor und verschieben Sie sie anschließend auf den geteilten Bildschirm, um sie mit den Teilnehmern zu teilen.|
|Machen Sie Notizen zu den Sitzungsthemen oder Aufgaben.|Monitor 2: Um das Notizfeld eines verknüpften Elements zu bearbeiten, klicken Sie auf die drei Punkte rechts neben dem Benutzerfeld und wählen Sie **Bearbeiten**. Erfassen Sie dort Ihre Notizen (Bemerkungen, entstehende Aufgaben, Beschlüsse,...).|
|Zeigen Sie den aktuellen Inhalt der Agendapunkte.|Monitor 1: Nachdem Sie Änderungen in der Besprechung über den nicht geteilten Monitor vorgenommen haben, erscheint in OpenProject auf dem geteilten Bildschirm ein Hinweisfenster. Aktualisieren Sie anschließend die Seite.|
|Schliessen Sie die Sitzung ab.|Wenn die Zeit nicht ausreicht, um alle Agendapunkte zu besprechen, vereinbaren Sie den nächsten Termin vor dem Abschluss der Sitzung.|
