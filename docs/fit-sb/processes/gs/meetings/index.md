---
title: Sitzungen aus Sicht der Geschäftsstelle
---

Die FIT-SB-Geschäftsstelle ist für die erfolgreiche Organisation und Durchführung der Gremiensitzungen verantwortlich. Der gesamte Prozess gliedert sich in drei wesentliche Phasen, die jeweils spezifische Aufgaben und Verantwortlichkeiten umfassen:

## Sitzungsvorbereitung

In der [Vorbereitungsphase](prepare) werden alle notwendigen Schritte unternommen, um eine strukturierte und effiziente Sitzung zu gewährleisten. Dies beinhaltet unter anderem:

- Terminierung und Einladungsmanagement
- Erstellung und Abstimmung der Agenda
- Koordination der Sitzungsunterlagen
- Technische und organisatorische Vorbereitung

## Sitzungsdurchführung  

Während der [Durchführung der Sitzung](conduct) liegt der Fokus auf:

- Präsentation der Agenda und Moderation
- Dokumentation der Diskussionen und Beschlüsse
- Technische Unterstützung bei der Durchführung
- Koordination von Redebeiträgen und Abstimmungen

## Sitzungsnachbereitung

Die [Nachbereitung](follow-up_work) umfasst wichtige administrative und dokumentarische Aufgaben:

- Erstellung und Abstimmung des Protokolls
- Nachverfolgung von Beschlüssen und Aufgaben  
- Verwaltung und Archivierung der Unterlagen
- Vorbereitung der Veröffentlichung

In diesem Bereich finden Sie detaillierte Informationen und Anleitungen zu allen drei Phasen aus Sicht der FIT-SB-Geschäftsstelle. Die Prozesse sind dabei so gestaltet, dass sie sowohl für Präsenz- als auch für Online-Sitzungen anwendbar sind.

Die Geschäftsstelle nutzt für die Organisation und Durchführung der Sitzungen primär **OpenProject** als zentrales Werkzeug. Die einzelnen Arbeitsschritte werden in den verlinkten Unterseiten im Detail beschrieben.
