---
title: Eine Sitzung vorbereiten
---

Auf dieser Seite erfahren Sie, welche Schritte die FIT-SB-Geschäftsstelle zur Vorbereitung einer FIT-SB-Sitzung durchführt und wie diese im Detail umgesetzt werden.

:::note[Hinweis]

Wenn in der Spalte **Ausführung** keine abweichende Angabe steht, befinden wir uns beim Ausführen der Schritte in OpenProject im [Projekt **itPLR-24-024_FIT-SB**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/).

:::

|Schritte|Ausführung|
|-|-|
|Laden Sie zum Sitzungstermin ein.|Versenden Sie eine Einladung zum Sitzungstermin über Ihren E-Mail-Client an die FIT-SB-Mitglieder sowie deren Vertreter.|
|Erarbeiten Sie einen Vorschlag für die Themen der bevorstehenden FIT-SB-Sitzung.|Öffnen Sie die Arbeitspaketansicht [**02-Alle offenen FIT-SB-Sitzungsthemen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=241) und setzen Sie den Status von Sitzungsthemen auf **Aktuelle Sitzung**, welche Sie für die aktuelle Sitzung vorsehen. Dies ist im geöffneten Sitzungsthema direkt unter dem Titel möglich oder wenn sich bereits ein Sitzungsthema in diesem Status befindet, auch direkt per Drag & Drop zwischen den unterschiedlichen Status. <br/> Passen Sie ggfs. nochmals die Reihenfolge der Sitzungsthemen innerhalb der Liste **Aktuelle Sitzung** an (ebenfalls per Drag & Drop).|
|Stimmen Sie den Agendaentwurf mit dem FIT-SB-Vorsitz ab.|Öffnen Sie die Arbeitspaketansicht [**02-Alle offenen FIT-SB-Sitzungsthemen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=241) und stimmen Sie die darin enthaltenen Sitzungsthemen in einem gemeinsamen Termin mit dem FIT-SB-Vorsitz ab.|
|Legen Sie eine neue Besprechung an.|Navigieren Sie zu den Besprechungen und wählen Sie darin die Ansicht [**Zukünftige Meetings**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings?filters=%5B%7B%22time%22%3A%7B%22operator%22%3A%22%3D%22%2C%22values%22%3A%5B%22future%22%5D%7D%7D%5D&sort=start_time) aus. <br/> Öffnen Sie dann die Besprechung [**VORLAGE: 202J-MM-TT_FIT-SB_Sitzung**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings/19) und kopieren Sie diese, indem Sie auf den Button mit den drei Punkten im rechten Bildschirm-Bereich der Titelleiste klicken. Benennen Sie die neue Besprechung nach dem Schema **JJJJ-MM-TT_FIT-SB_Sitzung** (das Datum entspricht dem Datum des Sitzungstags).|
|Erstellen Sie die Agenda mit den zu besprechenden Punkten.|Überarbeiten Sie die Abschnitte in der Vorlage bzw. fügen Sie weitere Abschnitte hinzu. Fügen Sie zu den Abschnitten Sitzungsthemen (empfohlen) oder Aufgaben hinzu. <br/> Hinweis zum Benutzer, welcher neben dem Sitzungsthema/der Aufgabe angezeigt werden soll: Ist der Benutzer ein Gast der Sitzung, erwähnen Sie diesen im Notizfeld des in der Besprechung verknüpften Sitzungsthemas (bspw. **Gast: NAME**) und löschen Sie den vorausgewählten Eintrag aus dem Benutzerfeld heraus.|
|(Optional) Fügen Sie erste Anmerkungen und Informationen zu verknüpften Sitzungsthemen oder Aufgaben hinzu.|Klicken Sie rechts neben dem Eintrag des Sitzungsthemas in der Besprechung auf die drei Punkte und anschließend auf den Button **Bearbeiten** um das Notizfeld zu bearbeiten.|
|Senden Sie den Agendaentwurf den FIT-SB-Mitgliedern zu.|Versenden Sie den Agendaentwurf über die Mailingliste. Fügen Sie in die E-Mail den Link zur OpenProject-Besprechung bei und als Anhang einen Druck der Besprechung über den Browser (empfohlene Druckeinstellungen: Als PDF speichern, Querformat, A2, Skalierung: 94 %, Optionen: Kopf- und Fußzeilen).|
|(Optional) Informieren Sie weitere Stakeholder über die Agenda.|Versenden Sie den für die FIT-SB-Mitglieder erstellten Druck der Besprechung per E-Mail.|
|(Optional) Fügen Sie Gäste zur Sitzung hinzu.|Laden Sie Gäste über Ihren E-Mail-Client zum Termin ein.|
|(Optional) Überarbeiten Sie die Agenda nach Rückmeldung von FIT-SB-Mitgliedern.|Überarbeiten Sie die Abschnitte in der Vorlage bzw. fügen Sie weitere Abschnitte, Sitzungsthemen oder Aufgaben hinzu.|
|(Optional) Fordern Sie Unterlagen zu Punkten der Agenda ein.|Versenden Sie per E-Mail einen Reminder an die Sitzungsteilnehmer, zu deren Sitzungsthemen noch Sitzungsunterlagen ausstehen (s.a. Ansicht **[01-Aktuelle FIT-SB-Sitzung](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=240)**).|
|Bereiten Sie die Sitzungsunterlage mit Anlagen vor.|Führen Sie alle Sitzungsunterlagen zu einer PDF-Datei mit Deckblatt (bspw. über [PDF24](https://www.pdf24.org/de/)) zusammen.|
|Versenden Sie die finale Agenda mit den Unterlagen.|Versenden Sie die finale Agenda über die Mailingliste. Fügen Sie in die E-Mail den Link zur OpenProject-Besprechung bei, als Anhang einen Druck der Besprechung über den Browser (empfohlene Druckeinstellungen: Als PDF speichern, Querformat, A2, Skalierung: 94 %, Optionen: Kopf- und Fußzeilen) sowie die Sitzungsunterlage mit Anlagen.|
|Bringen Sie die Sitzungsthemen in die aktuelle Reihenfolge für die Sitzung.|Öffnen Sie die Arbeitspaketansichten [**01-Aktuelle FIT-SB-Sitzung**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=240) und [**02-Alle offenen FIT-SB-Sitzungsthemen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=241) und verschieben Sie die Sitzungsthemen innerhalb der jeweiligen Ansicht per Drag & Drop an die gewünschte Stelle.|

:::note[Hinweis]

Weitere Informationen zur Bearbeitung von Sitzungsthemen und zum Hinzufügen von Sitzungsunterlagen finden Sie auf den Seiten [Sitzungsthemen einreichen](/docs/fit-sb/processes/members/meetings/submit.md) und [Sitzungsunterlagen hinzufügen](/docs/fit-sb/processes/members/meetings/add.md).
:::
