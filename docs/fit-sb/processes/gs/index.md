---
title: FIT-SB-Leitfaden für die Geschäftsstelle
---

Dieser Bereich wendet sich primär an die Mitarbeitenden in der Geschäftsstelle des Standardisierungsboards für Föderale IT-Standards (FIT-SB-GS).  
Hier werden aus dem Blickwinkel der Geschäftsstelle solche Aufgabenbereiche und Prozesse betrachtet, für welche die FIT-SB-GS verantwortlich ist. Dazu zählen:

## Sitzungsmanagement

Die [Organisation und Durchführung der Gremiensitzungen des FIT-Standardisierungsboards](meetings) ist ein zentrales Element der Arbeit der FIT-SB-Geschäftsstelle. Der gesamte Prozess gliedert sich in drei wesentliche Phasen:

- Sitzungsvorbereitung
- Sitzungsdurchführung  
- Sitzungsnachbereitung

Die Prozesse sind dabei so gestaltet, dass sie sowohl für Präsenz- als auch für Online-Sitzungen anwendbar sind.

## FIT-Standards

Ein weiterer wichtiger Aufgabenbereich der Geschäftsstelle ist die [Pflege der FIT-Standards](fit-standards). Dies umfasst:

- Die Verwaltung der Informationen zu den FIT-Standards im Projekt [**05_FIT-Standards**](https://project.fitko.opendesk.live/projects/12/)
- Die Übertragung der Informationen an die [Informationsplattform](https://docs.fitko.de/fit-standards/)

Beide Aufgabenbereiche sind eng miteinander verzahnt und werden durch etablierte Werkzeuge (insbesondere **OpenProject**) und Prozesse unterstützt.
