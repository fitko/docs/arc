---
title: Aufgaben einsehen und bearbeiten
---

:::note[Seite im Aufbau 🚧]

Wir arbeiten für Sie daran, diese Seite mit aktuellen Informationen zu füllen. Bitte schauen Sie später noch einmal vorbei, um weitere Inhalte zu sehen.  
Vielen Dank für Ihre Geduld!

:::

## Aufgaben einsehen

Entstehen aus Sitzungsthemen Aufgaben, erstellt die FIT-SB-GS diese nach der Sitzung als Arbeitspakete vom Typ "Epic (FIT-SB)" oder "Aufgabe (FIT-SB)" und weist sie dem verantwortlichen Bearbeiter zu.

[![FIT-SB-Sitzungen](/images/fit-standardisierungsboard/meetings-index-2.png)](/images/fit-standardisierungsboard/meetings-index-2.png)
