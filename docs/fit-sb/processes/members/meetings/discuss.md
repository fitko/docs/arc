---
title: "Sitzungsthemen einsehen, diskutieren und verwalten"
---

## Sitzungsthemen einsehen

Sitzungsthemen sind zum einen im Board **FIT-SB Sitzungsthemen** und zum anderen in unterschiedlichen Arbeitspaketansichten einsehbar. Je nach Arbeitsweise können Sie die für Sie optimale Darstellung (Board oder Arbeitspaketansichten) wählen. <br/> Im Folgenden werden die unterschiedlichen Ansichten näher beschrieben.

:::note[Achtung!]

Bitte beachten Sie, dass die Reihenfolge der Sitzungsthemen in den Ansichten (Board und Arbeitspaketansichten) nicht zwingend der tatsächlichen Behandlungsreihenfolge in der FIT-SB-Sitzung entspricht.

:::

### Board **FIT-SB Sitzungsthemen**

Im Board [**FIT-SB Sitzungsthemen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/boards/46) sind die Sitzungsthemen und ihre jeweiligen Status einsehbar.

Das Board ist in Spalten unterteilt, wobei jede Spalte einen Status darstellt. Die Themen werden entsprechend ihres aktuellen Status in den jeweiligen Spalten aufgeführt.

[![Sitzungsthemen einsehen](/images/fit-standardisierungsboard/discuss-1-Uebersicht-Board.png)](/images/fit-standardisierungsboard/discuss-1-Uebersicht-Board.png)

### Arbeitspaketansichten

Für die FIT-SB-Sitzungen stehen drei Arbeitspaketansichten zur Verfügung: [**01-Aktuelle FIT-SB-Sitzung**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=240), [**02-Alle offenen FIT-SB-Sitzungsthemen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=241) und [**03-Alle FIT-SB-Sitzungsthemen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=85).

#### Ansicht **01-Aktuelle FIT-SB-Sitzung** {#01ansicht}

In dieser Ansicht finden Sie alle für die kommende Sitzung geplanten Sitzungsthemen (Sitzungsthemen im Status **Aktuelle Sitzung**). Der FIT-SB-Vorsitz und die FIT-SB-Geschäftsstelle nutzen diese Ansicht zur Vorbereitung auf die kommende Sitzung. Die verschiedenen Spalten bieten Ihnen einen umfassenden Überblick über relevante Informationen zu den Sitzungsthemen – beispielsweise den Status der Sitzungsunterlagen und die zuständigen Verantwortlichen für deren Bereitstellung. 

[![Ansicht 01-Aktuelle FIT-SB-Sitzung](/images/fit-standardisierungsboard/discuss-0-01-Aktuelle-FIT-SB-Sitzung.png)](/images/fit-standardisierungsboard/discuss-0-01-Aktuelle-FIT-SB-Sitzung.png)

:::note[Hinweis]

Weitere Informationen zu den einzelnen Feldern und ihrer Bedeutung finden Sie auf der Seite [Sitzungsthemen einreichen](/docs/fit-sb/processes/members/meetings/submit.md).

:::

#### Ansicht **02-Alle offenen FIT-SB-Sitzungsthemen** {#02ansicht}

In dieser Ansicht finden Sie alle offenen Sitzungsthemen (gruppiert nach Ihrem Status). Der FIT-SB-Vorsitz und die FIT-SB-Geschäftsstelle nutzen diese Ansicht zur effizienten Planung der kommenden Sitzung. Auch in dieser Ansicht bieten Ihnen die verschiedenen Spalten einen umfassenden Überblick über relevante Informationen zu den Sitzungsthemen für die anstehende Sitzung.

[![Ansicht 02-Alle offenen FIT-SB-Sitzungsthemen](/images/fit-standardisierungsboard/discuss-0-02-Alle-offenen-FIT-SB-Sitzungsthemen.png)](/images/fit-standardisierungsboard/discuss-0-02-Alle-offenen-FIT-SB-Sitzungsthemen.png)

#### Ansicht **03-Alle FIT-SB-Sitzungsthemen** {#03ansicht}

In dieser Ansicht finden Sie alle Sitzungsthemen (gruppiert nach Ihrem Status und automatisch sortiert nach Ihrer ID).

[![Ansicht 02-Alle offenen FIT-SB-Sitzungsthemen](/images/fit-standardisierungsboard/discuss-0-03-Alle-FIT-SB-Sitzungsthemen.png)](/images/fit-standardisierungsboard/discuss-0-03-Alle-FIT-SB-Sitzungsthemen.png)



:::note[Hinweise]

- Sitzungsthemen sind auch in den standardmäßig angezeigten Arbeitspaketansichten wie [**Alle offenen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages) zu sehen. 

- Welche Sitzungsthemen in einer FIT-SB-Sitzung tatsächlich behandelt wurden, kann ausschließlich nach der FIT-SB-Sitzung in der Besprechung in OpenProject eingesehen werden. Das Board und die Arbeitspaketansichten sind Momentaufnahmen und ändern sich je nach Phase des Sitzungslaufs.

:::

### Status der Sitzungsthemen

Sitzungsthemen befinden sich in einem der folgenden Status:

|Status|Bedeutung|
|-|-|
|Neu|Ein neu eingereichtes oder von der FIT-SB-Geschäftsstelle angelegtes Thema.|
|Themenspeicher|Das Thema wurde bereits vorqualifiziert und kann in zukünftigen Sitzungen eingeplant werden.|
|Nächste Sitzung|Das Thema ist für die kommende Sitzung vorgesehen.|
|Aktuelle Sitzung|Das Thema ist derzeit in der Phase der Vorbereitung, Durchführung oder Nachbereitung der laufenden Sitzung.|
|Abgeschlossen|Das Thema wurde besprochen und die Nachbereitung ist abgeschlossen.|


## Sitzungsthemen diskutieren

Um ein Sitzungsthema mit anderen FIT-SB-Mitgliedern zu diskutieren, verwenden Sie das Kommentarfeld auf dem Reiter **Aktivität** ([s.a. OpenProjekt Nutzungshandbuch](https://www.openproject.org/de/docs/nutzungshandbuch/aktivitaet/#arbeitspaket-aktivit%C3%A4t)) im jeweiligen Sitzungsthema.

[![Sitzungsthemen diskutieren](/images/fit-standardisierungsboard/discuss-3-Kommentarfeld.png)](/images/fit-standardisierungsboard/discuss-3-Kommentarfeld.png)

## Benachrichtigungen

Standardmäßig erhalten Sie keine Benachrichtigungen für Sitzungsthemen, an denen Sie nicht beteiligt sind.
<br/> Sie gelten als beteiligt, wenn Sie in einem Sitzungsthema erwähnt, das Thema mit Ihnen geteilt wurde oder Sie als Beobachter hinzugefügt wurden. <br/> Sie haben die Möglichkeit sich als Beobachter selbst hinzuzufügen:

[![ALs Beobachter hinzufuegen](/images/fit-standardisierungsboard/discuss-4-Beobachter.png)](/images/fit-standardisierungsboard/discuss-4-Beobachter.png)

Anleitungen zur Anpassung der Benachrichtigungseinstellungen entnehmen Sie bitte dem [OpenProject Nutzungshandbuch](https://www.openproject.org/de/docs/nutzungshandbuch/benachrichtigungen/benachrichtigungseinstellungen).

:::note[Hilfreiche Hinweise zur Anpassung der Benachrichtigungseinstellungen]

Sitzungsthemen gehören zu den Arbeitspaketen in OpenProject.

:::
