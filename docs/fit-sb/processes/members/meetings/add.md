---
title: Sitzungsunterlagen hinzufügen
---

Es kann sinnvoll sein, zu einem Sitzungsthema passende Unterlagen hinzuzufügen. <br/>

Die empfohlene Vorgehensweise dafür finden Sie hier:

:::note[Hinweis]

Wir empfehlen Ihnen, Ihre Sitzungsunterlagen im PDF-Format abzulegen. Zusätzlich können Sie gerne auch das Rohdokument bereitstellen. 

:::

1. Öffnen Sie in der Nextcloud den Ordner **03_Sitzungen**. ([Direktlink](https://fs.fitko.opendesk.live/apps/files/files?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/01_Organisation/03_Sitzungen)) und wählen Sie den Ordner mit der Sitzung aus, zu der Sie eine Unterlage hinzufügen möchten.

[![Ordner 03_Sitzungen Nextcloud](/images/fit-standardisierungsboard/add-1-Ordner_03_Sitzungen.png)](/images/fit-standardisierungsboard/add-1-Ordner_03_Sitzungen.png)

2. Benennen Sie Ihre Datei nach dem Schema: **JJJJMMTT (Datum der Sitzung)__FIT-SB_TOP-XX (Nummer des Tagesordnungspunkts in der Besprechung)_(Titel des Themas)** und legen Sie sie hier ab.  
Zum Beispiel:

[![Ordner 03_Sitzungen Nextcloud Sitzungungsordner](/images/fit-standardisierungsboard/add-2-Sitzungsordner.png)](/images/fit-standardisierungsboard/add-2-Sitzungsordner.png)

3. Verknüpfen Sie die Unterlage mit Ihrem Sitzungsthema.  
Öffnen Sie dazu die Details der Datei...

[![Ordner 03_Sitzungen Nextcloud Sitzungungsordner Details](/images/fit-standardisierungsboard/add-3-Sitzungsordner_Details.png)](/images/fit-standardisierungsboard/add-3-Sitzungsordner_Details.png)

...und navigieren Sie zum Eintrag **OpenProject**. <br/>
Nutzen Sie anschließend die Suchzeile, um Ihr Sitzungsthema zu finden (z. B. durch Eingabe der ID), und wählen Sie es aus.

[![Ordner 03_Sitzungen Nextcloud Sitzungungsordner Details Suche](/images/fit-standardisierungsboard/add-4-Sitzungsordner_Details_OP.png)](/images/fit-standardisierungsboard/add-4-Sitzungsordner_Details_OP.png)

4. Aktualisieren Sie die Felder im Sitzungsthema in OpenProject für Sitzungsunterlagen<br/>
Der Abschnitt **Bereitstellung der Sitzungsunterlagen** dient dazu, zu dokumentieren, ob und von wem Sitzungsunterlagen bereitgestellt wurden. <br/>
Bitte aktualisieren Sie die entsprechenden Felder, sobald Sie eine Unterlage bereitgestellt haben. <br/>

[![Abschnitt Bereitstellung der Sitzungsunterlagen](/images/fit-standardisierungsboard/add-5-Bereitstellung-der-Sitzungsunterlagen.png)](/images/fit-standardisierungsboard/add-5-Bereitstellung-der-Sitzungsunterlagen.png)

:::note[Hinweis]

Hinweise zum Ausfüllen der Felder im Abschnitt **Bereitstellung der Sitzungsunterlagen** finden Sie jeweils über Anklicken des **?** hinter der jeweiligen Feldbezeichnung oder unter den Feldbeschreibungen [hier](/docs/fit-sb/processes/members/meetings/submit.md). 

:::

Zusätzliche Informationen zum allgemeinen Speichern von Artefakten in openDesk erhalten Sie auf [dieser Seite](/docs/fit-sb/processes/basics/artefacts/save.md).