---
title: Sitzungsthemen einreichen
---

## Anmeldefrist

Die Frist zur Anmeldung eines Sitzungsthemas für die nächste Sitzung können Sie [hier](index.md#fristen) entnehmen.

## Einreichung eines Sitzungsthemas über openDesk

Wenn Sie Themen zur Diskussion oder für einen Beschluss in einer der kommenden FIT-SB-Sitzungen einreichen möchten, stehen Ihnen verschiedene Möglichkeiten zur Verfügung. 

Für eine schnelle und einfache Möglichkeit, gehen Sie wie folgt vor:

:::note[Hinweis]

Stellen Sie sicher, dass Sie das Projekt [**itPLR-24-024_FIT-SB**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/) ausgewählt haben.

:::

1. Klicken Sie auf das **+**-Symbol und erstellen Sie ein neues **Sitzungsthema (FIT-SB)**.

[![Sitzungsthema erstellen](/images/fit-standardisierungsboard/submit-2-Plus-AP_erstellen.png)](/images/fit-standardisierungsboard/submit-2-Plus-AP_erstellen.png)

2. Tragen Sie die erforderlichen Informationen in die Felder ein. <br/>

[![Maske Sitzungsthema](/images/fit-standardisierungsboard/submit-3-AP-Maske.png)](/images/fit-standardisierungsboard/submit-3-AP-Maske.png)


Hinweise zum Ausfüllen der FIT-SB-spezifischen Felder finden Sie jeweils über Anklicken des **?** hinter der jeweiligen Feldbezeichnung im Sitzungsthema. Die Ausfüllhinweise zu den Feldern **Titel**, **Beschreibung**, **Priorität**, **Datum**, **Verantwortlich** und **Zugewiesen an** entnehmen Sie bitte den folgenden Feldbeschreibungen.


### Feld „Titel“

Verwenden Sie einen aussagekräftigen Titel für das Thema, das in der Sitzung behandelt wird.

### Feld „Beschreibung“

Formulieren Sie Ziel und Zweck der Befassung mit dem Sitzungsthema in der Beschreibung klar und umfassend.

### Feld „Priorität“

Bitte bestimmen Sie die Priorität, mit der aus Ihrer Sicht das Thema behandelt werden sollte.

|Priorität|Bedeutung|
|-|-|
|Niedrig|Das Thema kann ggf. auch in einer späteren Sitzung behandelt werden.|
|Normal|Standardwert. Das Thema sollte, wenn möglich, in der nächsten Sitzung besprochen werden.|
|Hoch|Das Thema ist dringend und sollte unbedingt in der nächsten Sitzung behandelt werden.|
|Sofort|Das Thema ist sehr dringend und erfordert eine Sondersitzung.|

### Feld „Datum“

Das Datum gibt an, bis wann die Sitzungsunterlage erstellt und bereitgestellt sein sollte.

### Feld „Verantwortlich“

Das Feld **Verantwortlich** benennt die Person, die für die Erstellung und Bereitstellung der Sitzungsunterlage verantwortlich ist.

### Feld „Zugewiesen an“

Das Feld **Zugewiesen an** benennt die Person, die mit der Erstellung und Bereitstellung der Sitzungsunterlage beauftragt ist.