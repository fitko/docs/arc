---
title: Sitzungen aus Sicht der Mitglieder
toc_max_heading_level: 4
---
import MeetingTL from '/images/fit-standardisierungsboard/Besprechungen-Timeline.svg'
import MeetingTL_dark_mode from '/images/fit-standardisierungsboard/Besprechungen-Timeline_dark-mode.svg'

## Prozess-Summary

FIT-SB-Sitzungen werden überwiegend über openDesk (OpenProject) organisiert.

Diese Plattform ermöglicht es, Informationen wie die Tagesordnung, Protokolle sowie verknüpfte Aufgaben und Verantwortlichkeiten übersichtlich und leicht zugänglich für alle FIT-SB-Mitglieder bereitzustellen. Diskussionen und Beschlüsse werden transparent festgehalten und dokumentiert.

Die folgende Darstellung zeigt den Prozessablauf einer FIT-SB-Sitzung mit den dazugehörigen Aufgaben und Verantwortlichkeiten (in Klammer).

Klicken Sie zur Navigation auf das Element in der Grafik, zu dem Sie Informationen wünschen oder verwenden Sie das Menü in der linken Seitenleiste (auf kleinen Bildschirmen öffnen Sie zunächst das Menü durch Anklicken des **☰**-Symbols).

<div className="meeting-tl light svg-border svg-width100">
  <MeetingTL />
</div>

<div className="meeting-tl dark svg-border svg-width100">
  <MeetingTL_dark_mode />
</div>

## Fristen

Im Prozessablauf sind die verschiedenen Aufgaben dargestellt, von denen einige an spezifische Fristen gebunden sind. Diese Fristen finden Sie in der nachfolgenden Tabelle.

|Phase|Aufgabe|Frist|
|:-|:-|:-|
|**Themensammlung**|Sitzungsthemen einreichen|bis 3-4 Wochen vor der nächsten Sitzung|
|**Vorbereitung**|Tagesordnungsentwurf verteilen|mind. 3 Wochen vor einer regulären Sitzung|
||Sitzungsunterlagen bereitstellen|1 Woche vor der Sitzung|
||Finale Tagesordnung verteilen|bis 1 Woche vor der Sitzung|
|**Nachbereitung**|Protokollentwurf verteilen|1 Woche nach der Sitzung|
||Protokoll einsehen und ggf. Einwände erheben|Innerhalb von 2 Wochen <br/> (erfolgen innerhalb dieses Zeitraums keine Einwände, gilt das Protokoll als genehmigt)|
||Finales Protokoll verteilen|Veröffentlichung frühestens 3 Wochen nach der Sitzung|

## Prozessablauf

### Themensammlung

#### Sitzungsthemen einreichen

FIT-SB-Mitglieder reichen Sitzungsthemen ein. Weitere Informationen hierzu erhalten Sie [hier](../meetings/submit.md).

#### Sitzungsthemen sammeln

Die FIT-SB-Geschäftsstelle sammelt weitere Themen, die von außen an das FIT-SB herangetragen werden.

### Vorbereitung

#### Tagesordnungsentwurf abstimmen

Der FIT-SB-Vorsitz stimmt den Tagesordnungsentwurf mit der FIT-SB-GS ab.

#### Tagesordnungsentwurf verteilen

Die FIT-SB-GS verteilt den Tagesordnungsentwurf über die FIT-SB-Mailingliste.

#### Gäste einladen lassen

Mitglieder und Vertreter können Gäste zur Sitzung einladen lassen, indem sie eine E-Mail an [it-standards@fitko.de](mailto:it-standards@​fitko.de)
senden.

#### Sitzungsunterlagen bereitstellen

Referenten stellen Ihre Sitzungsunterlagen auf der Kollaborationsplattform zur Verfügung.
Weitere Informationen, wie Sitzungsunterlagen bereitgestellt werden können, finden sich unter [Sitzungsunterlagen hinzufügen](/docs/fit-sb/processes/members/meetings/add.md).

#### Finale Tagesordnung verteilen

Die FIT-SB-GS verteilt die finale Tagesordnung über die FIT-SB-Mailingliste und an Gäste.

### Durchführung

#### Sitzung leiten

Der FIT-SB-Vorsitz leitet die Sitzung.

#### Sitzungsthemen besprechen

Alle Teilnehmer behandeln die in der FIT-SB-Sitzung geplanten Sitzungsthemen und fassen ggf. einen Beschluss.

#### Protokoll führen

Die FIT-SB-GS führt Protokoll und dokumentiert Beschlüsse.

### Nachbereitung

#### Aufgaben anlegen

Die FIT-SB-GS erstellt Aufgaben, die sich aus der Sitzung ergeben, und ordnet sie den zuständigen Personen zu.

#### Protokollentwurf verteilen

Die FIT-SB-GS erstellt einen Protokollentwurf und stellt diesen bereit.

#### Protokoll einsehen und ggf. Einwände erheben

Sitzungsprotokolle sind im Besprechungsmodul von openDesk einsehbar und als PDF-Datei in der Dateiablage (Nextcloud) abgelegt.
Alle Teilnehmer haben die Möglichkeit die Protokolle einzusehen und ggf. innerhalb der vorgegebenen Frist Einwände zu erheben.

#### Finales Protokoll verteilen

Die FIT-SB-GS sendet den Link zum finalen Protokoll über die Mailingliste und an Gäste. Des Weiteren wird das finale Protokoll auf der [Webseite des Föderalen IT-Standardisierungsboards](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-standardisierungsboard) veröffentlicht.
