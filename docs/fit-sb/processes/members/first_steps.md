---
title: Erste Schritte
---

## Herzlich Willkommen in Ihrem openDesk-Zugang

Sie wurden über die Bereitstellung Ihres openDesk-Zugangs informiert. Auf dieser Seite erfahren Sie, wie Sie Ihren Zugang aktivieren, Schritt für Schritt.

Um auf Ihr openDesk-Benutzerkonto zuzugreifen, gehen Sie bitte folgendermaßen vor:

- Klicken Sie auf [diesen Link](https://portal.fitko.opendesk.live/univention/portal/#/selfservice/passwordforgotten).
- Geben Sie auf der angezeigten Webseite Ihren openDesk-Usernamen ein:  
![Passwort vergessen](/images/fit-standardisierungsboard/oD-Password-forgotten.png)
- Ihr openDesk-Username setzt sich aus Ihrem Vor- und Zunamen in Kleinbuchstaben zusammen, getrennt durch einen Punkt. Also zum Beispiel _max.mustermann_.

- Klicken Sie auf **Next** und wählen Sie im nächsten Fenster die Option **Email**.
- Klicken Sie auf **Submit**.
- Sie erhalten kurze Zeit später eine E-Mail vom _Passwort Reset Service_ (Absender: noreply​\@​fitko.opendesk.live), die einen Link enthält. Klicken Sie auf den Link und vergeben Sie ein sicheres Passwort (mindestens 8 Zeichen, bestehend aus Groß- und Kleinbuchstaben, Ziffern und Sonderzeichen).
- Wiederholen Sie Ihr Passwort im Feld darunter und klicken Sie abschließend auf **PASSWORT ÄNDERN**.

Das war's! Sie können sich ab sofort über [diesen Link](https://portal.fitko.opendesk.live/univention/portal/#/) mit Ihrem openDesk-Usernamen und dem von Ihnen festgelegten Passwort bei openDesk anmelden.

## Verbinden von Dateien mit Projekten

 Im nächsten Schritt ist die Dateiablage mit dem Aufgaben-und Sitzungsmanagement zu verbinden. Dazu vollziehen Sie die Schritte, die in der Anleitung [Verbinden Sie Ihr OpenProject-Konto mit Ihrem Nextcloud-Konto](https://www.openproject.org/de/docs/nutzungshandbuch/dateimanagement/nextcloud-integration/#verbinden-sie-ihr-openproject-konto-mit-ihrem-nextcloud-konto) (OpenProject Nutzungshandbuch) beschrieben sind.

## Inhalte in openDesk

Aktuell steht Ihnen openDesk für folgende Zwecke zur Verfügung:

- als **Dateiablage** (Cloudspeicher) zum Austausch mit anderen FIT-SB-Mitgliedern und der FIT-SB-Geschäftsstelle
- zum **Erstellen und Bearbeiten von Dateien** mit Collabora Online, einem leistungsfähigen Online-Dokumentenbearbeitungsprogramm
- für das **Sitzungs- und Aufgabenmanagement** (OpenProject)
- für das **Einsehen von Informationen zu FIT-Standards** (OpenProject)

## Navigation in dieser Online-Anleitung

Navigieren Sie über das Menü in der Seitenleiste links (auf kleinen Bildschirmen: Klicken Sie zuerst auf das **☰**-Symbol) oder verwenden Sie dafür die Sitemap-Grafik auf der [FIT-SB-Startseite](/fit-sb/).

**Viel Spaß mit openDesk für das FIT-Standardisierungsboard!**  
Bei Fragen, Problemen oder Anregungen senden Sie eine E-Mail an [it-standards\@fitko.de](mailto:it-standards@fitko.de)
