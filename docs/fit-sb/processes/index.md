---
title: Leitfaden für das FIT-Standardisierungsboard
---

Diese Übersichtsseite bietet Ihnen einen schnellen Überblick über die wichtigsten Inhalte der **Dokumentation für das Standardisierungsboard für Föderale IT-Standards** (FIT-SB) und dient als Ausgangspunkt für eine tiefergehende Exploration.

Nachfolgend finden Sie Erläuterungen zu den Inhalten der verschiedenen Unterbereiche. Die jeweiligen Inhalte sind hilfreich, um die Abläufe im FIT-SB zu verstehen und effizient mitarbeiten zu können.

## Grundlegendes

Der Bereich [Grundlegendes](basics) bietet eine Einführung in die Nutzung von _openDesk_ und den darin enthaltenen Werkzeugen wie _OpenProject_ und _Nextcloud_.  
Hier werden wichtige Begriffe erklärt, die Navigation wird erläutert sowie das Ablegen und Finden von Artefakten.

## Für FIT-SB-Mitglieder

Der Bereich [Für FIT-SB-Mitglieder](members) wendet sich primär an die Mitglieder des FIT-SB. Er bietet detaillierte Informationen und Anleitungen für verschiedene Aspekte der FIT-SB-Mitgliedschaft, im Einzelnen:

- [**Erste Schritte**](members/first_steps): Tipps und Anleitungen für neue Mitglieder, um sich schnell in die Strukturen und Prozesse einzuarbeiten.  
- [**FIT-SB-Sitzungen**](members/meetings): Informationen, die im Zusammenhang mit dem Prozess rund um die Sitzungen des Standardisierungsboards stehen. Hier finden Sie praktische Tipps zur effektiven Teilnahme an Meetings und zur Erfüllung Ihrer Verantwortlichkeiten.

## Für die FIT-SB-Geschäftsstelle

[Dieser Bereich](gs) wendet sich primär an die Mitarbeitenden in der FIT-SB-Geschäftsstelle.

Er beschreibt die Abläufe in der Geschäftstelle, insbesondere im Zusammenhang mit FIT-SB-Sitzungen.

## Anwendungsszenarien

Der Bereich [Anwendungsszenarien](examples) zeigt anhand von Beispielen konkrete Lösungswege auf, um Antworten auf verschiedene Fragestellungen rund um das FIT-Standardisierungsboard zu erhalten. Hier werden typische Szenarien und Herausforderungen dargestellt, die im Alltag auftreten können.  
Durch die realistischen Beispiele ist dieser Bereich besonders nützlich für die Vertiefung des Verständnisses und bietet wertvolle Einblicke in die praktische Anwendung der FIT-SB-Werkzeuge.
