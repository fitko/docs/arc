---
title: Wie kann ich Informationen zu einer FIT-SB-Sitzung finden?
---

Auf dieser Seite wird dargestellt, wie Informationen zu Sitzungen des Föderalen Standardisierungsboards (FIT-SB) gefunden werden können.
- [Wer hat teilgenommen?](#teilnehmer)
- [Welche Sitzungsthemen werden bzw. wurden behandelt?](#sitzungsthemen)
- [Welche Beschlüsse wurden gefasst?](#beschluesse)
- [Wie kann ich Begleitdokumente einsehen?](#begleitdokumente)
- [Wie kann ich Informationen zu zukünftigen Sitzungen einsehen?](#zukuenftige_Sitzungen)

:::note[Hinweis]

Stellen Sie sicher, dass Sie das Projekt **itPLR-24-024_FIT-SB** ausgewählt haben und sich im Bereich [**Besprechungen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings) befinden.

:::


Bitte wählen Sie aus der linken Menüleiste [**Vergangene Meetings**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings?filters=%5B%7B%22time%22%3A%7B%22operator%22%3A%22%3D%22%2C%22values%22%3A%5B%22past%22%5D%7D%7D%5D&sort=start_time%3Adesc) aus.

Es werden alle vergangenen Sitzungen angezeigt. Sie können die gewünschte Sitzung anwählen. 

Beispiel: Sitzung vom 19.09.2024

## Wer hat teilgenommen? {#teilnehmer}

Eine Liste der Teilnehmer ist unter **TOP 01 - Begrüßung und Feststellung der Beschlussfähigkeit** zu finden.
![Teilnehmer](/images/fit-standardisierungsboard/OP-Besprechungen-Teilnehmer.png)

## Welche Sitzungsthemen werden bzw. wurden behandelt? {#sitzungsthemen}

Eine Besprechung wird in Tagesordnungspunkten (TOPs) gruppiert. Ein TOP kann ein oder mehrere Arbeitspakete (Sitzungsthema, Aufgabe) enthalten (auf dem Bild roter Kasten). 

Um weitere Informationen zu erhalten, öffnen Sie das Sitzungsthema indem Sie den Namen anwählen.

[![Arbeitspaket IEA](/images/fit-standardisierungsboard/OP-Besprechungen-Arbeitspaket-IEA1.png)](/images/fit-standardisierungsboard/OP-Besprechungen-Arbeitspaket-IEA1.png)

Zu jedem TOP findet direkt eine Protokollierung statt. Zu verlinkten Arbeitspaketen wird das Protokoll zu diesen geschrieben. Hierdurch ist dieser Teil des Protokolls auch im verknüpften Arbeitspaket verfügbar.

[![Sitzungsthemen](/images/fit-standardisierungsboard/OP-Besprechungen-Sitzungsthemen.png)](/images/fit-standardisierungsboard/OP-Besprechungen-Sitzungsthemen.png)
[![Besprechungsprotokoll](/images/fit-standardisierungsboard/OP-Besprechungsprotokoll-in-Arbeitspaket.png)](/images/fit-standardisierungsboard/OP-Besprechungsprotokoll-in-Arbeitspaket.png)

## Welche Beschlüsse wurden gefasst? {#beschlüsse}

Der getroffene Beschluss wird jeweils im TOP beim Sitzungsthema protokolliert. Zusätzlich wird die Informationen zum Beschluss auch im entsprechenden **Sitzungsthema (FIT-SB)** in dem Feld **Beschluss** dokumentiert. 

[![Beschluss](/images/fit-standardisierungsboard/OP-Beschluesse.png)](/images/fit-standardisierungsboard/OP-Beschluesse.png)


## Wie kann ich Begleitdokumente einsehen? {#begleitdokumente}

Mit TOPs verknüpfte Arbeitspakete können Begleitdokumente enthalten. Um diese einzusehen öffnen Sie das entsprechende Arbeitspaket und wechseln Sie auf das Register **Dateien**.

[![Sitzungsthema Anhänge](/images/fit-standardisierungsboard/OP-Besprechungen-IEA-Dateien.png)](/images/fit-standardisierungsboard/OP-Besprechungen-IEA-Dateien.png) 

## Wie kann ich Informationen zu zukünftigen Sitzungen einsehen? {#zukuenftige_Sitzungen}

Stellen Sie sicher, dass Sie das Projekt **itPLR-24-024_FIT-SB** ausgewählt haben und sich im Bereich [**Besprechungen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings) befinden.

Bitte wählen Sie aus der linken Menüleiste [**Zukünftige Meetings**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings?filters=%5B%7B%22time%22%3A%7B%22operator%22%3A%22%3D%22%2C%22values%22%3A%5B%22future%22%5D%7D%7D%5D&sort=start_time) aus.

Es werden alle zukünftigen Meetings aufgelistet. Aus der Auflistung können Sie die gewünschte Sitzung auswählen. 
