---
title: Wie kann ich Informationen zu einem Themenfeld finden?
---
# Wie kann ich Informationen zu<br/>einem Themenfeld finden?

:::note[Hinweis]

Stellen Sie sicher, dass Sie das Projekt [**itPLR-24-024_FIT-SB**](https://project.fitko.opendesk.live/projects/3/?jump=angular) ausgewählt haben.

:::

Themenfelder werden in Projekten als Arbeitspakete abgebildet.

1. Gehen Sie über die linke Menüleiste auf [**Arbeitspakete**->**Alle offenen**](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages)

![Arbeitspaket Auswahl](/images/fit-standardisierungsboard/Arbeitspaket-Themenfeld.png)

2. Wählen Sie aus der Liste der Themenfelder das gewünschte Themenfeld aus.

Unter **04_Themenfelder** werden die aktuellen Themenfelder aufgelistet. 

[![Themenfelder](/images/fit-standardisierungsboard/Themenfelder.png)](/images/fit-standardisierungsboard/Themenfelder.png)


Beispiel: „OZG Rechtsverordnung“

Die dazugehörigen Arbeitspakete, beispielsweise Sitzungsthemen und Aufgaben, finden Sie unter dem Register **Beziehungen**

[![OZG EPIC](/images/fit-standardisierungsboard/Register_Beziehungen_Themenfelder.png)](/images/fit-standardisierungsboard/Register_Beziehungen_Themenfelder.png)
