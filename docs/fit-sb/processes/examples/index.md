---
title: Anwendungsszenarien
---

Anhand von Beispielen zeigt dieser Abschnitt Lösungswege auf, wie sich Antworten auf verschiedene Fragestellungen rund um das FIT-Standardisierungsboard finden lassen.

[Informationen zu einer FIT-SB-Sitzung](info_sb-sitzung) erläutert, wie man detaillierte Informationen zu einer typischen SB-Sitzung erhält.

Ein _Themenfeld_ bezeichnet ein spezifisches Sachgebiet oder eine inhaltliche Kategorie, in der verwandte Themen und Aspekte zusammengefasst und behandelt werden. Wie Sie Informationen zu Themenfeldern im Kontext des FIT-SB finden, erfahren Sie [auf dieser Seite](info_themenfeld).
