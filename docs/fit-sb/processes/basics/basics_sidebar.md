---
title: Navigationshinweise
---
Die Hinweise auf dieser Seite unterstützen Sie bei der Navigation in der openDesk Arbeitsumgebung für das Föderale IT Standardisierungsboard (FIT-SB).

## Sitzungs-/Aufgabenmanagement des FIT-SB

Diese Themen werden in einem Projekt gemanaged (Informationen zu den FIT-Standards werden ebenfalls in einem Projekt verwaltet).  
Zu den Projekten gelangen sie [über diesen Link](https://project.fitko.opendesk.live/my/page) oder wenn Sie auf der Startseite von openDesk auf **Projekte** klicken.

[![Management Projekt](/images/fit-standardisierungsboard/OP-Projektstruktur2.png)](https://project.fitko.opendesk.live/my/page)

### Projekt itPLR-24-024_FIT-SB auswählen

Die Aktivitäten des FIT-Standardisierungsboards sind im Sitzungs- und Aufgabenmanagement im Projekt [itPLR-24-024_FIT-SB](https://project.fitko.opendesk.live/projects/3/?jump=angular) zusammengefasst. Wählen Sie das Projekt aus, indem Sie bei der Projektauswahl (in der OpenProject-Kopfzeile) auf den Eintrag **itPLR-24-024_FIT-SB** klicken.

[![Projekt auswählen](/images/fit-standardisierungsboard/OP-Projektstruktur.png)](https://project.fitko.opendesk.live/projects/3/?jump=angular)

#### Projektübersicht

Nachdem Sie das Projekt ausgewählt haben, sehen Sie die Projektübersicht:

[![Projektübersicht](/images/fit-standardisierungsboard/Übersichtsseite-itPLR-24-024_FIT-SB.png)](/images/fit-standardisierungsboard/Übersichtsseite-itPLR-24-024_FIT-SB.png)

Die Projektübersicht enthält folgende Informationen:

- Begrüssungstext mit Link zu diesem Online-Leitfaden
- Eine Auflistung Ihrer offenen Aufgaben nach Fälligkeit
- Eine Auflistung aller offenen Arbeitspakete mit Fälligkeit
- Linkverzeichnis mit
  - internen Links (Links innerhalb von OpenProject)
  - weiteren hilfreichen Links im Zusammenhang mit dem FIT-Standardisierungsboard


#### Arbeitspaketansichten im Projekt itPLR-24-024_FIT-SB {#arbeitspaketansichten-overview}

Die Arbeitspaketansichten in diesem Projekt zeigen Übersichten über Sitzungsthemen zu FIT-SB-Sitzungen:

- [01-Aktuelle FIT-SB-Sitzung](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=240) gibt einen Überblick über alle zum aktuellen Zeitpunkt für die kommende Sitzung geplanten Sitzungsthemen. <br/> Für weitere Informationen: [Ansicht **01-Aktuelle FIT-SB-Sitzung**](/docs/fit-sb/processes/members/meetings/discuss.md#01ansicht)  

- [02-Alle offenen FIT-SB-Sitzungsthemen](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=241) gibt einen Überblick über alle zum aktuellen Zeitpunkt offenen Sitzungsthemen. <br/> Für weitere Informationen: [Ansicht **02-Alle offenen FIT-SB-Sitzungsthemen**](/docs/fit-sb/processes/members/meetings/discuss.md#02ansicht)

- [03-Alle FIT-SB-Sitzungsthemen](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages?query_id=85) gibt einen Überblick über alle Sitzungsthemen. <br/> Für weitere Informationen: [Ansicht **03-Alle FIT-SB-Sitzungsthemen**](/docs/fit-sb/processes/members/meetings/discuss.md#03ansicht)

#### Boards im Projekt itPLR-24-024_FIT-SB für eine bessere Übersicht {#boards-overview}

Die Boards in diesem Projekt zeigen Übersichten zu einzelnen Themenbereichen:

- [FIT-SB Aktivitätsboard](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/boards/26) gibt einen Überblick aller Aufgaben und Epics des FIT-SB nach ihrem Status.
- [FIT-SB Sitzungsthemen](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/boards/46) stellt alle Sitzungsthemen in ihrem Planungsstatus dar und wird zur Sitzungsplanung verwendet.

#### Sitzungen des FIT-SB

Zu den Sitzungen des FIT-SB gelangen Sie [direkt](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/meetings?filters=%5B%7B%22time%22%3A%7B%22operator%22%3A%22%3D%22%2C%22values%22%3A%5B%22future%22%5D%7D%7D%5D&sort=start_time/) oder über die Startseite des openDesks und **Projekte**. Nach der Auswahl des Projekts **itPLR-24-024_FIT-SB** klicken Sie in der linken Navigationsleiste auf **Besprechungen**.

[![Sitzungsmanagement](/images/fit-standardisierungsboard/OP-Besprechungen.png)](/images/fit-standardisierungsboard/OP-Besprechungen.png)

#### Aufgabenmanagement des FIT-SB

In das Aufgabenmanagement des FIT-SB gelangen Sie [direkt](https://project.fitko.opendesk.live/projects/itplr-24-024_fit-sb/work_packages) oder über die Startseite des openDesks und **Projekte**. Nach der Auswahl des Projekts **itPLR-24-024_FIT-SB** wählen Sie in der linken Navigationsleiste **Aufgabenpakete** aus.

Unter Arbeitspaketen werden unterschiedliche vordefinierte Ansichten zur Auswahl angeboten. Über die Standardansicht **Mir zugewiesen** können Sie sich beispielsweise alle Arbeitspakete anzeigen lassen, die Ihnen zugewiesen sind.

[![Aufgabenmanagement](/images/fit-standardisierungsboard/OP-AP-Listen.png)](/images/fit-standardisierungsboard/OP-AP-Listen.png)

## Dateiablage des FIT-SB

Zur Dateiablage des FIT-SB gelangen sie [direkt](https://fs.fitko.opendesk.live/index.php/apps/files/files/647?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB) oder über die Startseite des openDesks und **Dateien**.

[![Sitzungsmanagement](/images/fit-standardisierungsboard/NC-Icon-Dateien.png)](/images/fit-standardisierungsboard/NC-Icon-Dateien.png)

Weitere Informationen erhalten Sie unter [Artefakte verwalten](/docs/fit-sb/processes/basics/artefacts/index.md).
