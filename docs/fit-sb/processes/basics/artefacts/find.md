---
title: Wo finde ich welche Artefakte?
---

Die Artefakte für das FIT-Standardisierungsboard finden Sie unter [**Dateien**](https://fs.fitko.opendesk.live/index.php/apps/files/files/647?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB).
Mit der Lupe können Sie eine Volltextsuche durchführen.

[![Verzeichnisbaum2](/images/fit-standardisierungsboard/artefacts-find-1.png)](/images/fit-standardisierungsboard/artefacts-find-1.png)


Die folgende Tabelle zeigt die übergeordneten Ordner des Verzeichnisses für das FIT-SB (itPLR-24-024_FIT-SB).

|Themen|Inhalt|
|------|------|
|[01_Organisation](https://fs.fitko.opendesk.live/index.php/apps/files/files/777?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/01_Organisation)|Ablageort für organisatorische Themen zum FIT-Standardisierungsboard  (z.B. Sitzungsunterlagen).|
|[02_Arbeitsgruppen](https://fs.fitko.opendesk.live/index.php/apps/files/files/796?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/05_Arbeitsgruppen)|Ablageort für die Arbeitsgruppen des FIT-Standardisierungsboards (z.B. Arbeitsergebnisse aus den Arbeitsgruppen).|
|[03_STDAG_Prozess](https://fs.fitko.opendesk.live/index.php/apps/files/files/797?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/06_STDAG_Prozess)|Ablageort für den Prozess der Standardisierungsagenda (z.B. Prozessbeschreibungen).|
|[04_Themenfelder](https://fs.fitko.opendesk.live/index.php/apps/files/files/798?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/07_Themenfelder)|Ablageort für die Themenfelder des FIT-Standardisierungsboards (z.B. Arbeitsergebnisse aus den Sitzungsthemen).|
|[99_Vorlagen](https://fs.fitko.opendesk.live/index.php/apps/files/files/840?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/99_Vorlagen)|Ablageort für Vorlagen.|
