---
title: Artefakte verwalten
---

In diesem Themenbereich finden Sie umfassende Informationen zur Verwaltung von Artefakten in openDesk, insbesondere hinsichtlich des [Speicherns](save) und [Wiederauffindens](find) dieser Artefakte.
