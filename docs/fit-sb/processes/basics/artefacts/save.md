---
title: Artefakte speichern
---

Arbeitsergebnisse werden in openDesk in [**Projekte**](https://project.fitko.opendesk.live/my/page/) abgelegt, während die dazugehörigen Dateien unter [**Dateien**](https://fs.fitko.opendesk.live/index.php/apps/files) wiederzufinden sind.

Das Speichern von Dateien kann auf unterschiedliche Weise erfolgen:

## Speichern in [**Projekte**](https://project.fitko.opendesk.live/projects/3/?jump=angular)

In Arbeitspaketen lassen sich über den Reiter **Dateien** (rechte Seitenleiste) Dateien hochladen und mit den Arbeitspaketen verknüpfen ([Wo speichern?](#wo-speichere-ich-artefakte-unter-dateien))

[![Reiter_Dateien](/images/fit-standardisierungsboard/artefacts-save-7.png)](/images/fit-standardisierungsboard/artefacts-save-7.png)

## Speichern unter [**Dateien**](https://fs.fitko.opendesk.live/index.php/apps/files)

Navigieren Sie zum passenden Verzeichnis auf der Weboberfläche und laden Sie Ihre Datei über das **+**-Symbol hoch. 

[![Plus_Symbol](/images/fit-standardisierungsboard/Dateiablage_Verzeichnis.png)](/images/fit-standardisierungsboard/Dateiablage_Verzeichnis.png)

Die folgende Tabelle zeigt die übergeordneten Ordner des Verzeichnisses für das FIT-SB [itPLR-24-024_FIT-SB](https://fs.fitko.opendesk.live/index.php/apps/files/files?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB).

|Themen|Inhalt|
|------|------|
|[01_Organisation](https://fs.fitko.opendesk.live/index.php/apps/files/files/777?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/01_Organisation)|Ablageort für organisatorische Themen zum FIT-Standardisierungsboard  (z.B. Sitzungsunterlagen).|
|[02_Arbeitsgruppen](https://fs.fitko.opendesk.live/index.php/apps/files/files/796?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/05_Arbeitsgruppen)|Ablageort für die Arbeitsgruppen des FIT-Standardisierungsboards (z.B. Arbeitsergebnisse aus den Arbeitsgruppen).|
|[03_STDAG_Prozess](https://fs.fitko.opendesk.live/index.php/apps/files/files/797?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/06_STDAG_Prozess)|Ablageort für den Prozess der Standardisierungsagenda (z.B. Prozessbeschreibungen).|
|[04_Themenfelder](https://fs.fitko.opendesk.live/index.php/apps/files/files/798?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/07_Themenfelder)|Ablageort für die Themenfelder des FIT-Standardisierungsboards (z.B. Arbeitsergebnisse aus den Sitzungsthemen).|
|[99_Vorlagen](https://fs.fitko.opendesk.live/index.php/apps/files/files/840?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/99_Vorlagen)|Ablageort für Vorlagen.|

## Wo speichere ich Artefakte unter Dateien?

Ihre Artefakte speichern Sie im passenden Unterordner des Verzeichnisses [**04_Themenfelder**](https://fs.fitko.opendesk.live/index.php/apps/files/files/798?dir=/01_IT-PLR/01_Gremien/itPLR-24-024_FIT-SB/04_Themenfelder).

[![Plus_Symbol](/images/fit-standardisierungsboard/Verzeichnis_Artefakte_speichern.png)](/images/fit-standardisierungsboard/Verzeichnis_Artefakte_speichern.png)

Sollte kein passender Ordner vorhanden sein, legen Sie bitte in Abstimmung mit der FIT-SB-Geschäftsstelle einen neuen Unterordner für das neue Themenfeld an.
