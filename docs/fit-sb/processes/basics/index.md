---
title: Grundlegendes
---
In diesem Abschnitt finden Sie [Informationen zu den Besonderheiten der openDesk-Arbeitsumgebung für das FIT-Standardisierungsboard](specials) (FIT-SB).  
Das Dokument erläutert, wie die verschiedenen Aktivitäten des FIT-SB in Arbeitspaketen organisiert sind (Aufgaben, EPICs und Sitzungsthemen).

Die [Navigationshinweise](basics_sidebar) unterstützen Sie bei der Navigation in der openDesk Arbeitsumgebung.

Im Bereich [Artefakte](artefacts) erhalten Sie umfassende Informationen zur Verwaltung von Artefakten in openDesk, insbesondere hinsichtlich des [Speicherns](artefacts/save) und [Wiederauffindens](artefacts/find) dieser Artefakte.

Schließlich führt die Dokumentation Sie durch die [openDesk-Umgebung für das FIT-Standardisierungsboard](openDesk). Dazu gehört

- die Verwaltung von Dateien
- die Auswahl von Projekten und
- die Nutzung von Boards zur besseren Organisation
- das Speichern und Abrufen von Artefakten in OpenProject und Nextcloud
