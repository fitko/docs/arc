---
title: openDesk
---

:::note[Seite im Aufbau 🚧]

Wir arbeiten für Sie daran, diese Seite mit aktuellen Informationen zu füllen. Bitte schauen Sie später noch einmal vorbei, um weitere Inhalte zu sehen.  
Vielen Dank für Ihre Geduld!

:::

Auf dieser und den folgenden Seiten finden Sie Informationen zur Nutzung von **openDesk**, die speziell auf die Aufgaben und Prozesse des FIT-Standardisierungsboards ausgerichtet sind.

Nachdem sie sich in **openDesk** angemeldet haben, sehen sie die **openDesk**-Arbeitsoberfläche:

![Die openDesk Arbeitsoberfläche](/images/fit-standardisierungsboard/oD-oberflaeche-1.png)

## Allgemeine Beschreibungen

Grundsätzlich steht ihnen als Mitglied des FIT-Standardisierungsboards **openDesk** für folgende Zwecke zur Verfügung:

* als **Dateiablage** (Cloudspeicher) zum Austausch mit anderen FIT-SB-Mitgliedern und der FIT-SB-Geschäftsstelle (klicken Sie dazu auf **Dateien**)
* zum **Erstellen und Bearbeiten von Dateien** mit **Collabora** Online, einem leistungsfähigen Online-Dokumentenbearbeitungsprogramm
* das **Sitzungs- und Aufgabenmanagement** in OpenProject (dazu auf **Projekte** klicken)
* Durchführen von **Video-Konferenzen** per Jitsi (auf **Video-konferenz** klicken)

## Prozessorientierte Beschreibungen

Vor allem finden sie hier Beschreibungen der jeweiligen Vorgehensweise, um folgende Aufgaben des FIT-Standardisierungsboards zu erledigen:

* [Besprechungsthema einreichen](docs/fit-sb/processes/members/meetings/submit.md)
* [Besprechungsthemen einsehen und diskutieren](docs/fit-sb/processes/members/meetings/discuss.md)
* [Organisation der FIT-SB-Sitzungen](../../members/meetings/)
* [Eine FIT-SB-Sitzung organisieren (Zielgruppe Geschäftsstelle)](../../gs/meetings/)
* Bearbeitung eingehender Anfragen (Zielgruppe Geschäftsstelle)

## Sonstige Hinweise

Im Rahmen dieser Dokumentation verwenden wir vorzugsweise deutsche Begriffe.  Unser [Glossar](docs/fit-sb/glossary.md) hält für sie im Falle eines nicht eindeutigen Begriffs die entsprechenden Querverweise bereit.