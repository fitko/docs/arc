---
title: Die Anwendungsgruppe „Management“
---

:::note[Seite im Aufbau 🚧]

Wir arbeiten für Sie daran, diese Seite mit aktuellen Informationen zu füllen. Bitte schauen Sie später noch einmal vorbei, um weitere Inhalte zu sehen.  
Vielen Dank für Ihre Geduld!

:::

In der Anwendungsgruppe _Management_ finden Sie das Modul _Projekte_.

Hinter dem Modul [_Projekte_](https://project.fitko.opendesk.live/my/page) steht [_OpenProject_](https://www.openproject.org/), eine kollaborative Projektmanagement-Software. Es enthält Werkzeuge zur Projektplanung und -steuerung inklusive Boards und Besprechungsmanagement.
Das Förderale IT-Standardisierungsboard (FIT-SB) setzt _OpenProject_ zur Planung, Steuerung und Dokumentation seiner Aktivitäten ein.

[![Modul Projekte](/images/fit-standardisierungsboard/oD-Modul-Management-Projekte.png)](https://project.fitko.opendesk.live/my/page)
