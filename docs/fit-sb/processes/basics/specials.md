---
title: FIT-SB-Besonderheiten
---

In der openDesk Arbeitsumgebung wurden einige Anpassungen vorgenommen, um die Prozesse des Föderalen IT-Standardisierungsboards (FIT-SB) besser zu unterstützen. Auf dieser Seite erfahren Sie, welche das sind.

## Arbeitspakettypen

Grundsätzlich werden die Aktivitäten des FIT-SB in Arbeitspaketen ([siehe auch OpenProjekt Nutzungshandbuch](https://www.openproject.org/de/docs/glossar/#arbeitspaket)) organisiert.
Für die Nutzung im FIT-SB werden folgende speziell angepasste Arbeitspakettypen eingesetzt:

### Sitzungsthema (FIT-SB)

FIT-SB-Sitzungen werden als Besprechungen mit mehreren Sitzungsthemen abgebildet. <br/>
Die einzelnen Sitzungsthemen werden mit dem Arbeitspaket-Typ **Sitzungsthema (FIT-SB)** geplant und dokumentiert.

Nähere Informationen zu den FIT-SB Sitzungen finden unter [Sitzungen aus Sicht der Mitglieder](fit-sb/processes/members/meetings/index.md).

[![Sitzungsthema im Beispiel](/images/fit-standardisierungsboard/OP-Sitzungsthema-Beispiel-blur.png)](/images/fit-standardisierungsboard/OP-Sitzungsthema-Beispiel-blur.png)

### Epic (FIT-SB)

Im Arbeitspaket-Typ **Epic (FIT-SB)** werden mehrere Arbeitspakete thematisch zusammengefasst. In den Epics lassen sich komplexere Zusammenhänge übersichtlich anordnen.
Zum Beispiel werden unter dem Epic **03_STDAG_Prozess** Arbeitspakete und Sitzungsthemen zusammengefasst.

[![Epic im Beispiel](/images/fit-standardisierungsboard/OP-Epic-Beispiel.png)](/images/fit-standardisierungsboard/OP-Epic-Beispiel.png)

### Aufgabe (FIT-SB)

Aufgaben sind ein zentrales Werkzeug für die detaillierte Planung, Bearbeitung und Nachverfolgung von Aktivitäten innerhalb eines Projekts. Sie  bieten Flexibilität und ermöglichen es, Aktivitäten klar zu definieren, Verantwortlichkeiten festzulegen und den Fortschritt im Projekt strukturiert zu verfolgen.
In den [Navigationshinweisen](basics_sidebar.md) werden die unterschiedlichen Darstellungsformen erläutert.

[![Aufgabe (FIT-SB) im Beispiel](/images/fit-standardisierungsboard/OP-Aufgabe-Beispiel-blur.png)](/images/fit-standardisierungsboard/OP-Aufgabe-Beispiel-blur.png)

### FIT-STD, FIT-STD-Verbund, FIT-STD-Modul

FIT-Standards werden in den erweiterten Arbeitspaket-Typen **FIT-STD**, **FIT-STD-Verbund**, **FIT-STD-Modul** abgebildet. Einzelheiten dazu finden Sie im Abschnitt [FIT-Standards](../../index_fit_standards).

## Boards

Für das FIT-SB wurden Boards zur Übersicht konzipiert. Weitere Einzelheiten sind im Abschnitt [Boards zur besseren Übersicht](basics_sidebar/#boards-overview) beschrieben.

## Arbeitspaketansichten

Für die Sitzungen des FIT-SB wurden Arbeitspaketansichten konzipiert. Weitere Einzelheiten sind im Abschnitt [Arbeitspaketansichten im Projekt itPLR-24-024_FIT-SB](basics_sidebar/#arbeitspaketansichten-overview) beschrieben.
