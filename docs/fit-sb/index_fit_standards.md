---
title: FIT-Standards
---
import useBaseUrl from '@docusaurus/useBaseUrl'

Auf dieser Seite dreht sich alles um Föderale IT-Standards (FIT-Standards).

Für FIT-Standards werden drei verschiedene Arbeitspakettypen verwendet:

- Verbünde von FIT-Standards wie beispielsweise **XStandards-Einkauf** setzen sich zusammen aus dem Arbeitspakettyp **FIT-STD-Verbund** und zugehörigen Arbeitspaketypen **FIT-STD-Modul** (im Beispiel XBreitband/XTrasse und XPlanung)
- Unverbundene FIT-Standards werden mit Arbeitspaketen vom Typ **FIT-STD** (beispielsweise DCAT-AP.de oder xdomea) abgebildet.

## Projekt 05_FIT-Standards auswählen

Informationen zu allen FIT-Standards können Sie im Projekt [05_FIT-Standards](https://project.fitko.opendesk.live/projects/12/?jump=angular) einsehen. Wählen Sie das Projekt aus, indem Sie bei der Projektauswahl auf den Eintrag **05_FIT-Standards** klicken.

<a href="https://project.fitko.opendesk.live/projects/12/?jump=angular" target="_blank" rel="noopener noreferrer">
  <img
    decoding="async"
    alt="Projekt auswählen"
    src={useBaseUrl('/images/fit-standardisierungsboard/OP-Projektstruktur_05_FIT-Standards.png')}
    style={{ maxWidth: 400 }}
  />
</a>

### Projektübersicht

Nachdem Sie das Projekt ausgewählt haben, sehen Sie die Projektübersicht:

[![Projektübersicht](/images/fit-standardisierungsboard/Übersichtsseite-05_FIT-Standards.png)](https://project.fitko.opendesk.live/projects/05-fit-standards/)

Die Projektübersicht enthält folgende Informationen:

- Begrüssungstext mit Links zu verschiedenen Ansichten
- Verschiedene statistische Darstellungen, im Einzelnen:
  - Anzahl FIT-Standards je Lebenszyklusphase
  - Anzahl FIT-Standards je Arbeitspakettyp
  - Anzahl FIT-Standards je Veröffentlichungsstatus auf der Informationsplattform
  - Anzahl FIT-Standards entsprechend ihrer Verbindlichkeit
  - Anzahl FIT-Standards entsprechend der jeweiligen Art des Standards
  - Anzahl FIT-Standards entsprechend dem jeweils verwendeten Framework

### Boards im Projekt 05_FIT-Standards {#boards-fit-std}

Die beiden Boards in diesem Projekt vermitteln einen Überblick über die FIT-Standards:

- [FIT-STD Lebenszyklusphasen](https://project.fitko.opendesk.live/projects/05-fit-standards/boards/61) listet die FIT-Standards nach ihrer Lebenszyklusphase auf.
- [FIT-STD Verbünde](https://project.fitko.opendesk.live/projects/05-fit-standards/boards/62) bietet einen Überblick über die Zuordnung der FIT-Standards zu Verbünden.

### Arbeitspaketlisten im Projekt 05_FIT-Standards

In der [Liste FIT-Standards](https://project.fitko.opendesk.live/projects/05-fit-standards/work_packages?query_id=263) sind die FIT-Standards entsprechend ihrer **Lebenszyklusphase** übersichtlich aufgeführt.

[![FIT-Standards nach Lebenszyklusphase](/images/fit-standardisierungsboard/OP-Standards-AP-Liste-hierarchisch.png)](/images/fit-standardisierungsboard/OP-Standards-AP-Liste-hierarchisch.png)

In ihrer **Verbundstruktur** sehen sie FIT-Standards in der Ansicht [alle offenen Arbeitspakete](https://project.fitko.opendesk.live/projects/05-fit-standards/work_packages):

 [![FIT-STDs im Verbund und einzeln](/images/fit-standardisierungsboard/OP-Verbund-APs-Liste.png)](https://project.fitko.opendesk.live/projects/05-fit-standards/work_packages)

### Unterlagen zu den FIT-Standards {#artefacts-fit-std}
Im Order **05_Standards** [(Direktlink)](https://fs.fitko.opendesk.live/apps/files/files?dir=/01_IT-PLR/05_Standards) in der Nextcloud finden Sie die Unterlagen zu den FIT-Standards.

**Ordnerstruktur:** Für jeden FIT-Standard gibt es ein Verzeichnis, das alle Unterlagen zu diesem FIT-Standard nach Lebenszyklusphasen geordnet enthält.
 Alle verbundenen FIT-Standards sind in Ordnern zusammengefasst. Die unverbundenen FIT-Standards finden Sie im Ordner **Ohne Verbund**.
 
 Die Tabelle zeigt ein Beispiel für den Ordneraufbau:

| Verbünde (1. Ebene) | FIT-Standards (2. Ebene) | Lebenszyklusphasen (3. Ebene) |
| - | - | - | 
| XStandards Einkauf | XRechnung | 06_Regelbetrieb und Monitoring |
|  | eForms | 03_Umsetzung |
| Ohne Verbund | DCAT-AP.de | 06_Regelbetrieb und Monitoring |
| | Interoperabler Support-Datenaustausch | 01_ldentifizierung und Bedarfsmeldung
| | XFall | 07_Dekommissionierung |

Die Unterordner für die verschiedenen Lebenszyklusphasen erhalten die Namen aus der Tabelle.

|  Unterordner Lebenszyklusphasen | 
| - |
| 01_ldentifizierung und Bedarfsmeldung |
| 02_Prüfung und Bewertung |
|03_Umsetzung |
|04_Genehmigung |
|05_Überführung in Regelbetrieb |
|06_Regelbetrieb und Monitoring |
|07_Dekommissionierung |
|08_Dekommissioniert |
|09_Zurückgestellt |
|10_Gestoppt-Zurückgezogen |

Näheres zum Lebenszyklusmodell der FIT-Standards finden Sie im [Lebenszyklus-Modell eines IT-Standards](https://docs.fitko.de/standardisierungsagenda/docs/einfuehrung/#lebenszyklus). 

## Informationsplattform für Föderale IT-Standards

Die [Informationsplattform für Föderale IT-Standards](https://docs.fitko.de/fit-standards/?sortby=lifecycle-asc) enthält eine öffentlich einsehbare Auflistung dieser Standards sowie weiterführende Informationen zu jedem dieser Standards.

:::note[Hinweis]

Beachten Sie, dass die öffentlich einsehbare Darstellung perspektivisch nicht so umfassend sein wird wie die im Projekt dargestellten Inhalte.

:::

Weitere Informationen zu FIT-Standards finden Sie in der [Bedienungshilfe der Seite](https://docs.fitko.de/fit-standards/hilfe/#stdag). Dort erfahren Sie mehr über FIT-Standards, insbesondere über das Lebenszyklusmodell, relevante Rollen, die Gültigkeit von FIT-Standards und vieles mehr.
