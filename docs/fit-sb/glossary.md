---
title: Glossar
---

## Anforderungsmanagement {#anforderungsmanagement}

Bedarfsträger sind Behörden, Institutionen oder Unterorganisationen (zum Beispiel Fachabteilungen) dieser Einrichtungen, die feststellen, dass sie Leistungen von Dritten benötigen. Der Bedarfsträger definiert die zu beschaffende Leistung in all ihren fachlichen und technischen Aspekten.
Das Anforderungsmanagement bietet eine Möglichkeit, Fehler zu vermeiden, indem es Änderungen an den Anforderungen verfolgt und die Kommunikation mit den Beteiligten vom Beginn eines Projekts an über den gesamten Entwicklungszyklus hinweg fördert.

Quelle: IBM, „Was ist Anforderungsmanagement?“

## Aufgabenmanagement {#aufgabenmanagement}

Planen, verwalten, überwachen und umsetzen von Aufgabenstellungen.

## Bedarfsmelder {#bedarfsmelder}

Der Bedarfsmelder identifiziert den Bedarf und meldet diesen an das Standardisierungsmanagement oder Standardisierungsboard.

## Bedarfsträger {#bedarfstraeger}

Bedarfsträger sind Behörden, Institutionen oder Unterorganisationen (zum Beispiel Fachabteilungen) dieser Einrichtungen, die feststellen, dass sie Leistungen von Dritten benötigen. Der Bedarfsträger definiert die zu beschaffende Leistung in all ihren fachlichen und technischen Aspekten.

### Bedarfsvertreter {#bedarfsvertreter}

Siehe [Bedarfsträger](#bedarfstraeger)

## Betreiber eines Föderalen IT-Standards {#betreiber-eines-foederalen-it-standards}

In Anlehnung an die EU-Richtlinie 1999/13/EG und im Kontext der Standardisierungsagenda ist ein Betreiber die Organisationseinheit, welche einen Föderalen IT-Standard betreibt oder besitzt oder der die ausschlaggebende wirtschaftliche Verfügungsmacht über den technischen Betrieb des IT-Standards übertragen worden ist.

## Betriebskonzept {#betriebskonzept}

Der Begriff „Betriebskonzept“ beschreibt die Strukturen und Prozesse für das Management eines Föderalen IT-Standards, einschließlich der Zusammenarbeit zwischen den Stakeholdern und der Aufnahme von Änderungswünschen.

## Definition of Done {#definition-of-done}

Das Dokument „Definition of Done (DoD)“ stellt einen Meilenstein in der Standardisierungsagenda dar. Im Prozessframework folgt die DoD nach der Realisierung einer Standardisierungsbedarfs durch das Umsetzungsteam. Sie bildet in Form einer Checkliste ab, ob die gestellten Aufgaben erledigt wurden und beinhaltet die Abnahmekriterien.

Es wird nach Abstimmung mit dem Umsetzungsteam vom Auftraggeber/Bedarfsträger ausgefüllt und bildet damit die Grundlage für eine Abnahme (Akzeptanz) des gelieferten Standards.

## Definition of Ready {#definition-of-ready}

Ziel des Dokuments „Definition of Ready (DoR)" ist es, die Erwartungshaltung eines Bedarfsträgers zu einem Standardisierungsbedarf anhand von Fragen in einem Dokument zu beschreiben und zu erläutern. Dadurch wird Einvernehmen zwischen dem Bedarfsträger und dem Standardisierungsboard über die genauen Anforderungen an das beschriebene Standardisierungsprojekt hergestellt.

### DoD {#dod}

Siehe [Definition of Done](#definition-of-done)

### DoR {#dor}

Siehe [Definition of Ready](#definition-of-ready)

### FIT-AB {#fit-ab}

Siehe [Föderales IT-Architekturboard](#foederales-it-architekturboard)

### FIT-SB {#fit-sb}

Siehe [Standardisierungsboard](#standardisierungsboard)

### FIT-Standard {#fit-standard}

Siehe [Föderale IT-Standards](#foederale-it-standards)

### FITKO {#fitko}

Siehe [Föderale IT-Kooperation](#foederale-it-kooperation)

## Föderale IT-Kooperation {#foederale-it-kooperation}

Die FITKO ist die zentrale Koordinierungs- und Vernetzungsstelle für Digitalisierungsvorhaben der öffentlichen Verwaltung in Deutschland. Sie bündelt die Kompetenzen und Ressourcen, die es braucht, um die Verwaltungsdigitalisierung voranzutreiben, und sorgt als agile Organisation des IT-Planungsrates dafür, dessen Entscheidungen umzusetzen.

## Föderale IT-Standards {#foederale-it-standards}

Der IT-Planungsrat ist bei der Verwaltungsdigitalisierung im Kontext von fachunabhängigen oder fachübergreifenden Themen über den IT-Staatsvertrag angehalten, Standards verbindlich vorzugeben. Um diese Standards von anderen Bereichen abzugrenzen, wird ein solcher Standard als „Föderaler IT-Standard“ bezeichnet – kurz „FIT-Standard“.

## Föderales Entwicklungsportal {#foederales-entwicklungsportal}

Das Föderale Entwicklungsportal ist der zentrale Ort für technische Dokumentation, Entwicklungsressourcen und Implementierungsbeispiele zu Standards und Schnittstellen der föderalen IT-Infrastruktur von Bund und Ländern.

## Föderales IT-Architekturboard {#foederales-it-architekturboard}

Das föderale IT-Architekturboard unterstützt und berät den IT-Planungsrat in strategischen Fragen bei der Entwicklung und Umsetzung von Maßnahmen der IT-Architektur.

Zu den Aufgaben des Boards gehören u. a. die Beschreibung und Fortführung föderaler IT-Architekturrichtlinien, die Initiierung von Architekturvorhaben sowie die kurzfristige Bereitstellung dringend benötigter Lösungen im Rahmen der Umsetzung des Onlinezugangsgesetzes. Das Architekturboard begleitet zudem mit sog. „Projektpaten“ Infrastrukturprojekte mit Auswirkungen auf die föderale IT-Architektur.

Die FITKO hat das föderale Architekturboard initiiert und den Vorsitz inne. Die Abteilungsleiterrunde und der IT-Planungsrat sollen ohne vorherige Einbindung des Boards keine Entscheidungen mit Relevanz für die föderale IT-Architektur treffen.

### IT-AB {#it-ab}

Siehe [Föderales IT-Architekturboard](#foederales-it-architekturboard)

### IT-PLR {#it-plr}

Siehe [IT-Planungsrat](#it-planungsrat)

## IT-Planungsrat {#it-planungsrat}

Der IT-Planungsrat fungiert als zentrales politisches Steuerungsgremium zwischen Bund und Ländern in Fragen der Informationstechnik und der Digitalisierung von Verwaltungsleistungen. Er fördert und entwickelt gemeinsame nutzer:innenorientierte IT-Lösungen und ebnet so den Weg für eine effiziente, sichere und gut vernetzte digitale Verwaltung in Deutschland.

Durch die Beschlüsse des IT-Planungsrates erhalten Bund und Länder eine verbindliche Grundlage für die gemeinsamen Föderalen Digitalisierungsaktivitäten.

Quelle: Website des IT-Planungsrates

## IT-Staatsvertrag {#it-staatsvertrag}

Vertrag über die Errichtung des IT-Planungsrats und über die Grundlagen der Zusammenarbeit beim Einsatz der Informationstechnologie in den Verwaltungen von Bund und Ländern – Vertrag zur Ausführung von Artikel 91c GG

Quelle: Website des Bundesministeriums des Innern und für Heimat

## Informationsplattform {#informationsplattform}

Die Informationsplattform der FITKO gibt einen Überblick über die Föderalen IT-Standards (FIT-Standards) des IT-Planungsrates und berücksichtigt den gesamten Lebenszyklus – von der Bedarfsmeldung über die Umsetzung und den Regelbetrieb bis hin zur endgültigen Dekommissionierung.

## Lebenszyklus eines IT-Standards {#lebenszyklus-eines-it-standards}

Der Lebenszyklus eines IT-Standards beschreibt die Phasen der Entstehung und Pflege von Föderalen IT-Standards und unterstützt damit effizient operative Entscheidungsbedarfe.

## Ready for Service {#ready-for-service}

Die Ready for Service Anzeige wird vom Umsetzungsteam erstellt und zeigt allen Beteiligten an, dass der umgesetzte IT-Standard betriebs-/einsatzbereit ist.

## Request for Decommissioning {#request-for-decommissioning}

Beim Request for Decommissioning (RfD) handelt es sich um ein Dokument.
Eine RfD wird vom STDB erstellt. Zweck des Dokuments ist die Empfehlung an den IT-PLR, einen Föderalen IT-Standard zu dekommissionieren.

### RfD {#rfd}

Siehe [Request for Decommissioning](#request-for-decommissioning)

### RfS {#rfs}

Siehe [Ready for Service](#ready-for-service)

### STDAG {#stdag}

Siehe [Standardisierungsagenda](#standardisierungsagenda)

### STDAG-Prozess {#stdag-prozess}

Siehe [Lebenszyklus eines IT-Standards](#lebenszyklus-eines-it-standards)

## Stakeholdermanagement {#stakeholdermanagement}

Der Begriff Stakeholder setzt sich aus zwei englischen Worten zusammen:

 - dem Wort „Stake“, was so viel heißt wie „Anteil“,
 - und „holder“, zu Deutsch „Besitzer/Anteilseigner“

Mit Stakeholder sind alle Personen oder Gruppen gemeint, für welche das vorliegende Projekt relevant ist.
Stakeholdermanagement ist eine wichtige Aufgabe im Projektmanagement. Denn die Personengruppen sind in irgendeiner Form am Projekt interessiert oder davon betroffen und können Einfluss auf Entscheidungen nehmen.

Stakeholdermanagement beschreibt den Prozess rund um das Ermitteln der Bedürfnisse der wichtigsten Interessensgruppen und das Berücksichtigen der dabei gewonnenen Erkenntnisse im Rahmen der Projektplanung und -durchführung.

## Standardisierungsagenda {#standardisierungsagenda}

Die Standardisierungsagenda dient der Priorisierung und Steuerung von Aktivitäten sowie Schaffung von Transparenz rund um den Lebenszyklus Föderaler IT-Standards.

## Standardisierungsbedarf {#standardisierungsbedarf}

Standardisierungsbedarfe sind vereinfacht ausgedrückt Anwendungsbereiche im bund-/länderübergreifenden Datenaustausch, für die per Beschluss des IT-Planungsrates ein einheitlicher IT-Interoperabilitätsstandard vorgegeben werden soll.
Ein Standardisierungsbedarf wird durch den sogenannten Bedarfsträger verantwortlich bearbeitet. Unterstützung erfährt er dabei in der Regel durch ein Fachgremium, das zu diesem Zwecke durch ihn konstituiert wird.

## Standardisierungsboard {#standardisierungsboard}

Das Standardisierungsboard verwaltet Standardisierungsbedarfe und berät zum Lebenszyklus von IT-Standards.

### Taskmanagement {#taskmanagement}

Siehe [Aufgabenmanagement](#aufgabenmanagement)

## Transitionsplan {#transitionsplan}

Transition bezeichnet die Überführung eines Föderalen IT-Standards in den Regelbetrieb. Die Transitionsphase beginnt im Anschluss an die Umsetzung, nach Zustimmung durch das Standardisierungsboard (DoD erfüllt) und Freigabe durch den IT-Planungsrat.

Ein geordneter Transitionsprozess stellt dabei sicher, dass der umgesetzte IT-Standard reibungslos in einen zuverlässigen Regelbetrieb übernommen wird. Zu diesem Prozess gehören Analyse, Planung und Implementierung in den Betrieb.

Der Transitionsplan beschreibt diesen Prozess, einschliesslich der durchzuführenden Aktivitäten.

## Umsetzungsteam {#umsetzungsteam}

Ein Umsetzungsteam setzt einen angemeldeten und genehmigten Standardisierungsbedarf um und überführt diesen gemeinsam mit dem Betreiber in den Regelbetrieb.

