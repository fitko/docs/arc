---
title: Anleitung FIT-Standardisierungsboard
sidebar_label: Föderales IT-Standardisierungsboard
slug: /fit-sb/
hide_table_of_contents: true

---
import SitemapSvg from '/images/fit-standardisierungsboard/FIT-SB_Dokumentation-Navigation.svg'

Auf dieser und den folgenden Seiten finden Sie Nutzungshinweise zum Umgang mit openDesk, die speziell auf die Aufgaben und Prozesse des FIT-Standardisierungsboards (FIT-SB) ausgerichtet sind.

## Navigation auf dieser Webpräsenz

Verwenden Sie zur Navigation das Menü in der linken Seitenleiste (auf kleinen Bildschirmen öffnen Sie zunächst das Menü durch Anklicken des **☰**-Symbols) oder klicken Sie auf das Element in der Grafik, zu dem Sie Informationen wünschen.
Alle hinterlegten Informationen sind prinzipiell für jede beteiligte Person geeignet.

### Informationen zur Sitemap-Grafik

Die Sitemap-Grafik erlaubt eine schnelle und direkte Navigation zu allen Themenbereichen dieser Webpräsenz.  
In der oberen Hälfte befinden sich prozessorientierte Beschreibungen im Umfeld des Standardisierungsboards für Föderale IT-Standards. Der linke Quadrant fokussiert dabei die Prozesse für FIT-SB-Mitglieder, der rechte entsprechend für die FIT-SB-Geschäftsstelle (FIT-SB-GS).  
Die untere Hälfte enthält allgemeine Grundlagen zu FIT-SB-spezifischen Anpassungen im Umfeld von openDesk, OpenProject und Nextcloud sowie zwei konkrete, praktische Beispiele für die Erledigung FIT-SB-typischer Aufgaben.

<div className="svg-border svg-max-width-900">
  <SitemapSvg />
</div>
