---
title: Verwaltungsabkommen zur Umsetzung des OZG
---

:::info Zusammenfassung
Der Bund, die Länder und Kommunen haben sich darauf verständigt bei der Digitalisierung von Verwaltungsleistungen zu kooperieren.
Im Zuge dieses Verwaltungsabkommens werden Ziele, Umfang und Rahmenbedingungen die dieser Kooperation festgehalten.
:::

-----

# Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes

Die Bundesrepublik Deutschland vertreten durch das Bundesministerium des Innern, für Bau und Heimat - nachfolgend „Bund“ genannt – und das Land Baden-Württemberg, der Freistaat Bayern, das Land Berlin, das Land Brandenburg, die Freie Hansestadt Bremen, die Freie und Hansestadt Hamburg, das Land Hessen, das Land Mecklenburg-Vorpommern, das Land Niedersachsen, das Land Nordrhein-Westfalen, das Land Rheinland-Pfalz, das Saarland, der Freistaat Sachsen, das Land Sachsen-Anhalt, das Land Schleswig-Holstein, der Freistaat Thüringen, - nachfolgend „Länder“ genannt bzw. Bund und Länder werden auch gemeinsam oder einzeln als „Kooperationspartner“ bezeichnet - schließen folgendes Verwaltungsabkommen:

## Präambel
Durch das Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen (Onlinezugangsgesetz – OZG) vom 14.08.2017 sind Bund und Länder verpflichtet, bis spätestens zum Ablauf des fünften auf die Verkündung des OZG folgenden Kalenderjahres – mithin bis zum 31.12.2022 – ihre Verwaltungsleistungen auch elektronisch über Verwaltungsportale anzubieten (§ 1 Abs. 1 OZG).

Hierdurch soll für Bürgerinnen und Bürger von Bund und Ländern ein barriere- und medienbruchfreier Zugang zu allen elektronischen Verwaltungsleistungen von Bund und Ländern (einschließlich Kommunen) geschaffen werden (§ 3 Abs. 1 OZG).

Die Kooperationspartner streben im Rahmen dieser Vereinbarung die kooperative, einheitliche, zukunftsweisende und effiziente Umsetzung des OZGs an.
Die beabsichtigte Zusammenarbeit der Kooperationspartner auf föderaler Ebene ist in Art. 91 c GG ausdrücklich vorgesehen. 
Bund und Länder können bei der Planung, der Errichtung und dem Betrieb der für ihre Aufgabenerfüllung benötigten informationstechnischen Systeme zusammenwirken. 
Nach der Rechtsprechung des EuGH ist anerkannt, dass öffentliche Stellen ihre im Allgemeininteresse liegenden Aufgaben mit ihren eigenen Mitteln und auch in Zusammenarbeit mit anderen öffentlichen Stellen erfüllen können. 
Die Organisation von Zuständigkeiten bestimmter Aufgaben innerhalb der Mitgliedsstaaten unterliegt insofern nicht dem Vergaberecht. 
Die angestrebte Kooperation mehrerer öffentlicher Auftraggeber zur Erreichung gemeinsamer Ziele ist zudem unter den in § 108 Abs. 6 GWB beschriebenen Voraussetzungen vom Anwendungsbereich des Vergaberechts ausdrücklich ausgenommen. 
Gleiches gilt unter den in § 108 Abs. 1 bis 5 GWB beschriebenen Voraussetzungen auch für verschiedene sog. In-House-Konstellationen.

Es wird ein interdisziplinäres Arbeiten, eine agile Arbeitsweise, arbeitsteiliges Vorgehen und die konsequente Nutzerzentrierung zugrunde gelegt. 
Die konsequente Digitalisierung erfolgt nach dem Modell „Einer für Alle/Einer für Viele“. So wird sichergestellt, dass die an einer Stelle entwickelten und betriebenen Online-Dienste von allen Kooperationspartnern kostengünstig genutzt werden können. 
Die Nutzerfreundlichkeit der digitalen Angebote ist das oberste und handlungsleitende Digitalisierungsprinzip.

Wesentlicher Bestandteil der Umsetzung des OZG ist eine moderne technische Infrastruktur, über die Länder (einschließlich Kommunen) digitale und nutzerfreundliche Verwaltungsleistungen anbieten können. 
Die Bundesregierung stellt im Rahmen des Konjunkturpakets zusätzliche Finanzmittel in Höhe von drei Mrd. Euro zur Verfügung, um schnell ein flächendeckendes digitales Verwaltungsangebot in Deutschland zu schaffen und dabei die Länder gezielt zu entlasten. 
Die Digitalisierung der Verwaltungsleistungen in Bund und Ländern setzt ein leistungsfähiges System digitaler Plattformen voraus. 
Online-Dienste müssen schnell und mit hochwertiger Nutzerführung erstellt und betrieben werden. 
Zugleich haben die Kooperationspartner die Anschlussfähigkeit aller Länder (einschließlich Kommunen) sowie die Anbindung der Fachverfahren zu gewährleisten. 
Die teilweise noch fragmentierte IT-Landschaft soll zu einem leistungsfähigen, interoperablen Plattformsystem ausgebaut werden.
Der aus dem Kooperationsvertrag entstehende Sach- und Personalaufwand in den Kommunen ist beachtlich. 
Die durch diesen Vertrag bei ihnen veranlassten Mehrausgaben werden von den Ländern ausgeglichen.

## § 1 Gegenstand und Ziel der Kooperation {#p1}
1. Bund und Länder schließen diese Kooperationsvereinbarung, um für die Umsetzung des OZG informationstechnische Lösungen gemeinsam zu entwickeln und dauerhaft zu betreiben. 
Das OZG verpflichtet Bund und Länder, bis spätestens Ende des Jahres 2022 ihre Verwaltungsleistungen auch elektronisch über Verwaltungsportale anzubieten und ihre Verwaltungsportale miteinander zu einem Portalverbund zu verknüpfen. 
Bund und Länder verfolgen insofern gemeinsame Ziele.

2. Ziel der Kooperation ist es, im Zusammenwirken die im öffentlichen Interesse liegenden Aufgaben zur Digitalisierung von Verwaltungsverfahren umzusetzen, die Ebenen übergreifend im Bund sowie in den Ländern relevant sind. 
Dabei streben die Kooperationspartner insbesondere die zügige Umsetzung des OZGs sowie die Weiterentwicklung der teilweise noch heterogenen föderalen IT-Landschaft zu einem leistungsstarken, interoperablen Plattformsystem der digitalen Verwaltung an.

## § 2 Grundsätze und Prinzipien der Kooperation {#p2}
1. Die Kooperation ist von folgenden Grundsätzen und Prinzipien geleitet:

    a) dem übereinstimmenden Willen zur vertrauensvollen Zusammenarbeit bei der Umsetzung des OZG

    b) der ausgewogenen Berücksichtigung der jeweiligen Interessen der Kooperationspartner und dem Verständnis, dass die jeweiligen Leistungen und Beiträge der Kooperationspartner nicht allein bezogen auf einzelne Maßnahmen und Handlungsfelder, sondern bezogen auf die Umsetzung des OZGs im Sinne eines Gesamtvorhabens zu bewerten sind

    c) der Abstimmung der Handlungsschritte zur Zielerreichung der unter § 3 angelegten konkreten Handlungsfelder zwischen den Kooperationspartnern

    d) der Sicherstellung des Einsatzes von durch die Kooperationspartner für die Zusammenarbeit zur Verfügung gestellten Ressourcen zur Erreichung der gemeinsamen Ziele

    e) des kontinuierlichen Austausches über laufende Aktivitäten im Bereich der Digitalisierung, so dass ein kontinuierlicher Wissensaustausch über die Aktivitäten im Bereich Digitalisierung zwischen den Kooperationspartnern entsteht

    f) dem Willen im Konfliktfall eine konsensuale Lösung in den IT-Planungsrat-Strukturen zu finden.

2. Die originären Zuständigkeiten, Aufgaben und Verantwortungsbereiche der 
Kooperationspartner werden durch diesen Kooperationsvertrag nicht berührt.

## § 3 Konkrete Handlungsfelder der Kooperation {#p3}
Die jeweils zuständigen Kooperationspartner legen die konkreten Handlungsfelder für ihre Zusammenarbeit zur Umsetzung des OZG jeweils in Einzelvereinbarungen fest. 
Hierbei werden Art und Umfang sowie weitere Details zur vereinbarten Zusammenarbeit für die einzelnen Maßnahmen verbindlich festgeschrieben. 
Die jeweilige Einzelvereinbarung soll insbesondere Regelungen zu Leistungen und zu den jeweiligen Beiträgen der Kooperationspartner, der Finanzierung, des Controlling/Programmstruktur und Laufzeit enthalten.

## § 4 Organisation der Kooperation {#p4}
1. Die Gesamtkoordinierung und strategische Steuerung erfolgt über den Bund in Abstimmung mit den für die OZG-Umsetzung relevanten Gremien.

2. Im Rahmen der Kooperation werden die Gremienstrukturen der Bundesverwaltung sowie die föderalen Gremienstrukturen des IT-Planungsrats und der Fachministerkonferenzen beteiligt.

## § 5 Leistungen und Beiträge der Kooperationspartner {#p5}
1. Die Partner verpflichten sich, jeweils Kooperationsbeiträge für einzelne Arbeitspakete zu leisten. 
Die Beiträge werden in Einzelvereinbarungen präzisiert. 
Die Beiträge können insbesondere darstellen:

   - Übergreifende fachliche Konzeption und strategische Steuerung bei Vorhaben der Verwaltungsdigitalisierung oder des E-Government (Fachkonzeptionen für Digitalisierungsprogramme und Register, Steuerung von Themenfeldarbeit und Nachnutzung, digitale Infrastruktur, Registerarchitektur),

   - Entwicklung/ Implementierung von Software und Plattformen/ Registern (technische Infrastruktur und Basiskomponenten/-dienste),

   - Entwicklung/ Implementierung innovativer Technologien,

   - Design von bürgerfreundlichen Diensten (Forschung, Entwicklung, Testen),

   - Entwicklung und langfristige Sicherstellung von Betrieb und Weiterentwicklung der Online-Dienste,

   - Schaffung der Voraussetzungen für Nachnutzung der „Einer für Alle “-Online-Dienste,

   - Beteiligung am Aufbau der digitalen Infrastruktur, der arbeitsteiligen Bereitstellung von interoperablen Basiskomponenten und der Definition von Standards.

2. Den Kooperationspartnern ist es unter Beachtung der einschlägigen rechtlichen Voraussetzungen möglich, für die ihnen obliegenden Leistungsanteile Aufträge an Dritte zu erteilen. 
Die Einhaltung der gesetzlichen Regelungen (insb. des Vergabe- und Beihilferechts) liegt in der Verantwortung des jeweils beauftragenden Kooperationspartners.

## § 6 Ergänzende Finanzierung aus Mitteln des Konjunkturpakets {#p6}
Im Rahmen dieser Vereinbarung können Maßnahmen im Sinne des § 4 OZG aus Mitteln des Konjunkturpakets des Bundes finanziert werden.

1. Diese Maßnahmen müssen zu einer flächendeckenden Verfügbarkeit von Online-Diensten für Nutzerinnen und Nutzer führen.

2. Die Mittel werden ausschließlich für konkrete Umsetzungsprojekte eingesetzt. 
Es werden keine Mittel pauschal zugewiesen.

3. Für den Einsatz der Mittel gelten die folgenden Grundprinzipien:

    a) Die Digitalisierung erfolgt nach der Priorisierung, die das Programmmanagement, die Ressorts und die federführenden Länder festgelegt haben (Relevanz).

    b) Die Methode „Digitalisierungslabor“ wird grundsätzlich bei der Digitalisierung von durch Lebenssachverhalte zusammenhängenden Verwaltungsleistungen (sog. Nutzerreisen) angewandt und die Vorteile der Registermodernisierung werden genutzt (Nutzerfreundlichkeit und Once-Only-Prinzip).
    
    c) Die Kooperationspartner müssen die zeitlichen Vorgaben einhalten (Geschwindigkeit).

    d) Die Mittel werden ausschließlich eingesetzt für „Einer für Alle“-Lösungen („Einer für Alle“/ Wirtschaftlichkeit), d.h. die umsetzenden Länder wenden die Nachnutzungsstandards an.

    e) Die Mittel werden nur für nachhaltige und zukunftsfähige Lösungen genutzt (Innovation und technische Qualität).

    f) In der Entwicklung und im Betrieb sind nach Möglichkeit offene Standards zu nutzen. 
    Der Quellcode wird nach Möglichkeit als Open Source zur Verfügung gestellt, d.h. in nachnutzbarer Form (Offene Standards und Open Source).

4. Weitere Einzelheiten zur Finanzierung werden jeweils in den Einzelvereinbarungen geregelt.

## § 7 Laufzeit und Kündigung {#p7}
Die Kooperationsvereinbarung tritt einen Tag nach Zeichnung aller Kooperationspartner in Kraft und wird auf unbestimmte Zeit geschlossen. 
Die Kooperationspartner können die Kooperationsvereinbarung nicht mit Wirkung zu einem früheren Zeitpunkt als dem 31.12.2022 kündigen. 
Die Kündigungsfrist beträgt sechs Monate zum Ende eines Kalenderjahres. 
Die Kündigung muss schriftlich erfolgen.

## § 8 Allgemeine Bestimmungen {#p8}
1. Sofern die Länder in Bezug auf die Regelungen in § 6 dieses Abkommens bzw. die Einzelvereinbarungen die zeitlichen und inhaltlichen Vorgaben nur anteilig erfüllen, können eventuell überzahlte Mittel des Bundes zurückgefordert oder verrechnet werden. 
Zurückerstattete Mittel sollen für andere OZG-Leistungen verwendet werden.

2. Nebenabreden, Änderungen und Ergänzungen dieser Vereinbarung sind nur im Einvernehmen zwischen den Kooperationspartnern möglich und bedürfen der Schriftform.

3. Sollten einzelne Bestimmungen dieser Vereinbarung ganz oder teilweise unwirksam oder unanwendbar sein oder werden, so wird die Gültigkeit der übrigen Bestimmungen hiervon nicht berührt. 
Die Kooperationspartner verpflichten sich, in einem solchen Fall an der Schaffung von Bestimmungen mitzuwirken, durch die ein der nichtigen oder unwirksamen Bestimmung rechtlich oder wirtschaftlich möglichst nahekommendes Ergebnis rechtswirksam erzielt wird. 
Dasselbe gilt für etwaige Regelungslücken.

4. Sind Bestimmungen dieser Vereinbarung auslegungs- oder ergänzungsbedürftig, so hat die Auslegung oder Ergänzung in der Weise zu erfolgen, dass sie dem Geist, Inhalt und Zweck dieser Vereinbarung bestmöglich gerecht wird. 
Dabei soll diejenige Regelung gelten, die die Beteiligten bei Abschluss dieser Vereinbarung getroffen hätten, wenn sie die Auslegungs- oder Ergänzungsbedürftigkeit erkannt hätten.

-----

## Primärquellen
- [Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes auf der Webseite des Onlinezugangsgesetzes](https://www.onlinezugangsgesetz.de/SharedDocs/downloads/Webs/OZG/DE/dachabkommen-vorabversion.pdf?__blob=publicationFile&v=1)
