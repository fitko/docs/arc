---
title: Föderale IT-Architekturrichtlinien
---

:::info Zusammenfassung
Architekturrichtlinien helfen Architekturentscheidungen bei der Entwicklung von Softwarelösungen effizient zu treffen und wiederkehrende Grundsatzdiskussionen zu vermeiden.
Somit werden Auswahlprozesse beschleunigt, Fehlentscheidungen reduziert und Beschlussergebnisse vereinheitlicht.
Das [Architekturboard](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard) definiert die föderalen IT-Architekturrichtlinien und entwickelt diese weiter.
Die Richtlinien gelten für alle laufenden und neuen Projekte und Vorhaben, die Einfluss auf die föderale IT-Landschaft haben.

In der aktuellen Fassung werden die strategischen Architekturrichtlinien definiert (siehe [Kapitel 5](#5-strategische-architekturrichtlinien)).
Die Einhaltung dieser Vorgaben dient unmittelbar der Erreichung der vereinbarten Ziele zur Digitalisierung der Verwaltung.

Die verpflichtende Anwendung der Architekturrichtlinien wurde vom IT-Planungsrat mit [Beschluss 2021/37](https://www.it-planungsrat.de/beschluss/beschluss-2021-37) verbindlich festgelegt.

Feedback und Anregungen nehmen wir gerne auch über den [Issue-Tracker zur Föderalen IT-Infrastruktur](https://gitlab.opencode.de/fitko/feedback/-/issues/?label_name%5B%5D=F%C3%B6derale%20IT-Architekturrichtlinien) entgegen.
:::

-----

# Föderale IT-Architekturrichtlinien
Version 1.0, Stand: 29.10.2021

## 1 Management Summary
Die vorliegende Version dieses Dokuments enthält eine erste Fassung übergreifender strategischer Architekturrichtlinien. 
Es ist beabsichtigt bei der weiteren Fortschreibung die strategischen Architekturrichtlinien um weitere domänenspezifische Architekturrichtlinien zu ergänzen. 
Diese Architekturrichtlinien leiten sich aus trategischen föderalen Zielen und Gesetzen/Verordnungen ab. 
Sie sind für eine Vielzahl von föderalen Szenarien einsetzbar, auch wenn sie voraussichtlich zunächst bei der Umsetzung des Onlinezugangsgesetzes und der Registermodernisierung genutzt werden. 
Die vorliegende Version der Architekturrichtlinien kann ab sofort für ausgewählte Infrastrukturprojekte und -anträge verprobt werden.

Das Architekturboard überprüft anhand der Architekturrichtlinie alle architekturrelevanten Entscheidungsvorlagen hinsichtlich der Einhaltung der beschlossenen Architekturrichtlinien. 
Hierfür müssen Infrastrukturanträge aus dem Konjunkturpaket entsprechende Beschreibungen umfassen und aufzeigen, wie die Projekte beabsichtigen, die strategischen Architekturrichtlinien zu berücksichtigen.
Evtl. Abweichungen zu diesen Architekturrichtlinien sind zu begründen und können maximal als Übergangslösungen vom föderalen Architekturboard akzeptiert werden.

Die Architekturrichtlinien können auf Antrag der Abteilungsleiterrunde/ des IT-Planungsrates angepasst und ergänzt werden. 
Das Architekturboard kann ebenfalls Änderungen an den Architekturrichtlinien vorschlagen und diese weiterentwickeln. 
Die Prüfung der Anträge wird innerhalb des Architekturboards erfolgen. 
Sobald die vorliegende Fassung über die notwendige Reife verfügt, werden die Architekturrichtlinien gemäß dem Dokument „Zusammenwirken im föderalen Architekturboard (Organisation)“ der AL-Runde zur Beschlussfassung vorgelegt.

## 2 Einleitung
Mit Beschluss der AL-Runde wurde zum 22.02.2021 das föderale IT-Architekturboard [^1] als neues Steuerungsgremium des IT-Planungsrats errichtet [^2]. 
Das Architekturboard setzt sich zum Ziel, die föderale Digitalisierungsinfrastruktur ganzheitlich und planvoll weiterzuentwickeln. 
Die in diesem Dokument aufgeführten Architekturrichtlinien sind ein wesentliches Instrument, diese Weiterentwicklung zu steuern.
Unter Architekturrichtlinien werden Leitlinien verstanden, die bei der Entwicklung und dem Betrieb der föderalen IT-Architektur Orientierung geben.

[^1]: In diesem Dokument wird der Begriff „Architekturboard“ verwendet.
[^2]: Siehe https://www.it-planungsrat.de/SharedDocs/Startseitenmeldungen/DE/Startseite_IT_Architekturboard.html

>**Begriffsbestimmung**  
*Architekturrichtlinien sind Entscheidungshelfer für den Entwurf und die Entwicklung von IT-Architekturen. 
Sie orientieren sich an Zielen, rechtlichen Vorgaben und Nutzen für die Verwaltung.
Sie bilden die Basis für einheitliche und nachvollziehbare Entscheidungsprozesse.*

Architekturrichtlinien helfen Architekturentscheidungen effizient zu treffen und wiederkehrende Grundsatzdiskussionen zu vermeiden. 
Somit werden Auswahlprozesse beschleunigt, Fehlentscheidungen reduziert und Beschlussergebnisse vereinheitlicht.

Das Architekturboard befasst sich mit der Gestaltung der föderalen IT-Architektur und nicht mit der internen IT-Architektur der Beteiligten (Bund, Länder und Kommunen). 
Dementsprechend wird sich das Architekturboard mit föderalen Architekturrichtlinien befassen. 
Das Dokument enthält im [Kapitel 3.1](#31-betrachtete-dimensionen) eine Konkretisierung, was unter dem Begriff „föderal relevant“ verstanden wird.

### 2.1 Zielgruppe

Das vorliegende Dokument richtet sich an folgende Personen:
-  Mitglieder des Architekturboards als Entscheidungshilfe bei der Gestaltung der föderalen IT-Architektur und bei der Bewertung von Vorschlägen und Anträgen, die an das Architekturboard gerichtet sind.
- Vom Architekturboard benannte Experten, die bei der Gestaltung der föderalen Architektur unterstützen.
- Antragssteller, die Mittel aus dem Konjunkturpaket beantragen, um die föderale Infrastruktur durch gemeinsame Dienste oder Interoperabilität zu verbessern. [^3]
- Personen (Bund, Land, Kommunen), die Anträge und Vorschläge unterschiedlicher Art an das Architekturboard richten.
- Projektleiter und Chefarchitekten, die föderale Infrastrukturprojekte verantworten und diese konform zu den IT-Architekturrichtlinien gestalten.
- Produktverantwortliche für Anwendungen des IT-Planungsrats, die beabsichtigen diese Anwendungen auf eine Konformität zu den Architekturrichtlinien zu ertüchtigen.

[^3]: Siehe [Entscheidung IT-Planungsrat 2020/39 – OZG-Umsetzung (Digitalisierung von Verwaltungsleistungen) Konjunkturpaket](https://www.it-planungsrat.de/beschluss/beschluss-2020-39)

### 2.2 Vorgehen
Die vorliegende Version des Dokuments wurde durch eine Arbeitsgruppe des föderalen Architekturboards erarbeitet. 
Sie basiert auf bestehenden Architekturrichtlinien des Bundes und der Länder, z. B. der Architekturrichtlinie des Bundes und der „Referenzarchitektur zur Umsetzung des OZG“ der ALD (Arbeitsgemeinschaft, Leiter der Datenzentralen).

### 2.3 Aufbau des Dokuments
Die Architekturrichtlinien liegen mit der aktuellen Fassung in einer ersten Version vor. 
Es ist beabsichtigt die Architekturrichtlinien laufend, kooperativ und bedarfsorientiert anzupassen, siehe dazu [Kapitel 7](#7-anpassung-und-weiterentwicklung-der-architekturrichtlinien).

Die Inhalte des vorliegenden Dokuments sind wie folgt gegliedert:
- [Kapitel 3](#3-umfang-und-rahmenbedingungen) beschreibt den Umfang (Geltungsbereich) der Architekturrichtlinien aus unterschiedlichen Perspektiven und legt wesentliche Rahmenbedingungen für deren Verwendung fest.
- Kapitel 4 enthält ein Metamodell zur Strukturierung der Architekturrichtlinien, eine Vorlage für deren Dokumentation sowie eine Klassifizierung unterschiedlicher Verbindlichkeitsgrade.
- Kapitel 5 enthält eine Auflistung der übergreifenden, strategischen Architekturrichtlinien
- Kapitel 6 beschreibt, wie die Architekturrichtlinien in der operativen Arbeit im Architekturboard von IT-Architekten und Projektleitern angewendet werden können.
- Kapitel 7 schließlich beschreibt, wie die vorliegenden Architekturrichtlinien fortgeschrieben und wie Anträge für Anpassungen der Architekturrichtlinien gestellt werden können.

## 3 Umfang und Rahmenbedingungen
Dieses Dokument beschreibt in einer ersten Fassung strategische Architekturrichtlinien. 
Sie basieren auf der Zielsetzung, alle Verwaltungsleistungen bis 31.12.2022 Bürger:innen und der Wirtschaft auch digital anzubieten.
Das föderale IT-Architekturboard achtet auf die Einhaltung dieser Richtlinien. 
Die Zusammenarbeit des Architekturboards mit Gremien und föderalen Infrastrukturprojekten zeigt [Abbildung 1](#abbildung-1):

<a name="abbildung-1"></a>

![Die Organisation des föderalen Architekturboards](/images/foederale-it-architekturrichtlinien/01-foederales-it-architekturboard.png)

Das Architekturboard ist mit umfassenden Kompetenzen versehen. Aufgaben und Organisation des Architekturboards sind ausführlich im Dokument „Zusammenwirken im föderalenArchitekturboard (Organisation)“ [^4] beschrieben.

[^4]: Siehe Ablage Architekturboard Ordner 01_Organisation/01_Organisationsdokument

Der IT-Planungsrat legt für einen definierten Zeitraum Ziele fest. 
Diese Ziele beziehen sich auf die Digitalisierung der öffentlichen Verwaltung auf föderaler Ebene. 
Zur Erreichung dieser Ziele wird eine strategische Vorgehensweise festgelegt. 
Diese Strategie wird von der Runde der Abteilungsleiter verfolgt. 
Entsprechend den Vorgaben der Strategie entscheidet die Abteilungsleiterrunde über Projekte, Produkte und über neue Standardisierungsvorhaben.

Das Architekturboard richtet die strategischen Architekturrichtlinien an den Ziel- und strategischen Vorgaben aus. 
Es unterstützt den IT-Planungsrat bzw. die Runde der Abteilungsleiter bei Architekturentscheidungen, gibt Empfehlungen ab, begleitet Projekte und berät das Produktmanagement zu Weiterentwicklungen bestehender Produkte. 
Das Architekturboard sorgt für die Einhaltung der Architekturrichtlinien und ist insofern als ein strategisches Instrument anzusehen, welches wesentlich zur Erreichung der festgelegten Ziele beiträgt.

Das bei der FITKO eingerichtete Portfolio-Board führt das strategische Controlling aus. 
Hierzu werden die Daten aus den Bereichen Standards, Projekte und Produkte erhoben, ausgewertet und der Abteilungsleiterrunde/ dem IT-Planungsrat zur Verfügung gestellt.

### 3.1 Betrachtete Dimensionen
Der Zielkorridor für die Anwendung der föderalen Architekturrichtlinien ergibt sich aus der additiven Betrachtung der folgenden Dimensionen, siehe [Abbildung 2](#abbildung-2).

<a name="abbildung-2"></a>

![Dimensionen mit Einfluss auf die föderalen Architekturrichtlinien](/images/foederale-it-architekturrichtlinien/02-dimensionen-far.png)

#### Strategische Ziele:
- S1 Digitale Souveränität: Der IT-Planungsrat hat am 17. März 2021 die „Strategie zur Stärkung der Digitalen Souveränität für die IT der öffentlichen Verwaltung“ beschlossen ([Beschluss 2021/09](https://www.it-planungsrat.de/beschluss/beschluss-2021-09)). 
Digitale Souveränität wird hier definiert als „die Fähigkeiten und Möglichkeiten von Individuen und Institutionen, ihre Rolle(n) in der digitalen Welt selbstständig, selbstbestimmt und sicher ausüben zu können“. 
Dabei werden drei strategische Ziele verfolgt:
  - Wechselmöglichkeit: Die Öffentliche Verwaltung hat die Möglichkeit einer freien Wahl bzw. eines flexiblen Wechsels zwischen IT-Lösungen, IT-Komponenten und Anbietern
  - Gestaltungsmöglichkeit: Die Öffentliche Verwaltung hat die Fähigkeit ihre IT (mit-)gestalten zu können und 
  - Einfluss auf Anbieter: Die Öffentliche Verwaltung kann ihre Anforderungen und Bedarfe (z. B. hinsichtlich Produkteigenschaften, Verhandlung und Vertragsgestaltung) gegenüber Technologieanbietern artikulieren und durchsetzen.
- S2: Green-IT: Der IT-Planungsrat hat eine Koordinationsgruppe Green-IT eingerichtet, mit dem Ziel eine Green-IT-Strategie zu erarbeiten. 
Über die KG Green-IT sollen entsprechend bundesweite Mindestanforderungen an einen nachhaltigen und ressourcenschonenden IT-Einsatz definiert werden ([Beschluss IT-PLR 2021/11](https://www.it-planungsrat.de/beschluss/beschluss-2021-11)).
- S3: Digitale Verwaltung: Die Digitalisierung der Verwaltungsleistungen folgt grundsätzlich den im Eckpunktepapier des Konjunkturprogramms formulierten sechs Grundprinzipien „Relevanz“, „Nutzerfreundlichkeit“, „Geschwindigkeit“, „Einer für Alle/Wirtschaftlichkeit“, „Innovation und nachhaltige technische Qualität“, „Offene Standards und Open Source“, siehe Beschluss [IT-PLR 2020/39](https://www.it-planungsrat.de/beschluss/beschluss-2020-39).
- S4: Verwaltung als Plattform: Ziel ist es, eine funktionierende föderale Plattform (Infrastruktur) für die flächendeckende Digitalisierung zu schaffen. 
Diese Plattform(en) stellen Services zur Verfügung, die Bund und Länder nutzen können. 
Dabei steht die Verbindung bestehender Systeme und Plattformen in Bund und Ländern im Vordergrund. 
Dieses Zusammenspiel wird auf der Grundlage von Interoperabilität sichergestellt. 
Das European Interoperability Framework betrachtet Interoperabilität auf rechtlicher, organisatorischer, semantischer und technischer Ebene. 
Um eine funktionierende Interoperabilität aller Plattformen zu ermöglichen, müssen alle Ebenen berücksichtigt werden. 
Auf Grundlage der föderalen Plattform sollen Verwaltungsleistungen (Online-Dienste) nach dem „Einer für Alle Paradigma“ zentral entwickelt und betrieben werden können und zur direkten Nachnutzung bereitgestellt werden. 
Es soll die Möglichkeit für externe Akteure (Wirtschaft) bieten, Zusatzleistungen anzubieten, ohne dabei die digitale Souveränität Deutschlands zu gefährden. 
- S5: Open Data fördern: Daten sind der Rohstoff der Digitalisierung. 
Open Data liegt die Idee zugrunde, dass die Erhebung dieser Daten durch öffentliche Stellen im Auftrag der Bürger erfolgt und somit der Zugang und die Nutzung diesen frei stehen. 
Darin ist auch ein Gewinn an Transparenz von staatlichem Handeln zu sehen. 
Das E-Government Gesetz des Bundes und die E-Government Gesetze bzw. Transparenzgesetze vieler Länder legen fest, dass Behörden Daten zum Abruf über öffentliche Netze bereitzustellen haben, sofern diese nicht geschützt werden müssen. 
Für die Veröffentlichung der Daten wurde GovData als Anwendung des IT-Planungsrat eingeführt (siehe Beschluss IT-PLR [2014/20](https://www.it-planungsrat.de/beschluss/beschluss-2014-20) und [2021/18](https://www.it-planungsrat.de/beschluss/beschluss-2021-18)). 
Verantwortlich für das Portal ist die Geschäfts- und Koordinierungsstelle GovData mit Sitz in Hamburg.

#### Normative Vorgaben (Gesetze und Verordnungen):
Im Folgenden werden die wesentlichen normativen Vorgaben (Gesetze und Verordnungen) genannt. 
Aufgeführt werden Vorgaben mit unmittelbarem Bezug zur Digitalisierung der Verwaltung des Bundes und der Länder genannt. 
Weitere Gesetze ohne unmittelbaren Bezug, wie z. B. die Datengrundschutzverordnung (DGSVO) die für alle IT-Umsetzungsprojekte von Relevanz ist, werden aus Gründen der Übersichtlichkeit nicht aufgeführt. 
- R1: Onlinezugangsgesetz (OZG) - Das Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen (Onlinezugangsgesetz – OZG) verpflichtet Bund, Länder und Kommunen, bis Ende 2022 ihre Verwaltungsleistungen über Verwaltungsportale auch digital anzubieten.
- R2: Single Digital Gateway Verordnung (SDG) - Das Europäische Parlament und der Europäische Rat haben im Jahr 2018 beschlossen, mit dem Single Digital Gateway (SDG) ein einheitliches digitales Zugangstor zur Verwaltung in der EU zu schaffen.
- R3: Registermodernisierungsgesetz - Das Registermodernisierungsgesetz umfasst die Einführung und Verwendung einer Identifikationsnummer in der öffentlichen Verwaltung und zur Änderung weiterer Gesetze. 
Mit dem Registermodernisierungsgesetz kann das "Once-Only"-Prinzip verwirklicht werden. 
Bereits in Registern gespeicherte Angaben und Nachweise müssen dann nicht erneut vorgelegt werden. 
Zudem wird die Qualität der Registerdaten nachhaltig gesteigert.
- R4: E-Government-Gesetze: Bund und Länder haben E-Government-Gesetze verabschiedet. 
Die Inhalte der E-Government-Gesetze variieren je Bundesland. 
Grundsätzliches Ziel ist es, einen einfachen digitalen Zugang zur öffentlichen Verwaltung zu schaffen sowie interne Verwaltungsabläufe zu digitalisieren.
- R5: Verwaltungsverfahrensgesetze: Die Verwaltungsverfahrensgesetze des Bundes und der Länder regeln alles, was die Verwaltung tut und wie sie es tun darf. 
Es enthält allgemeine Verfahrensgrundsätze, die für alle Behörden gelten. 
Hierunter fallen z. B. die Tätigkeiten einer Behörde, die erforderlich sind, um einen Verwaltungsakt [^5] zu erlassen.

[^5]: Siehe z. B. https://www.bmi.bund.de/SharedDocs/glossareintraege/DE/v/verwaltungsakt.html

#### Fachübergreifende Dienste:
Für eine Festlegung des Umfangs der föderalen IT ist es sinnvoll die IT entsprechend des länderübergreifenden Wiederverwendungspotenzials aufzuteilen. 
Für diesen Zweck wird der Begriff Dienst verwendet. 
Unter Dienst wird eine logische Einheit verstanden, die einen definierten Umfang an funktionalen Anforderungen erfüllt. 
Mit Hilfe des Dienstebegriffs kann die föderale IT beschrieben werden, ohne konkret auf die spezifischen IT-Lösungen von Bund und Ländern einzugehen. 
So gibt es z. B. deutschlandweit mehrere IT-Lösungen, die den Dienst „Nutzerkonto“ realisieren. 
Die nachfolgende Aufteilung ist angelehnt an das das IT-Rahmenkonzept Bund [^6]:

[^6]: Diese Definitionen sind angelehnt an das IT-Rahmenkonzept Bund, siehe 
https://www.cio.bund.de/SharedDocs/Publikationen/DE/Architekturen-und-Standards/rahmenarchitektur_itsteuerung_bund_grundlagen_download.pdf?__blob=publicationFile

- Fu1: Fachdienste unterstützen eine ressortspezifische Fachlichkeit. Fachdienste können nicht ressortübergreifend wiederverwendet werden.
-  Fu2: Querschnittsdienste unterstützen eine ressortübergreifende Fachlichkeit. Beispiele: E-Rechnung und Beschaffungsplattformen. 
Eine ressortübergreifende Wiederverwendung ist möglich. 
- Fu3: Basisdienste bieten Funktionen an, die übergreifend und unabhängig vom spezifischen fachlichen Kontext eingesetzt werden können. 
Obwohl die Basisdienstfunktionalität nach fachlichen Gesichtspunkten konfiguriert werden kann, wird die Lösung ohne spezifische fachliche (ressortspezifische) Kenntnisse betrieben und weiterentwickelt. 
Beispiele: Kollaborationswerkzeuge und E-Mail-Server. 
Das Wiederverwendungspotential ist höher als bei Querschnittsdiensten, da die Basisdienste nicht nur einen spezifischen fachlichen Prozess unterstützen können. 
- Fu4: Infrastrukturdienste bieten Funktionen an, die als Fundament für Entwicklung und Betrieb von Anwendungen und den Datenaustausch dienen. 
Beispiele: Server-, Netzinfrastrukturen, Betriebssysteme und Applikationsserver. 
Das Wiederverwendungspotenzial ist höher als bei Basisdiensten, da sie eine Unterstützung für beliebig viele Funktionen anbieten. 
So kann z. B. der gleiche Applikationsserver für Kollaborationswerkzeuge und E-Mail verwendet werden. 

Der primäre Fokus liegt auf den Querschnittsdiensten, Basisdiensten und Infrastrukturdiensten, da hier das Wiederverwendungspotenzial am höchsten ist. 
Fachdienste werden nur teilweise behandelt. 
Für Fachdienste liegt der Fokus auf der verwendeten Plattform und auf generischen Vorgaben. 
Die realisierte Fachlichkeit wird nicht betrachtet, wie etwa die Gestaltung einzelner Online-Dienste. 
Plattformen für Fachdienste sollten ressortübergreifende Querschnittsdienste, Basisdienste und Infrastrukturdienste wiederverwenden und keine konkurrierenden IT-Lösungen einsetzen, wie z. B. ressortspezifische Nutzerkonten.

#### Föderal relevant:
Der Umfang des föderalen Architekturboard ist auf föderal relevante Dienste begrenzt. Diese sind:
- Fö1: Zentrale Dienste, die länderübergreifend eingesetzt werden. Beispiele: Portalverbund Online-Gateway, einheitliches Unternehmenskonto und NdB-Verbindungsnetz
- Fö2: Interoperabilitätskonzepte und -lösungen zur Verknüpfung von regionalen Diensten. 
Die Festlegungen zur Interoperabilität können Prozesse, Semantik und Technologien
umfassen, vgl. das European Interoperability Framework (EIF)[^7].
Beispiele: Standardisierte Schnittstellen zu E-Payment-Diensten und zentrales Verzeichnis mit Information über regionale E-Payment-Dienste.

[^7]: European Interoperability Framework, siehe https://ec.europa.eu/isa2/eif_de

### 3.2 Ganzheitliche Architekturbetrachtung
Für die Gestaltung der föderalen IT-Architektur in Deutschland ist eine ganzheitliche Betrachtung notwendig. 
Die folgenden Architekturdomänen orientieren sich am Architekturframework TOGAF[^8], siehe auch [Abbildung 3](#abbildung-3). 
Alle Architekturdomänen müssen bei einer Architekturgestaltung
berücksichtigt werden:
- Geschäftsarchitektur, d. h. fachliche Prozesse, Akteure und Organisationseinheiten
- Informationssystemarchitektur, d. h. Anwendungen und Daten von Informationssystemen
- Technische Architektur, d. h. die unterstützende technische Infrastruktur

<a name="abbildung-3"></a>

![Architekturdomänen am Beispiel des TOGAF Content-Framework Metamodell](/images/foederale-it-architekturrichtlinien/03-togaf.png)

[^8]: Siehe https://www.opengroup.org/togaf

### 3.3 Abgrenzung
Das vorliegende Dokument umfasst eine Beschreibung der Architekturrichtlinien. 
Deren konkrete Verwendung wird separat im Rahmen des noch zu erstellenden Dokumentes “Dokumentation von Prozessen zur Gestaltung der Architektur” beschrieben.

## 4 Struktur der Architekturrichtlinien
Die Strukturierung der Architekturrichtlinien orientiert sich an den Architekturrichtlinien Bund und ist in [Abbildung 4](#abbildung-4) aufgeführt.

<a name="abbildung-4"></a>

![Struktur der Architekturrichtlinien](/images/foederale-it-architekturrichtlinien/04-struktur-ar.png)

Abgeleitet aus den strategischen Zielen und normativen Vorgaben werden übergreifende strategische Architekturrichtlinien festgehalten. 
Diese dienen wiederum als Ausgangspunkt für eine Ableitung von spezifischen Architekturrichtlinien, rechts in der [Abbildung 4](#abbildung-4). 
Diese spezifischen Architekturrichtlinien beziehen sich auf föderal relevante und fachübergreifende Aspekte und umfassen folgende Domänen:

1. Dienste: Hier werden Architekturrichtlinien festgehalten, die sich auf einzelne Dienste beziehen. 
Ein Dienst ist eine logische Einheit, die einen definierten Umfang an funktionalen Anforderungen erfüllt. 
Betrachtet werden Infrastrukturdienste, Basisdienste und Querschnittsdienste und nicht Fachdienste, siehe auch [Kapitel 3](#3-umfang-und-rahmenbedingungen).
2. Information: Hier werden Architekturrichtlinien aufgeführt, die sich auf die Semantik derInformationsobjekte, z. B. im Umfeld Nachrichtenaustausch zwischen Systemen beziehen. 
3. Technik: Architekturrichtlinien umfassen technische Vorgaben, z. B. hinsichtlich der Nutzung spezifischer technischer Standards wie z. B. technische Protokolle. 
4. Informationssicherheit, Datenschutz und Geheimschutz: Wegen der hohen Relevanz für die öffentlichen Verwaltungen werden diese Architekturrichtlinien als eigene Domäne 
festgehalten.

In der vorliegenden Version werden die **strategischen Architekturrichtlinien** behandelt. 
Diese sind bei der Konkretisierung der weiteren Domänen zu beachten und einzuhalten. 
Sie stellen den strategischen Rahmen der Architekturentwicklung dar. 
Die Einhaltung dieser Vorgaben dient unmittelbar der Erreichung der vereinbarten Ziele zur Digitalisierung der Verwaltung. 
Die strategischen Architekturvorgaben können sich aufgrund neuer Zielsetzungen ändern, sind also nicht als unveränderbar anzusehen.

Die spezifischen Architekturrichtlinien werden in der vorliegenden Version dieses Dokuments nicht weiter spezifiziert, sondern sind für weitere Ausbaustufen der Architekturrichtlinie vorgesehen.

#### Vorlage zur Beschreibung der Architekturrichtlinien
Um die praktische Arbeit mit den Architekturrichtlinien zu erleichtern, wird jede Architektur im Folgenden durch einen eindeutigen Bezeichner, einen aussagekräftigen Titel sowie den Verbindlichkeitsgrad gekennzeichnet. 
Für eine einheitliche Darstellung und Lesbarkeit der in diesem Dokument beschriebenen Architekturrichtlinien wurde die folgende, aus dem TOGAF Framework adaptierte, Formatvorlage genutzt.

| Bezeichner: Titel der Architekturrichtlinie | Verbindlichkeitsgrad |
|---------------------------------------------|----------------------|
| Beschreibung                                | Kurze Beschreibung der Architekturrichtlinie.|
| Begründung                                  |Darstellung der geschäftlichen und technologischen Vorteile der Architekturrichtlinie.|
| Abhängigkeiten                              |Beschreibung der Beziehungen zu anderen Architekturrichtlinien und der Erläuterung, unter welchen Umständen einer konkurrierenden Architekturrichtlinie Priorität eingeräumt wird.|
| Implikationen                               |Beschreibung der geschäftlichen und technologischen Auswirkungen der Architekturrichtlinie auf die Behörden und Dienstleister (z. B. in Bezug auf IT-Systeme, Ressourcen, Kosten und Aktivitäten/ Aufgaben).                      |
| Beispiele für die Anwendung                 | Beispiele für die Anwendung der Architekturrichtlinie|
| Bezug zu Zielen und Gesetzen                |Verlinkung zu Zielen und Gesetzen aus der sich die Architekturrichtlinie ableitet.|

Zur Verringerung des Interpretationsspielraums und somit besseren Verständlichkeit für die Anwenderin und den Anwender des vorliegenden Dokuments wird zur Beschreibung der Architekturrichtlinie eine einheitliche Beschreibungssemantik verwendet. 
Diese orientiert sich an RFC 2119[^9] und sieht zur Beschreibung des Verbindlichkeitsgrads einer Architekturvorgabe grundsätzlich vier Abstufungen vor, die in der nachfolgenden Tabelle verdeutlicht werden. 
Allerdings wird auf die Option „SOLL“ verzichtet, da auch für „MUSS“-Anforderungen begründete Abweichungen und Ausnahmegenehmigungen zulässig sind.

[^9]: Weiterführende Informationen finden Sie unter https://tools.ietf.org/html/rfc2119;

| Begriff 	| Begriffsdefinition                        	| Anmerkung 	|
|---------	|-------------------------------------------	|-----------	|
| MUSS    	| „MUSS“ kennzeichnet eine Aussage mit dem Charakter einer verbindlichen Festlegung.	|Evtl. Abweichungen von der Vorgabe müssen schriftlich begründet und durch das Architekturboard geprüft und genehmigt werden. Auch die Einhaltung muss dokumentiert und begründet werden.|
| KANN    	| „KANN“ kennzeichnet eine Aussage mit dem Charakter einer gestatteten Option.|Diese Architekturrichtlinien werden vom Architekturboard stark empfohlen|
| DARF NICHT| „DARF NICHT“ kennzeichnet eine Aussage mit dem Charakter eines absoluten Verbots, z. B. veraltete Technologien oder Standards|Die Einhaltung muss dokumentiert und begründet werden.|

Die Prüfung der Einhaltung der „MUSS“ und „DARF NICHT“-Kriterien erfolgt durch das 
Architekturboard auf Grundlage der Dokumentenlage.

## 5 Strategische Architekturrichtlinien
Nachstehend werden die strategischen Architekturrichtlinien beschrieben. 

### SR1: Verwendung von Standards {#sr1}

| SR1: Verwendung von Standards | MUSS |
| -----	| -----	|
| Beschreibung| Eine der Hauptaufgaben des IT-Planungsrates ist die Bereitstellung fachübergreifender Standards (vgl. Staatsvertrag). Der IT-Planungsrat hat bereits einige Standards verbindlich verabschiedet. Sie sind daher konsequent anzuwenden. Bedarfe für neue Standards sind über die bei der FITKO geführte Standardisierungsagenda anzumelden.<br/><br/> Standards werden auf alle architekturrelevante Ebenen gemäß European Interoperability Framwork (EIF) verwendet. Betrachtet werden die Ebenen Prozesse, Semantik und Technik sofern föderal relevant. Standards dienen u. a. dazu, Daten effizient zwischen und innerhalb der föderalen Ebenen digital auszutauschen. Ergänzend dazu müssen Standards betrachtet werden, die für eine einheitliche Qualität der IT-Architektur sorgen, z. B. zum Thema IT-Sicherheit und Nutzerorientierung. <br/><br/>Unter Standards werden auch einheitliche Methoden betrachtet, sofern diese für die Gestaltung der föderalen IT-Architektur relevant sind. |
| Begründung| Für ein effizientes Zusammenspiel der IT-Lösungen in Deutschland bedarf es einer Standardisierung auf der föderalen Ebene (horizontal Land - Land und vertikal – Bund, Land, Kommune). Diese orientieren sich in erster Linie an Aspekten, die eine föderale Interoperabilität gewährleisten. Standardisierte Methoden vereinfachen den Austausch zwischen Projektbeteiligten und sorgen für eine effiziente Projektdurchführung.|
| Abhängigkeiten| Eine Verwendung von Standards unterstützt die Architekturrichtlinie ([SR9](#sr9)): Gewährleistung der Interoperabilität von IT-Lösungen. Auch kann das Ziel Wiederverwendung ([SR2](#sr2)) unterstützt werden, wenn einzelne Anwendungen oder Komponenten als Standards erklärt werden. Bestehende Marktstandards sind zu verwenden ([SR3](#sr3)) sofern Herstellerunabhängigkeit gewährleistet ist ([SR7](#sr7)). Bestimmte Standards im Umfeld Nutzereinbindung ([SR6](#sr6)), IT-Sicherheit ([SR4](#sr4)) und Open Data ([SR13](#sr13)) sind ebenfalls zu berücksichtigen. |
| Implikationen| Die konsequente Anwendung einheitlicher Standards und Methoden beschleunigt die Digitalisierung signifikant. Dies unterstützt zudem den föderalen Staatsaufbau, wobei alle Länder und der Bund eigene Architekturen betreiben können. Anpassungen sind nur dort vorzunehmen, wo ein föderaler Datenaustausch erforderlich wird. Einheitliche Standards und Methoden führen zu minimalen Aufwänden.<br/><br/> Anforderungen föderaler Infrastrukturprojekte können zur Erweiterung bestehender Standards oder der Entwicklung und Einführung neuer Standards führen.<br/><br/> Das föderale Architekturboard wird eine Liste der relevanten Standards veröffentlichen und regelmäßig aktualisieren[^10]}. |
| Beispiele für die Anwendung| Sicherstellung föderal einheitliche Architekturnotation z. B. durch den Einsatz von Archimate|
| Bezug zu Zielen und Gesetzen|S1: Digitale Souveränität, S3: Digitale Verwaltung, S4 Verwaltung als Plattform, S5: Open Data|

[^10]:Sobald diese Standards veröffentlicht sind, werden auf diese Standards in diesem Dokument verlinkt.

### SR2: Sicherstellung von Wiederverwendung {#sr2}

| SR2: Sicherstellung von Wiederverwendung | MUSS |
| -----	| -----	|
| Beschreibung| Bei der Neu- und Weiterentwicklung von IT-Lösungen und -Diensten soll die Wiederverwendung von Komponenten/Modulen anderer IT-Lösungen und Dienste geprüft werden. Geeignete Komponenten/Module sollen als Bestandteil der zu entwickelnden Lösung integriert werden. Bei der Abgrenzung des Funktionsumfangs und der qualitativen Eigenschaften von Komponenten/Modulen soll speziell deren Wiederverwendbarkeit im Kontext mehrerer Lösungen berücksichtigt und gewährleistet werden. |
| Begründung| Die systematische Wiederverwendung von (Teil-)Lösungen in einer IT- Landschaft vermeidet unnötige Redundanzen und konzeptionell unterschiedliche Lösungen derselben Problemstellung und trägt damit wesentlich zur Reduzierung des Aufwands und der Kosten für die Entwicklung, die Wartung und den Betrieb von Lösungen bei. Durch spezifische Maßgaben hinsichtlich des Designs von (Teil-)Lösungen kann die Wiederverwendbarkeit optimiert werden (Design for Reuse)|
| Abhängigkeiten| Die Architekturrichtlinien SR9 (Interoperabilität), SR10 (Lose Kopplung/Modularität) bilden die Grundlage für die Schaffung von wiederverwendbaren IT-Lösungen. Allerdings darf die Schaffung einer wiederverwendbaren IT-Lösung nicht dazu führen, dass die IT-Lösung übermäßig komplex wird, siehe [SR12](#sr12) oder die Herstellerunabhängigkeit gefährdet (S7). Bei der Auswahl einer IT-Lösung, die wiederverwendet werden kann, ist ebenfalls diese IT-Lösung auf Eignung zu prüfen, siehe z. B. [SR3](#sr3) (Bestehende Marktstandards verwenden)|
| Implikationen| Die Wiederverwendung und Wiederverwendbarkeit von Komponenten/Modulen wird bei Neu- und Weiterentwicklungen durch das föderale Architekturboard geprüft.|
| Beispiele für die Anwendung| Einsatz „Einer für Alle“, d. h. Schaffung eines Vorgehens und einer Infrastruktur, die eine deutschlandweite Wiederverwendung von Online-Diensten ermöglicht.|
| Bezug zu Zielen und Gesetzen|S3: Digitale Verwaltung (Grundprinzip: „Einer für Alle/Wirtschaftlichkeit“)|

### SR3: Bestehende Marktstandards verwenden {#sr3}

| SR3: Bestehende Marktstandards verwenden | MUSS |
| -----	| -----	|
| Beschreibung| Bestehende Marktstandards sind zu verwenden.|
| Begründung| Bestehende Marktstandards haben eine breite Nutzungsbasis, die über die öffentliche Verwaltung in Deutschland hinaus geht. Sie öffnen Möglichkeiten einer schnelleren Umsetzung, bewährter sowie von Nutzern bekannten und somit akzeptierten IT-Lösungen sowie die Umsetzung neuer bisher ohne den Marktstandards nicht umsetzbaren fachlichen Anforderungen.|
| Abhängigkeiten| Die Marktstandards dürfen das Ziel einer digitalen Souveränität in Deutschland nicht gefährden. Auch ist eine Abwägung mit der Architekturrichtlinie SR2 (Sicherstellung von Wiederverwendung) notwendig.|
| Implikationen| Bei Einführung neuer IT-Lösungen sollen diese auf bestehende Marktstandards setzen, vgl. Gartner Quadranten. Bestehende IT-Lösungen und Standards sind regelmäßig auf eine Ertüchtigung zu bestehenden Marktstandards auszurichten.|
| Beispiele für die Anwendung| Maschine zu Maschine Kommunikation auf der Grundlage von RESTful WebServices. <br/><br/> Einsatz von IPv6|
| Bezug zu Zielen und Gesetzen| S3: Digitale Verwaltung (Grundprinzip: "Nutzerfreundlichkeit“ und „Innovation und nachhaltige technische Qualität“)|

### SR4: Sichere Systemgrundkonfiguration („Security-by-Default“ und “Privacy-by-Default”) {#sr4}

| SR4: Sichere Systemgrundkonfiguration („Security-by-Default“ und “Privacy-by-Default”) | MUSS |
| -----	| -----	|
| Beschreibung| Alle relevanten Sicherheitseinstellungen müssen bereits in der Grundkonfiguration des Dienstes aktiviert sein (Security-by-Default und „Privacy-by-Default“)|
| Begründung| Nur wenn bereits in der Grundkonfiguration eines Dienstes relevante Sicherheitsfestlegungen getroffen werden (z. B. DENY ALL Regeln), kann sichergestellt werden, dass unautorisierte Zugriffe (z. B. bei Nicht-Verfügbarkeit von Sicherheitsdiensten) verhindert werden.|
| Abhängigkeiten| Abhängigkeiten bestehen grundsätzlich zu allen Architekturrichtlinien.|
| Implikationen| Die Architekturrichtlinie ist bei Entwurf, Entwicklung, Bereitstellung und Einsatz von Diensten zu berücksichtigen.|
| Beispiele für die Anwendung|  Frühzeitige Einbindung von IT-Sicherheitsexperten, frühzeitige Festlegung der Anforderungen an IT-Sicherheit z. B. auf der Grundlage von IT-Grundschutzvorgaben (BSI). Firewallkonfiguration entsprechend der Vorgabe „Deny all by default“.|
| Bezug zu Zielen und Gesetzen| R4: E-Government-Gesetze, R5: Verwaltungsverfahrensgesetze|

### SR5: API-First Ansatz {#sr5}

| SR5: API-First Ansatz | MUSS |
| -----	| -----	|
| Beschreibung| Bei der Entwicklung von neuen IT-Lösungen und Interoperabilitätskonzepten sind diese nach dem API-First Ansatz zu konzipieren. Konkret bedeutet dies, dass zuerst die Schnittstellen spezifiziert, mit allen Beteiligten getestet und abgestimmt werden. Erst danach erfolgt die Umsetzung der konkreten IT-Lösungen.|
| Begründung| Dies führt zu einer Vermeidung von Missverständnissen bzgl. des Funktionsumfangs und zur frühzeitigen Klärung von offenen fachlichen Fragen. Außerdem kann mit diesem Ansatz die Integration neuer Lösungen in bestehende Architekturenfrühzeitig verprobt werden.|
| Abhängigkeiten| Mit dem API-First Ansatz werden die Wiederverwendbarkeit von IT-Lösungen ([SR2](#sr2)) und die Interoperabilität ([SR9](#sr9)) gefördert. Ebenfalls ermöglicht es eine lose Kopplung/Modularität ([SR10](#sr10)) und reduziert durch wohldefinierte Schnittstellen die Komplexität der Gesamtlösung ([SR12](#sr12)). Schnittstellen eignen sich in besonderem Maße zur Standardisierung ([SR1](#sr1)).|
| Implikationen| Die Architekturrichtlinie fordert eine frühzeitige Festlegung der notwendigen Schnittstellen sowie deren Abstimmung mit allen Beteiligten. Die Schnittstellen müssen im fachlichen Kontext entlang eines Prozesses oder ggf. Sequenzdiagramms genau verortet werden können. Die im Kontext der OZG-Umsetzung erarbeitete Landkarte für Kommunikationsbeziehungen (Landkarte Standards und Schnittstellen) ist zu prüfen und zu ergänzen. Während der Umsetzung der Lösungen sollen frühzeitig auf Grundlage der Schnittstellen anzubindende Lösungen für eine Anbindung ertüchtigt werden. Es soll möglich sein, während der Umsetzung Änderungswünsche an die Schnittstellenspezifikation zu formulieren. Geplante und umgesetzte Schnittstellen werden an zentraler Stelle veröffentlicht und im Rahmen eines definierten Releasemanagements versioniert. Alte Versionen werden in einer Übergangszeit unterstützt und dann abgeschaltet. Schnittstellen sind sicher zu gestalten und sollen - sofern sinnvoll - auch für externe Akteure (Wirtschaft etc.) verfügbar gemacht werden. Im OZG-Kontext können z. B. Online-Dienste ergänzend zu Formularen als API-Schnittstellen bereitgestellt werden. Externe Akteure können diese Schnittstellen bei der Entwicklung von integrierten Lösungen nutzen, um somit die Verbreitung der Verwaltungsleistungen und Nutzerakzeptanz zu erhöhen.|
|Beispiele für die Anwendung| Definition von standardisierten Schnittstellen, um länderübergreifende Interoperabilität zu ermöglichen, z. B. im Kontext OZG-Einführung und Registermodernisierung. Einsatz z.B. von OpenAPI für eine konkrete und leicht verständliche Schnittstellenbeschreibung. Frühzeitige Festlegung der Schnittstellen („Schnittstellenverträge“)|
| Bezug zu Zielen und Gesetzen| S3: Digitale Verwaltung (Grundprinzipen „Geschwindigkeit“, „Nutzerfreundlichkeit“, „Innovation und nachhaltige technische Qualität“), S4: Verwaltung als Plattform|

### SR6: Sicherstellung der Nutzereinbindung („Usability by Design“) {#sr6}

| SR6: Sicherstellung der Nutzereinbindung („Usability by Design“) | MUSS |
| -----	| -----	|
| Beschreibung| Die Gestaltung und Bedienung von IT-Lösungen muss für jeden Benutzenden einfach, einheitlich und intuitiv sein. Die zugrundeliegenden Bedienungskonzepte müssen den Anwendenden bekannt und transparent sein. Insbesondere müssen muss hierbei der Industriestandard DIN EN ISO 9241 beachtet werden.|
| Begründung| Wenn die zugrundeliegenden Bedienkonzepte einer IT-Lösung den Benutzerinnen und Benutzern unbekannt sind, wird deren Produktivität negativ beeinträchtigt. Benutzerfreundliche IT-Lösungen erreichen eine höhere Akzeptanz bei den Anwendenden und fördern ein produktiveres Arbeiten, eine höhere Qualität und verringern Fehlbedienungen durch die Anwendenden. Aufwände und Kosten werden gespart, da die Bedienung ähnlich zu anderen, gängigen IT-Lösungen erfolgt, der Schulungsaufwand begrenzt und das Risiko einer unsachgemäßen Bedienung der IT-Lösung gering ist.|
| Abhängigkeiten| Ausgehend von dieser Architektur-Richtlinie können föderal verbindliche Standards abgeleitet werden ([SR1](#sr1)). Die Architekturrichtlinie kann zu einem Konflikt mit der Erfüllung von IT-Sicherheitsanforderungen führen ([SR4](#sr4)). Es ist fallbezogen eine gute Abwägung zwischen den Zielen der Nutzerorientierung und IT-Sicherheit zu finden, so dass z. B. die konzipierte Lösung sowohl sicher ist als auch von den Nutzern akzeptiert wird.|
| Implikationen| Eine frühzeitige und laufende Einbindung der zukünftigen Nutzer ist sicherzustellen. Es werden Aspekte wie Sprache, Lokation, Barrierefreiheit, körperliche Einschränkungen der Anwendenden (z. B. Sehschwäche, beschränkte Möglichkeit Tastatur und Maus zu nutzen) und Schulungen beim Design der IT-Lösung berücksichtigt. Standardisierte Design-Aspekte für die grafische Benutzeroberfläche der IT-Lösungen werden angewandt (einheitliches Look-and-Feel) und moderne Technologien berücksichtigt (z. B. Touchscreen). Die Architekturrichtlinie ist nur für IT-Lösungen relevant, die direkt oder indirekt einen Einfluss auf die Interaktion durch den Nutzer hat. |
| Beispiele für die Anwendung| Der Projektplan umfasst Aktivitäten zur laufenden Einbindung der Nutzer|
| Bezug zu Zielen und Gesetzen| S3: Digitale Verwaltung (Grundprinzip: Nutzerfreundlichkeit)|

### SR7: Sicherstellung der Herstellerunabhängigkeit {#sr7}

| SR7: Sicherstellung der Herstellerunabhängigkeit | MUSS |
| -----	| -----	|
| Beschreibung| IT-Lösungen sollen derart gestaltet werden, dass eine realisierte IT-Unterstützung nicht nur durch einen einzigen Hersteller erbracht werden kann bzw. nur ein einziges IT-Produkt in Frage kommt (Verfolgung einer Dual- bzw. Multi-Vendor-Strategie). Um dies zu gewährleisten, sollen IT-Lösungen gem. Europäischem Interoperabilitätsrahmen ([COM/2017/0134](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX%3A52017DC0134)), soweit sinnvoll und wirtschaftlich, herstellerunabhängige/(quell-)offene Standards und Technologien nutzen. Dies betrifft sowohl die Beschaffung von Standardlösungen als auch die Entwicklung von Individuallösungen und die Implementierung von Schnittstellen zwischen den IT-Lösungen. Insbesondere für Austauschformate gelten folgende Mindestanforderungen an die Offenheit als Leitlinie:<br/> <br/>- Die Spezifikation wurde vollständig publiziert und die Publikation ist kostenfrei erhältlich.<br/><br/> - Die Verwendung der Spezifikation ist für Hersteller und Nutzerinnen und Nutzer der Software-Systeme uneingeschränkt und kostenfrei möglich.<br/><br/> - Zum Zeitpunkt der Bewertung ist nicht erkennbar, dass die Spezifikation in Zukunft die vorherstehenden Anforderungen nicht mehr erfüllen wird. |
| Begründung| Herstellerunabhängigkeit ist gerade für die öffentliche Verwaltung ein wesentliches IT-architektonisches Leitprinzip zur Sicherstellung architektonischer Flexibilität, Gestaltungs- und Handlungshoheit und Vermeidung von Abhängigkeitsverhältnissen zu einzelnen Herstellern („Herstellermonopole“). Solche Abhängigkeitsbeziehungen zu einzelnen Anbietern führen z. B. zu <br/><br/> - eingeschränkten Mitspracherechten seitens öffentlicher Verwaltung hinsichtlich geschäftlicher Konditionen und technischer Umsetzungen (z. B. der Zwang, Nutzungsdaten an den Hersteller zu übermitteln), <br/><br/> - potenziell höheren Kosten bei der Beschaffung von IT-Produkten und der Beeinträchtigung des Prinzips der Wirtschaftlichkeit (z. B. durch das Risiko ungünstiger Lizenzbedingungen für die öffentliche Verwaltung),<br/><br/> - Risiken für den IT-Betrieb (z. B. durch das Kündigen des Supports durch den Hersteller),<br/><br/> - einer Erhöhung der Systemkomplexität durch Vorhalten funktionsähnlicher IT-Produkte unterschiedlicher Hersteller zur Erbringung einer konkreten Leistung, <br/><br/> - einer schlechteren Verhandlungsbasis und Verringerung der Innovationsfähigkeit durch das Entstehen von „Lock-in-Effekten“. IT-Lösungen sollen daher nicht an bestimmte herstellerspezifische Funktionen, Austauschformate oder an spezielle Hardware-Technologien/-Plattformen individueller Hersteller gebunden sein, solange diese nicht explizit notwendig sind. Insbesondere für Fachanwendungen können definierte Ausnahmen hiervon begründet sein.Falls die öffentliche Verwaltung nicht in der Lage wäre, kurz- bzw. mittelfristig auf ein anderes IT-Produkt (ggf. auch das eines anderen Herstellers) auszuweichen, könnte dies in letzter Konsequenz die effiziente Erbringung hoheitlicher Aufgaben beeinträchtigen. Um auch in Zukunft selbstbestimmt und selbständig sowie flexibel auf Herausforderungen des digitalen Wandels reagieren zu können, ist es auch im Sinne der digitalen Souveränität der deutschen Verwaltung eine langfristige Abhängigkeit von einzelnen Herstellern zu vermeiden.|
| Abhängigkeiten| Diese Vorgabe steht in direktem Zusammenhang mit den Architekturrichtlinien [SR1](#sr9) (Standards), [SR8](#sr8) (Open Source) und [SR9](#sr9) (Interoperabilität). Die vorgenannten Vorgaben begünstigen die Anbindung und Interoperabilität verschiedener IT-Lösungen und fördern somit eine Herstellerunabhängigkeit i. S. der Vermeidung von Abhängigkeiten zu einzelnen Herstellern sowie Sicherstellung der Flexibilität i. d. S., dass Hersteller und deren IT-Produkte bedarfsbezogen getauscht werden können.|
| Implikationen| Die Sicherstellung der Herstellerunabhängigkeit erfordert eine Verankerung in strategische, prozessuale und technische Strukturen der öffentlichen Verwaltung. Auf strategischer Ebene sind klare Leitlinien zu entwickeln, was unter Herstellerunabhängigkeit zu verstehen ist. Hier ist insbesondere auch festzulegen, wie mit einzelnen Ausnahmen (i. S. einer „bewussten Herstellerabhängigkeit“) umzugehen ist. Auf technischer Ebene erfordert die Herstellerunabhängigkeit klare Leitlinien hinsichtlich zu nutzender Standards für die Entwicklung und den Einsatz von IT-Lösungen. Diese Standards sind übergreifend festzulegen und in den Produktkatalogen der IT-Dienstleister zu detaillieren. Die Gewährleistung der Herstellerunabhängigkeit kann einen hohen Implementierungsaufwand – und damit die Entstehung von Mehrkosten – durch den Verzicht von einzelnen herstellerabhängigen Funktionen nach sich ziehen. Darüber hinaus können weitere Risiken durch die Nutzung von Schnittstellen zwischen verschiedenen Komponenten entstehen. Zudem ist nicht auszuschließen, dass durch eine bewusste Fokussierung auf einzelne Hersteller, i. S. von spezifischen Anwendungsfällen (z. B. hinsichtlich der strategischen Mitgestaltungsmöglichkeit technischer Weiterentwicklungen von IT-Lösungen), eine positive Wirkung erzielt werden kann. |
| Beispiele für die Anwendung| Eine IT-Lösung eines Herstellers hat durch den Einsatz von offenen Standards und Open Source-Werkzeugen eine breite Entwicklerbasis die über den Hersteller hinausgehen. Die Lösung ist grundsätzlich auf alternative Produkte portierbar. <br/> <br/>Vermeidung von Hersteller-Lösungen mit monolithischen Software-Architekturen zur einfachen Austauschbarkeit einzelner Komponenten (Frontend, Persistenz, aber auch horizontal Module der Anwendung.|
| Bezug zu Zielen und Gesetzen| S1: Digitale Souveränität, S3: Digitale Verwaltung – Grundprinzip|

### SR8: Einsatz von Open Source {#sr8}

| SR8: Einsatz von Open Source | MUSS |
| -----	| -----	|
| Beschreibung| Der Quellcode aus der Realisierung digitaler Angebote der Verwaltung (Eigenentwicklung) ist als Open Source, d. h. in nachnutzbarer Form zur Verfügung zu stellen. Open Source Lösungen sind Nicht-Open Source Lösungen vorzuziehen, sofern geeignet und wirtschaftlich.|
| Begründung| Der Einsatz von Open Source unterstützt die Wiederverwendbarkeit und Herstellerunabhängigkeit. Etablierte Open Source Lösungen werden gemeinsam (durch eine breite Nutzerbasis) weiterentwickelt. Es ist möglich Einfluss auf Open Source Lösungen zu nehmen. Durch die Bereitstellung der eigenen Lösungen als Open Source wird die Wiederverwendung gefördert.|
| Abhängigkeiten| Der Einsatz von Open Source fördert eine Wiederverwendung ([SR2](#sr2)) und stärkt die Herstellerunabhängigkeit sowie die digitale Souveränität ([SR7](#sr7)), siehe auch oben.|
| Implikationen| Bei der Auswahl der Architektur neuer IT-Lösungen sind Open Source Produkte einzusetzen. Darüber hinaus muss die Eigenentwicklung als Open Source bereitgestellt werden.|
| Beispiele für die Anwendung| Eigenentwicklungen und deren Abhängige Komponenten verwenden Open Source-Infrastrukturen statt proprietären Infrastrukturen (z. B. Applikationsserver und Datenbanken) Bereitstellung des Quellcodes und der Betriebs- und Installationshandbüchern in öffentlichen Repositories|
| Bezug zu Zielen und Gesetzen| S1: Digitale Souveränität, S3: Digitale Verwaltung – Grundprinzip „Open Source“|

### SR9: Gewährleistung der Interoperabilität von IT-Lösungen {#sr9}

| SR9: Gewährleistung der Interoperabilität von IT-Lösungen | MUSS |
| -----	| -----	|
| Beschreibung| Bei der Neu- und Weiterentwicklung von IT-Lösungen sollen Interoperabilitätsstandards angewendet werden. Dies umfasst neben Anwendungen insbesondere auch Schnittstellen, Daten, Protokolle und Netze. Hierfür sollen u. a. geeignete Austauschformate (z. B. XML, XÖV-Standards) und relevante semantische Standards angewendet werden. Für IT-Lösungen, die europaweit eingesetzt werden können, muss weiterhin das European Interoperability Framework (EIF) berücksichtigt werden. |
| Begründung| Ein maßgeblicher Faktor bei der Neu- und Weiterentwicklung sowie der Beschaffung von IT-Lösungen ist die Interoperabilität. Interoperabilität erleichtert den Datenaustausch zwischen den IT-Lösungen, ermöglicht eine einfache Integration unterschiedlicher Anwendungen und Technologien über Daten und Schnittstellen. Interoperabilität hilft dabei, die grenz- und sektorübergreifende Interaktion zwischen europäischen Verwaltungen zu erleichtern und zu fördern. Ähnliches gilt für die Zusammenarbeit mit anderen Bundesländern. Neben den bekannten Vorteilen der Nutzung von Standards (u. a. Austauschbarkeit, Flexibilität, erhöhte Kompatibilität) ermöglicht die Nutzung von Interoperabilitätsstandards auch die Zusicherung mehrerer Hersteller hinsichtlich Produktunterstützung und fördert das Zusammenspiel und die Integration zu anderen Anwendungen und Technologien. Weitere Vorteile, die durch Einhaltung dieser Vorgabe entstehen, sind die Vermeidung von Medienbrüchen und die Ermöglichung von „Best-of-Breed“-Architekturen im Gegensatz zu den Lock-in-Effekt begünstigenden, monolithischen Architekturen.|
| Abhängigkeiten| Die Architekturvorgabe ergänzt und erweitert die Architekturrichtlinie [SR4](#sr4) (Standards). |
| Implikationen| Die Bereitstellung von Anwendungen durch unterschiedliche IT-Dienstleister und in unterschiedlichen Sicherheitsdomänen erfordert zur Sicherstellung der Interoperabilität erweiterte Regelungen zur Harmonisierung und Standardisierung der IT-Lösungsbereitstellung. Diese Regelungen sind auf rechtlicher, organisatorischer, semantischer und technischer Ebene zwischen den Ländern abzustimmen und umzusetzen, sodass ein einheitlicher und nahtloser Zugang zu den Diensten in unterschiedlichen Sicherheitsdomänen der Länder gewährleistet ist und Dienste in unterschiedlichen Sicherheitsdomänen interoperabel erbracht werden können. Die Regelungen sollen insbesondere auch einen Wechsel des Dienstleisters durch die Länder technisch ermöglichen.|
| Beispiele für die Anwendung| Erarbeitung einer föderalen Interoperabilitätsschicht für die Umsetzung des Onlinezugangsgesetzes, die einen Datenaustausch zwischen den Digitalisierungsplattformen der Länder ermöglicht. Die Interoperabilitätsplattform sorgt z. B. dafür, dass Nutzerkonten der Länder interoperabel gestaltet werden können und dass unterschiedliche Bezahldienste der Länder mit Hilfe von standardisierten Schnittstellen durch zentral betriebene Einer für Alle Online-Dienste standardisiert angesprochen werden können. Sie enthält weiterhin zentrale Verzeichnisse z. B. für die Verwaltung von Verwaltungsleistungen und Links zu Online-Dienste aller Länder unter Beibehaltung der regionalen Redaktionssysteme und Zuständigkeitsfinder (Online-Gateway)|
| Bezug zu Zielen und Gesetzen| S1: Digitale Souveränität, S3: Digitale Verwaltung – Grundprinzip „Einer für Alle/Wirtschaftlichkeit“, S4: Verwaltung als Plattform|

### SR10: Sicherstellung von loser Kopplung/Modularität {#sr10}

| SR10: Sicherstellung von loser Kopplung/Modularität | MUSS |
| -----	| -----	|
| Beschreibung| Die föderale IT-Architektur muss nach dem Baukastenprinzip modular aufgebaut werden. Jeder Baustein soll eigenständig nutzbar sein und entsprechend unabhängig weiterentwickelt, aktualisiert und betrieben werden können. Der funktionale Umfang eines Bausteins soll sich hierbei an etablierten Referenzmodellen und -architekturen sowie sinnvollen Kriterien für den jeweiligen Zuschnitt (z. B. der Abdeckung fachlich abgegrenzter Funktionen, Wirtschaftlichkeit, Wiederverwendbarkeit) orientieren.|
| Begründung| Das Ergebnis nicht-modular aufgebauter Anwendungen und Dienste sind schwer anpassbare und inflexible Lösungen, mit denen hohe Kosten und Aufwände einhergehen. Eine lose Kopplung ermöglicht es, Änderungen an einzelnen Komponenten eines Systems einfacher durchzuführen und vereinfacht auch die Wartbarkeit der Komponenten, da diese unabhängig von anderen Komponenten durchgeführt werden kann (ausgenommen Schnittstellen). Weiterhin ist die Modularisierung eine wesentliche Voraussetzung für die Wiederverwendbarkeit/Nachnutzung und ermöglicht ein strukturiertes Testvorgehen bei der Weiterentwicklung/Wartung. Außerdem sind modularisierte Anwendungen und Dienste aufgrund der klaren Abgrenzungen der Teilkomponenten einfacher zu verwalten. Funktional sinnvoll und schlank geschnittene Module, die eine eindeutige Verantwortlichkeit wahrnehmen, beugen unnötiger Komplexität und mangelnder Flexibilität vor. <br/><br/>Die lose Kopplung kann des Weiteren auch die Stabilität des Gesamtsystems begünstigen, da der Ausfall von losen gekoppelten Modulen ggf. nur einzelne Funktionalitäten der IT-Lösung beeinflussen. <br/><br/> Auch die Herstellerunabhängigkeit ([SR7](#sr7)) wird durch die Anwendung begünstigt.|
| Abhängigkeiten| Diese Vorgabe steht in engem Zusammenhang mit den Architekturrichtlinien Wiederverwendung ([SR2](#sr2)) und Reduzierung der Komplexität auf ein notwendiges Maß ([SR12](#sr12)). Des Weiteren bildet sie eine Grundlage für Interoperabilität ([SR9](#sr9)), da sie aktiv die Wiederverwendung von IT-Lösungen bzw. ausgewählten Teilbereichen adressiert.|
| Implikationen| Die Vorgabe impliziert eine Abkehr von rein monolithischen IT-Lösungen zugunsten flexibler, modularer IT-Lösungen. Die Richtlinie ist für den Funktionsumfang der föderalen IT-Lösungen relevant (unabhängig von der internen IT-Architektur). Dabei geht es darum IT-Lösungen mit einem zu umfassenden Funktionsumfang zu vermeiden. Sie ist auch für die Gestaltung der internen IT-Architektur einzelner IT-Lösungen relevant. Dieses Kriterium verfolgt den Ansatz, Anforderungen an die Architektur in Form von Architekturbausteinen zu beschreiben und in Form von Lösungsbausteinen umzusetzen. Auf Basis dieser Bausteineund ihrer Beziehungen untereinander lassen sich Referenzmodelle und –architekturen entwickeln. Die Bausteine sind geeignet, in ein Architektur-Repository aufgenommen zu werden und können hieraus zur Darstellung von IST-Architekturen und SOLL-Architekturen herangezogen werden. Aufgrund der Bedeutung für die Wartbarkeit, Flexibilität und Wirtschaftlichkeit der IT-Landschaft wird eine Prüfung von loser Kopplung/Modularität bei Neu- und Weiterentwicklungen von föderalen IT-Lösungen durch das Architekturboard vorgenommen.|
| Beispiele für die Anwendung| Erarbeitung einer föderalen IT-Landkarte als Orientierung für den Schnitt von föderalen IT-Lösungen. <br/><br/>Auch die interne IT-Architektur einer IT-Lösung ist modular aufzubauen.|
| Bezug zu Zielen und Gesetzen| S3: Digitale Verwaltung – Grundprinzipen „Wirtschaftlichkeit“ und „nachhaltige technische Qualität“|

### SR11: Gewährleistung einer umweltfreundlichen und nachhaltigen Nutzung von Informationstechnik {#sr11}

| SR11: Gewährleistung einer umweltfreundlichen und nachhaltigen Nutzung von Informationstechnik | MUSS |
| -----	| -----	|
| Beschreibung| Es muss über den gesamten Lebenszyklus der Informationstechnik auf einen umweltfreundlichen und nachhaltigen Einsatz geachtet werden. Rechenzentren und deren Infrastruktur sollen so ausgewählt und gestaltet werden, dass eine hohe Energieeffizienz und hohe Auslastung der Hardware-Ressourcen (z. B. Serverkapazitäten) erreicht wird. Darüber hinaus sind die Anforderungen des “Blauen Engels” für einen energieeffizienten Rechenzentrumsbetrieb in den Rechenzentren der öffentlichen Verwaltung einzuhalten. Ein flexibles Up-/Downsizing an kapazitative Anforderungen soll dabei Berücksichtigung finden. Um natürliche Ressourcen zu schonen, soll funktionstüchtige IKT nach dem Nutzungsende in den Behörden für eine Wiederverwendung Verfügung gestellt oder fachgerecht verwertet werden. Zusätzlich sollen jegliche IT-Lösungen so konzipiert werden, dass der Ressourcenverbrauch über den gesamten Lebenszyklus minimiert wird und gleichzeitig möglichst optimale Auslastungszeiten realisiert werden.|
| Begründung| Umsetzung der Green-IT-Strategie des IT-Planungsrates|
| Abhängigkeiten| Keine Abhängigkeiten vorhanden|
| Implikationen| Die Anschaffungskosten für Hardware können im Zusammenhang mit dieser Richtlinie steigen, da umweltfreundliche Geräte häufig preisintensiver sind.|
| Beispiele für die Anwendung|  Bewertung von Architekturalternativen auch auf der Grundlage vom geschätzten Energieverbrauch.|
| Bezug zu Zielen und Gesetzen| S2: Green-IT|

### SR12: Umsetzung des „Once Only“ Prinzips {#sr12}

| SR12: Umsetzung des „Once Only“ Prinzips | SOLL |
| -----	| -----	|
| Beschreibung| Bürgerinnen und Bürger sollten ihre Daten und Dokumente nur einmal mitteilen müssen. Nachweisdokumente werden schrittweise durch Registerabfragen und zwischenbehördliche Datenaustausche ersetzt.|
| Begründung| Das Once-Only-Prinzip ist nicht nur eine Entlastung der Bürgerinnen und Bürger, sondern auch der Verwaltung selbst. Durch den Austausch, das Abrufen und gemeinsame Speichern bereits gesammelter Informationen verringern sich der Aufwand und die Kosten für die Verwaltung.|
| Abhängigkeiten| Keine Abhängigkeiten vorhanden|
| Implikationen| Das Once-Only-Prinzip kann direkt über Registerabfragen und Once-Only-Projekte umgesetzt werden, wenn diese Ressourcen bestehen und keine rechtlichen Hürden im Weg stehen. Sollte es Hürden technischer oder rechtlicher Art geben, ist es wichtig, den Onlinedienst standardisiert so umzusetzen, dass dieser in Zukunft in neue Systeme integriert werden kann.| 
Beispiele für die Anwendung|  Online-Dienstanträge werden mit Informationen aus Registern vorbefüllt. Der Nutzer muss nur die Korrektheit der Informationen bestätigen. |
| Bezug zu Zielen und Gesetzen| R2: Single Digital Gateway Verordnung, R3:Registermodernisierungsgesetz|

### SR13: Open Data by Design {#sr13}

| SR13: Open Data by Design | MUSS |
| -----	| -----	|
| Beschreibung| IT-Lösungen sind bereits während des Designs so zu gestalten, damit Daten über öffentliche Netze verfügbar gemacht werden können. Dies gilt für alle Daten, die nicht geschützt werden müssen.|
| Begründung| Daten der öffentlichen Verwaltung öffnen Möglichkeiten für externe Akteure (Wirtschaft und Privatpersonen) Mehrwert-Dienstleistungen zu schaffen.|
| Abhängigkeiten| Keine Abhängigkeiten vorhanden|
| Implikationen| Föderale Infrastrukturprojekte müssen unentgeltliche Bereitstellungen von Daten ermöglichen. Die föderale Infrastruktur ist auf Open Data Fähigkeit zu prüfen, sofern relevant. Das Architekturboard wird bei der Prüfung von Infrastrukturprojekten die geplante IT-Architektur und deren Umsetzung prüfen. Die Daten müssen mit Metadaten versehen werden und auf dem nationalen Portal GovData eingestellt werden.| 
Beispiele für die Anwendung| Statistische Erhebungen z. B. Anzahl der Zugriffe auf Verwaltungsleistungen und Online-Dienste <br/><br/>Recyclingkarte mit Altkleider-, Altpapier- und Altglas-Container. |
| Bezug zu Zielen und Gesetzen| S5: Open Data fördern, S4: Verwaltung als Plattform|

## 6 Anwendung der Architekturrichtlinien
Die Anwendung der Architekturrichtlinien richten sich nach der Verbindlichkeit der Architekturrichtlinien (MUSS, KANN, DARF NICHT). 
Abweichungen von Architekturrichtlinien der Kategorie MUSS und DARF NICHT müssen dem Architekturboard dargelegt und begründet werden. 
Für Anträge auf diesbezügliche Ausnahmegenehmigungen wird das föderale Architekturboard entsprechende Vorlagen bereitstellen. 
Neue Projekte müssen begründen, wie sie beabsichtigen, MUSS-Architekturrichtlinien umzusetzen, wenn diese nicht unmittelbar eindeutig messbar sind. 
Dies ist insbesondere für strategische Architekturrichtlinien der Fall.

Der Ablauf zur Nutzung der Architekturrichtlinien ist Teil des Architekturmanagementprozesses und wird separat dokumentiert. 
Grundsätzlich gilt, dass entlang des Lebenszyklus von föderalen IT-Lösungen Quality-Gates vorgesehen sind. 
Zu ausgewählten Zeitpunkten, z. B. vor Projektinitialisierung oder vor einer Ausschreibung muss der Projektleiter bzw. verantwortliche IT-Architekt die Konformität zu den Architekturrichtlinien begründen. 
Ergänzend dazu dienen die Architekturrichtlinien als Grundlage für Architekturreviews, die durch das Architekturboard initiiert werden können. 

## 7 Anpassung und Weiterentwicklung der Architekturrichtlinien
Die vorliegende Version der Architekturrichtlinie soll für ausgewählte Projekte/Infrastrukturanträge erprobt werden. 
Basierend auf den Erfahrungen sollen sie bedarfsorientiert angepasst werden. 
Im weiteren Verlauf sollen die Architekturrichtlinien ergänzt werden, so dass zusätzlich zu den übergreifenden strategischen Architekturrichtlinien domänenspezifische Architekturrichtlinien hinzukommen. 
Darüber hinaus werden die Architekturrichtlinien wie folgt angepasst:

- Turnusmäßige (z. B. jährliche) Überprüfung der Architekturrichtlinien. 
Dabei erfolgt eine Überprüfung der neuen Versionen vorhandener aktualisierter Architekturrichtlinien Bund und Länder. 
Sofern für föderale Belange relevant und sinnvoll, werden angepasste oder neue Architekturrichtlinien übernommen.
- Anpassung auf Antrag: Beteiligte Bund und Länder können Vorschläge zur Ergänzung der Architekturrichtlinien unterbreiten und dem Architekturboard senden. 
Das Architekturboard wird für die Anträge entsprechende Vorlagen erarbeiten und veröffentlichen. 
Die Entscheidung, inwieweit die Vorschläge unverändert oder mit Anpassung übernommen werden, wird im Architekturboard getroffen. 
- Anpassungen können sich auch aufgrund geänderter strategischer Vorgaben des IT-Planungsrates ergeben. 
Die Richtlinien sind in diesen Fällen zeitnah den geänderten Rahmenbedingungen anzupassen.
- Die Architekturrichtlinien werden nach Vorlage und Vorstellung durch das Architekturboard von der AL-Runde verabschiedet.

Die Architekturrichtlinien in der vorliegenden Fassung sollen als Grundlagendokument initial 
seitens des IT-Planungsrates beschlossen werden. Entsprechend dem Dokument 
„Zusammenwirken im föderalen Architekturboard (Organisation)“[^11] werden alle weiteren 
Anpassungen der strategischen Architekturrichtlinien nach Vorlage und Vorstellung durch das 
Architekturboard von der AL-Runde verabschiedet.

[^11]:  https://www.fitko.de/mm/Organisation_Architekturboard_1.0.pdf

-----

## Primärquellen
- [Version 1 der Föderalen Architekturrichtlinien (pdf)](https://www.fitko.de/fileadmin/fitko/foederale-koordination/gremienarbeit/Foederales_IT-Architekturboard/Foederale_IT-Architekturrichtlinien_V1.0.pdf) findet sich auf der Webseite des [Föderalen Architekturboards](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard).

## Fußnoten
