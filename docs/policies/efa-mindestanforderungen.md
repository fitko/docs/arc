---
title: EfA-Mindestanforderungen
---

:::info Zusammenfassung
Die EfA-Mindestanforderungen definieren Vorgaben zur Umsetzung von Online-Services nach dem sog. „Einer für Alle“-Prinzip.
Die EfA-Mindestanforderungen sind ein Nachnutzungsstandard im Sinne des [§ 6 Nr. 3d des Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes](./verwaltungsabkommen-ozg.md#p6).

Gemäß Beschluss der [Abteilungsleiterrunde](https://www.it-planungsrat.de/foederale-zusammenarbeit/gremien) vom 8.12.2020 werden die EfA-Mindestanforderungen durch das [Architekturboard des IT-Planungsrates](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard) fortgeschrieben. Die Beschlussfassung über Fortschreibungen erfolgt durch die Abteilungsleiterrunde.

Feedback und Anregungen nehmen wir gerne auch über den [Issue-Tracker zur Föderalen IT-Infrastruktur](https://gitlab.opencode.de/fitko/feedback/-/issues/?label_name%5B%5D=EfA-Mindestanforderungen) entgegen.
:::

-----

# Mindestanforderungen an „Einer für Alle“-Services
<p><center>Verpflichtungserklärung der Länder zur Umsetzung von Leistungen  mit Mitteln des Konjunkturpakets Version 2.0 beschlossen in der OZG-AL-Runde am 9. November 2022</center></p>

<p><center>Version 2.0 beschlossen in der OZG-AL-Runde am 9. November 2022</center></p>

<p><center>Nachnutzungsstandard im Sinne von § 6 Nr. 3d  des  Kooperationsvertrags zur Umsetzung des Onlinezugangsgesetzes</center></p>

## Hintergrund
Das Konjunkturpaket des Bundes bietet eine einmalige Chance, die Digitalisierung von Verwaltungsleistungen entscheidend voran zu bringen. 
Dabei ist unbestritten, dass dies nur arbeitsteilig gelingen kann. 
Als umfassende Form der Arbeitsteilung und Zusammenarbeit wurde hierfür das „Einer für Alle“-Prinzip etabliert.
„Einer für Alle“-Services sind flächendeckend einsetzbare Lösungen, die einmal nutzerzentriert konzipiert und entwickelt, fachlich betreut und technisch betrieben werden. 

Sowohl im Rahmen des Digitalisierungsprogramms zur Umsetzung des OZG als auch darüber hinaus wurden bereits mehrere Leistungen auf Basis bestehender Technologien nach dem EfA-Prinzip umgesetzt. 
Dennoch gibt es kein einheitliches Verständnis darüber, welche Anforderungen ein EfA-Service erfüllen muss. 

## Zweck des Dokuments
Dieses Dokument beschreibt, welche Mindestanforderungen ein Online-Dienst zur elektronischen Abwicklung von Verwaltungsleistungen im Rahmen des Konjunkturpaketes erfüllen muss, damit er als „EfA-konform“ gelten kann.
Vor dem Hintergrund der geringen verbleibenden OZG-Umsetzungsfrist wird hier ausschließlich auf bereits heute bestehende Infrastrukturen und etablierte Technologien abgestellt. 
Sofern sich im Rahmen der OZG-Umsetzung weitere Technologien durchsetzen sollten und die entsprechenden Voraussetzungen geschaffen sind, werden diese hier aufgenommen.

Die Länder verpflichten sich untereinander und gegenüber dem Bund, EfA-Services nach den hier vereinbarten Mindestanforderungen umzusetzen. 
Damit soll eine Verlässlichkeit unter den Ländern sichergestellt werden, dass die von den jeweils anderen Ländern umgesetzten EfA-Services im eigenen Land genutzt werden können. 

## "Einer für Alle"-Anforderungen

### Oberflächengestaltung & Design
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| OD1 | Der Online-Dienst MUSS über ein neutrales (keine landes-, kommunal- oder behördenspezifischen Styleguides oder die vollständige Anmutung der Oberfläche der jeweiligen Verwaltungsportale der beteiligten Länder, Kommunen oder Behörden) Design verfügen.|
| OD2 | Der Online-Dienst SOLL über ein mit Nutzer:innen getestetes Design verfügen und die Leitlinien zum Nutzererlebnis Portalverbund berücksichtigen.|
| OD3 | Der Online-Dienst MUSS, nachdem das leistungsspezifische Zuständigkeitsmerkmal (z.B. Postleitzahl. Ortsangaben oder georeferenzierter Daten oder Parameterübergabe bei Online-Dienst-Aufruf) ermittelt wurde, die individuell zuständige Behörde mit den Kontaktdaten anzeigen und SOLL das jeweilige Wappen der zuständigen Gebietskörperschaft, sofern es durch diese hinterlegt wurde, anzeigen.|
| OD4 | Der Online-Dienst MUSS die für den Empfang des Antrags zuständige(n) Behörde(n) mittels Leistungsschlüssel gemäß FIM und amtlichen Regionalschlüssel aus dem aktuellen Datenbestand des Portalverbundes ermitteln können.|

### Fachlogik
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| F1 | Der Online-Dienst MUSS die fachrechtlichen Anforderungen der Bundesgesetze erfüllen. |
| F2 | Der Online-Dienst MUSS landesrechtliche Zusatzanforderungen aller nachnutzenden Länder berücksichtigen. |
| F3 | Der Online-Dienst SOLL bei Bedarf landes- oder satzungsrechtliche Ausführungsvorschriften zu bundesrechtlich geregelten Leistungen geeignet berücksichtigen können (z.B. durch Mandantenfähigkeit, Parametrisierung). |

### Nutzerkonto
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| NK1 | An den Online-Dienst für Bürgerinnen und Bürger MUSS mindestens ein interoperables Nutzerkonto (Authentifizierung und Postfach) angebunden sein. Wenn ein Online-Dienst kein interoperables Nutzerkonto anbinden kann, dann muss die BundID angebunden werden. |
| NK2 | An den Online-Dienst für Unternehmen und andere Organisationen MUSS das einheitliche Organisationskonto angebunden werden.  |
|  | *Übergangsregelung zu NK2: Andere Organisationskontos, die dem Funktionsumfang der Bausteine 1-6 entsprechen, können bis zur vollständigen Verfügbarkeit des einheitlichen Organisationskontos Bausteine 1-6 weiter eingesetzt werden.* |

### Payment 
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| P1 | Wenn die fachliche Anforderung einer Vorabzahlung gegeben ist und die Berechnung einer Gebühr im Online-Dienst möglich ist, MUSS der Online-Dienst für die Bezahlung eine von den empfangenden Behörden bereitzustellende Bezahlkomponente parametrisiert aufrufen können, sofern diese Komponente und deren Parameter (im PVOG und DVDV) von der empfangenden Behörde bereitgestellt werden. Dabei MUSS die standardisierte Bezahldienstschnittstelle (Payment-API) für die Anbindung verwendet werden, sobald die Payment-API mindestens in der Version 1 vorliegt.|
| P2 | Derr Online-Dienst KANN zusätzlich eine eigene Bezahlkomponente anbieten, die Behörden konfigurieren können, die über keine eigene Bezahlkomponente verfügen. |

### Datenaustauschstandard 
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| DS1 | Der Online-Dienst MUSS über eine automatisierte Schnittstelle die Antragsdaten in einem standardisierten XML-Format (z.B. als Modul innerhalb eines XÖV-Standards oder die XDatenfelder in einem XFall-Container) ausgeben, das von Fachverfahren wiederum (halb-) automatisch eingelesen werden kann. Sofern es keine Fachverfahren gibt, SOLL der Online-Dienst (zusätzlich) eine lesbare PDF-Datei erzeugen. |
| DS2 | Sofern kein Fachstandard existiert, MUSS ein Standardisierungsprozess aufgesetzt werden, der folgende Aspekte sicherstellen soll: Planbarkeit, Verlässlichkeit, Verbindlichkeit, Finanzierung; Steuerung durch die öffentliche Verwaltung; Beteiligung aller relevanten Stakeholder; Offenheit der Standards [im Sinne der Free Software Foundation Europe](https://fsfe.org/freesoftware/standards/index.de.html); Praxisorientierung; regelmäßige Weiterentwicklung (Änderungsmanagement – nicht nur bei Änderungen der Rechtsgrundlagen, sondern auch aufgrund von Feedback aus der Praxis); hoher Detaillierungsgrad, hohe Qualität, technisch robust; angemessener und realistischer Standardisierungsgegenstand; nachgewiesener Reifegrad der Methodik / des Rahmenwerks; angemessene Berücksichtigung der Vorgaben und Angebote der EU. |
| DS3 | Der Online-Dienst MUSS eine strukturierte Ausgabe des Antrags im XFall-Format basierend auf den zugehörigen FIM-Stammdatenschemata erzeugen, sofern in der Verwaltung KEIN Fachstandard existiert oder geschaffen wird (z.B. XÖV).|
| DS4 | Der Online-Dienst SOLL an die meist genutzten Fachverfahren unterschiedlicher Hersteller (soweit existent) in den nach dem EfA-Prinzip anzuschließenden Ländern anschlussfähig sein. |

### Routing & Transport
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| RT1 | Die technischen Verbindungsdaten der zuständigen Behörden KÖNNEN bei einer geringen Anzahl bundesweit empfangender Stellen (kleiner gleich 16) direkt im Online-Dienst hinterlegt und gepflegt werden.  |
| RT2 | Der Online-Dienst MUSS bei einer größeren Zahl bundesweit empfangender Stellen (>16) deren technische Adressierung mittels des Zugriffs auf das DVDV ermitteln.|
| RT3 | Bei einem Routing mithilfe des DVDV MUSS für den Online-Dienst ein DVDV-Eintragungskonzept erstellt werden. |
| RT4 | Der Online-Dienst MUSS mindestens eine der folgenden Möglichkeiten zur verschlüsselten Übermittlung von Antragsdaten nutzen: <br/><br/>a) Die zu transportierenden Daten werden über einen OSCI-Sender (ggf. über eine XTA-Schnittstelle zum Sender) an die von den antragsbearbeitenden Behörden definierten OSCI-Empfänger übermittelt. <br/>b) Die zu transportierenden Daten werden vom Online-Dienst über die FIT-Connect–Schnittstelle an die FIT-Connect Infrastruktur übertragen. Der Transport von dort zur antragsbearbeitenden Stelle findet in der durch diese Stelle gewählte Art (FIT-Connect, XTA oder OSCI) statt. <br/><br/>Sofern es in einzelnen Fachdomänen bereits bundesweit etablierte Übertragungsstandards (z.B. ELSTER) gibt, KÖNNEN diese genutzt werden, sofern die Schutzziele Vertraulichkeit, Integrität (inkl. Authentizität) und Verfügbarkeit sichergestellt sind. |
| RT5 | Der Online-Dienst MUSS eine zertifikatsbasierte Übermittlung der Daten mit Ende-zu-Ende Verschlüsselung zwischen dem Endgerät der antragstellenden Person bzw. dem Online-Dienst bzw. dem Portal auf dem der Dienst betrieben wird und der zuständigen Behörde ermöglichen. Die Verschlüsselung MUSS mindestens bis zu einem von der nachnutzenden Behörde zu definierenden Endpunkt reichen. Die verwendeten Zertifikate müssen der Verwaltungs-PKI entstammen.  |

### Rechtliche Nachnutzungsmöglichkeit 
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| R1 | Das verantwortliche Land MUSS eine geeignete rechtliche Mitnutzungsmöglichkeit für Leistungen im Landesvollzug und übertragenen Wirkungskreis anbieten (z.B. Verwaltungsvereinbarung, FIT-Store, govdigital Marktplatz).  |
| R2 | Das verantwortliche Land MUSS für den Online-Dienst über ausreichende Lizenzrechte für die Nutzung durch andere Länder und Kommunen verfügen. |

Auf Ebene der AL-Runde unter Beratung der kommunalen Spitzenverbände soll eine rechtliche Mitnutzung von Online Services der Kommunen geklärt werden, damit auch die Entlastung der Kommunen bei der OZG Umsetzung sichergestellt werden kann.  

### Organisation
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| O1 | Für den Online-Dienst MUSS eine organisatorische Zusammenarbeitsstruktur geschaffen (oder eine bestehende genutzt) werden, in der die beteiligten Länder die fachlichen, rechtlichen, technischen etc. Anforderungen fortwährend pflegen.  |

### IT-Sicherheit
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| S1 | Der Online-Dienst MUSS über eine security.txt gemäß RFC 9116 verfügen. Ein interner Prozess zum Umgang mit Responsible-Disclosure-Meldungen muss etabliert sein. |

### (Ausgewählte) Anforderungen an EfA-mitnutzende Länder 
| Nr. | Anforderung                               |
|-----|-------------------------------------------|
| NL1 | Die antragsbearbeitende Behörde MUSS ihre Zuständigkeitsinformationen (Behördenbezeichnung, Ortsangaben etc.) mittels der im Land etablierten Redaktionssysteme pflegen und eine Übertragung dieser Informationen an den Portalverbund (Sammlerdienst) sicherstellen, damit der Online-Dienst über den Portalverbund auffindbar (Online-Gateway) ist.  |
| NL2 | Bei einem Transport via XTA-OSCI MUSS die antragsbearbeitende Behörde einen OSCI-Empfänger zum Empfang des Transportcontainers bereitstellen.   Dieser Empfänger muss nicht zwingend je Behörde bereitgestellt werden. Hier sind auch im Land vorhandene gemeinsame Empfangsstrukturen nutzbar. |
| NL3 | Bei einem Routing mithilfe des DVDV muss die Pflegende Stelle zur Registrierung der Behörden und technischen Adressen im DVDV beauftragt werden und Fachverfahren müssen an den jeweiligen DVDV-Server des Landes angebunden werden.|
| NL4 | ie antragsbearbeitende Behörde MUSS die Übermittlung von Statusnachrichten und Bescheiden rechtssicher gemäß § 41 Abs. 2a VwVfG oder § 9 OZG sicherstellen.  |
| NL5 | Die nachnutzende Behörde MUSS, sofern eine Bezahlung erforderlich ist, eine Bezahlkomponente sowie die Parameter für deren Aufruf bereitstellen oder die Übermittlung der Zahlungsinformationen an Nutzer eigenständig sicherstellen.|

-----

## Primärquellen
Die EfA-Mindestanforderungen wurden ursprünglich in folgenden Fassungen veröffentlicht:
- [EfA-Mindestanforderungen, Version 1.0](https://www.onlinezugangsgesetz.de/SharedDocs/downloads/Webs/OZG/DE/EfA/efa-mindestanforderungen.pdf?__blob=publicationFile&v=3) auf der Webseite [onlinezugangsgesetz.de](https://www.onlinezugangsgesetz.de)
- [EfA-Mindestanforderungen, Version 2.0](https://leitfaden.ozg-umsetzung.de/display/OZG/Arbeitshilfen?preview=/12583387/49316400/EfA_Mindestanforderungen.pdf) im [OZG-Leitfaden](https://leitfaden.ozg-umsetzung.de/) ([Ankündigung zur Veröffentichung](https://www.onlinezugangsgesetz.de/SharedDocs/kurzmeldungen/Webs/OZG/DE/2022/11_efa_mindestanforderungen.html))
