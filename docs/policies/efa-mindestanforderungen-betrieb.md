---
title: EfA-Mindestanforderungen Betrieb
---


:::info Zusammenfassung
Die EfA-Mindestanforderungen Betrieb definieren Vorgaben zu Rollen und Verantwortlichkeiten für Betrieb und Support von EfA-Online-Services nach dem sog. „Einer für Alle“-Prinzip.
Die EfA-Mindestanforderungen Betrieb sind ein Nachnutzungsstandard im Sinne des [§ 6 Nr. 3d des Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes](./verwaltungsabkommen-ozg.md#p6).

Gemäß [Beschluss 2023/07](https://www.it-planungsrat.de/beschluss/beschluss-2023-07) des IT-Planungsrates vom 29.03.2023 werden die EfA-Mindestanforderungen durch die Arbeitsgruppe „Rahmenbedingungen für den Betrieb von EfA-Services“ (AG RaBe) evaluiert und Best Practices für die Umsetzung entwickelt.
Die Beschlussfassung über Fortschreibungen erfolgt durch die [Abteilungsleiterrunde](https://www.it-planungsrat.de/foederale-zusammenarbeit/gremien).

Feedback und Anregungen nimmt die AG RaBe gerne unter unter der Mail-Adresse `ag-rabe@sk.hamburg.de` entgegen.
:::

<!-- Feedback und Anregungen nehmen wir gerne auch über den [Issue-Tracker zur Föderalen IT-Infrastruktur](https://gitlab.opencode.de/fitko/feedback/-/issues?label_name%5B%5D=EfA-Mindestanforderungen-Betrieb) entgegen. -->

-----

# Mindestanforderungen an den Betrieb von „Einer für Alle“-Services

Verpflichtungserklärung der Länder bezüglich des Betriebs von EfA-Services

**Hintergrund**

Das Konjunkturpaket des Bundes bot eine einmalige Chance, die Digitalisierung von Verwaltungsleistungen entscheidend voran zu bringen.
Dabei war unbestritten, dass dies nur arbeitsteilig gelingen kann.
Als umfassende Form der Arbeitsteilung und Zusammenarbeit wurde hierfür das „Einer für Alle“-Prinzip etabliert.
„Einer für Alle“-Services [^1] sind flächendeckend einsetzbare Lösungen, die einmal nutzerzentriert konzipiert und entwickelt, fachlich betreut und technisch betrieben werden.
Sowohl im Rahmen des Digitalisierungsprogramms zur Umsetzung des OZG als auch darüber hinaus wurden bereits mehrere Leistungen auf Basis bestehender Technologien nach dem EfA-Prinzip umgesetzt.
Um ein einheitliches Verständnis darüber zu schaffen, welche Anforderungen ein EfA-Onlinedienst erfüllen muss, wurden die Mindestanforderungen an „Einer für Alle“-Services entwickelt.
Diese konzentrieren sich bisher darauf, wie die EfA-Onlinedienste umgesetzt werden, geben allerdings keine Auskunft darüber, wie ein weiterer Betrieb der Dienste erfolgen kann.

[^1]: [EfA-Service und EfA-Onlinedienst werden hier synonym verwendet.]

**Zweck des Dokuments**

In diesem Dokument sind die Mindestanforderungen beschrieben, die jeder Betrieb von EfA-Onlinediensten erfüllen muss, damit er als „EfA-konform“ gelten kann.
Des Weiteren soll dieses Dokument als Startpunkt dienen, um die Regelungen zum Betrieb von EfA-Services und anderen Onlinediensten festzulegen, mit dem Anspruch kontinuierlich fortgeschrieben und weiterentwickelt zu werden.
Die Länder verpflichten sich untereinander den Betrieb von EfA-Services nach den hier vereinbarten Mindestanforderungen umzusetzen.
Damit soll eine Verlässlichkeit für einen nutzerfreundlichen und wirtschaftlichen Betrieb sowie eine effiziente Mitnutzung von EfA-Services unter den Ländern sichergestellt werden.

**Hinweise zu den Mindestanforderungen**

Für den Betrieb von EfA-Services ist es wichtig, ein einheitliches Grundverständnis der Mindestanforderungen zu schaffen: Jedes umsetzende Land kann in der Betriebsphase gleichzeitig betreibendes sowie mitnutzendes Land für die im eigenen Land entwickelten EfA-Services sein.
Um die Aufgaben und Verantwortlichkeiten für betreibende und mitnutzende Länder besser voneinander abzugrenzen, wurden diese in den nachfolgenden Mindestanforderungen an den Betrieb von EfA-Services, getrennt betrachtet.

Außerdem ist es das Ziel der Mindestanforderungen an den Betrieb von EfA-Services die organisatorischen Schnittstellen zwischen den Ländern festzulegen, sich dabei aber von der landesinternen Organisation klar abzugrenzen.
Es ist beispielsweise festgelegt, dass es für jeden EfA-Service einen Steuerungskreis und eine koordinierende Stelle im betreibenden Land (Betriebsverantwortlicher) geben muss.
Dies schließt jedoch nicht aus, dass die Steuerungskreise von EfA-Services mit gleichen fachlichen Hintergründen terminlich zusammengelegt werden.
Genauso können die Aufgaben der Rolle Betriebsverantwortlicher in Personalunion ausgeführt oder auf verschiedene Personen aufgeteilt werden.
Wichtig für die Zusammenarbeit ist, dass die Verantwortlichkeiten übernommen und die Kontakte klar kommuniziert werden.

Um auch sprachlich ein einheitliches Verständnis für den Betrieb von EfA-Services sicherzustellen, ist den Mindestanforderungen ein Glossar beigefügt. In diesem finden sich alle Fachbegriffe, Fremdwörter und Abkürzungen mit den entsprechenden Erklärungen.

## Mindestanforderungen an Rollen und Verantwortlichkeiten

![Schaubild zu Rollen und organisatorischen Schnittstellen in der Betriebsphase je EfA-Onlinedienst](/images/efa-mindestanforderungen-betrieb/rollen.png)

| Nr. | Name |Anforderung |
|--|--|--|
| R1 |Steuerungskreis  |Das BeLa muss Gremien (Steuerungskreise) sicherstellen, die alle betriebenen EfA-Onlinedienste sachgerecht und ressourcen-schonend bündeln und in denen alle MiLa stimmberechtigt und das BeLa moderierend vertreten sind. <br/>Der IT-PLR wird alle Fachministerkonferenzen auffordern, ab 2024 bzw. bei der Überleitung in den Routinebetrieb für ihren Zuständigkeitsbereich ein Steuerungsgremium festzulegen. Das BeLa wird jeden EfA-Onlinedienst dem fachlich zuständigen Steuerungsgremien zuweisen. Bei einer fachministerkonferenz-übergreifenden Zuständigkeit entscheidet das BeLa nach fachlichen Gesichtspunkten und stellt eine Beteiligung der ebenfalls betroffenen Fachministerkonferenzen sicher. Sofern möglich, sollten existierende Gremien der jeweiligen Fachministerkonferenz genutzt werden. Die Fachministerkonferenz beschließt im Rahmen der bestehenden Mustergeschäftsordnung über Zusammensetzung, Stimmrecht und Tagungsweise des Gremiums, wobei die Teilnahme des BeLa sichergestellt werden muss. Das Gremium ist für alle grundlegenden und strategischen Entscheidungen zuständig, sofern diese Aufgaben nicht dem Betriebsverantwortlichen übertragen wurden. Bis das Steuerungsgremium der zuständigen FMK etabliert ist, ist das BeLa für die Durchführung des Steuerungskreises zuständig. Dabei sollen bereits etablierte Strukturen aus der Umsetzungsphase genutzt werden.<br/><br/>Das Steuerungsgremium ist insbesondere zuständig für:<br/>&nbsp;&nbsp;&nbsp;&nbsp;a. die Freigabe der strategischen Gesamtplanung des jeweiligen EfA-Onlinedienstes<br/>&nbsp;&nbsp;&nbsp;&nbsp;b. die Entscheidungen über strategische Fragen zum Betrieb und Weiterentwicklung oder Abänderung des jeweiligen Onlinedienstes, die Auswirkungen auf den vereinbarten Kostenrahmen oder erhebliche Auswirkungen auf die Fachverfahren oder die Interessen der MiLa sowie des BeLa haben <br/>&nbsp;&nbsp;&nbsp;&nbsp;c. die Entscheidung über die Erweiterung oder Änderungen in der Kostenplanung in Bezug auf die Finanzierung, das Budget, die Kostenverteilung und Mehrbedarfe sowie die Annahme des, vom Betriebsverantwortlichen erstellten, Budgets <br/>&nbsp;&nbsp;&nbsp;&nbsp;d. unter Berücksichtigung etwaiger vertraglicher Beziehungen entscheidet der Steuerungskreis über die Festlegung der Einzelheiten bei dem Beitritt sowie Austritt der Länder z.B. der Zahlung bei Eintritt
R2|Expertengruppen|Es kann Expertengruppen geben, die die Inhalte des Steuerungskreises fachlich, rechtlich und technisch aufbereiten und bewerten. Eine Expertengruppe kann aus Fachleuten aus Bund, Ländern, Kommunen (MiLa und BeLa) oder anderen Experten bestehen und wird bei Bedarf vom Betriebsverantwortlichen oder dem Steuerungskreis einberufen.
R3|Betriebs-verantwortlicher (BeV)|Es muss in jedem BeLa eine koordinierende Stelle geben (Betriebsverantwortlicher), die die zentrale Verantwortung für Betrieb und Pflege des EfA-Onlinedienstes innehat.<br/> Der Betriebsverantwortliche muss die organisatorischen, rechtlichen, finanziellen und technischen Aufgaben sicherstellen, die für Betrieb, Support und Weiterentwicklung des EfA-Onlinedienstes notwendig sind. Außerdem ist er verantwortlich für die Umsetzung der im Steuerungskreis getroffenen Entscheidungen.<br/> In die Verantwortung des Betriebsverantwortlichen fallen mindestens folgende Aufgaben:<br/>&nbsp;&nbsp;&nbsp;&nbsp;a. den operativen Betrieb des Onlinedienstes sicherzustellen, z.B. durch die Bereitstellung einer Supportstruktur, Reporting-Aktivitäten, Kommunikationsmatrix für Incident-, Problem- und Changemanagement, etc. <br/>&nbsp;&nbsp;&nbsp;&nbsp;b. sicherstellen, dass die fachlichen Anforderungen sowie die Erkenntnisse aus dem Betrieb in die Weiterentwicklung des Onlinedienstes fließen und die technische Umsetzung nach Weisung des Steuerungskreises realisiert wird <br/>&nbsp;&nbsp;&nbsp;&nbsp;c. entsprechend den Anforderungen der MiLa sicherzustellen, dass der Onlinedienst stets auf dem aktuellen (it-sicherheits-) technischen und rechtlichen Stand ist <br/>&nbsp;&nbsp;&nbsp;&nbsp;d. das Erstellen einer Jahresplanung, z.B. zur Weiterentwicklung auf Grundlage der strategischen Entscheidungen des Steuerungskreises <br/>&nbsp;&nbsp;&nbsp;&nbsp;e. sicherzustellen, dass Governance-Strukturen existieren, die die EfA-Onlinedienste möglichst sinnvoll und ressourcenschonend bündeln <br/>&nbsp;&nbsp;&nbsp;&nbsp;f. sonstige Aufgaben nach Weisung des Steuerungskreises, z.B. Durchführung von Vergabeverfahren, Dokumentationen, Auditverfahren oder die Vor- und Nachbereitung von Steuerungskreissitzungen<br/><br/>Die Entscheidungen zu den Punkten a) bis c) trifft der Betriebsverantwortliche selbstständig. Landesspezifische rechtliche Fragen müssen in Abstimmung mit den MiLa getroffen werden, wenn es eine Expertengruppe gibt, kann diese zur Entscheidung herangezogen werden.
R4|Second Level Support (technisch) BeLa|Das BeLa muss eine zuständige Stelle für den technischen Betrieb (Second Level) des EfA-Onlinedienstes sowie den Kontakt zu möglichen Betreibern der Basiskomponenten (Third Level Support) sicherstellen.
R5|Mitnutzungsverantwortlicher (MiV)|Es muss in jedem MiLa eine koordinierende Stelle geben (Mitnutzungsverantwortlicher), die die zentrale Verantwortung für die Vertretung der Interessen des MiLa hinsichtlich Support und Weiterentwicklung des EfA-Onlinedienstes innehat.<br/>Der Mitnutzungsverantwortliche muss mindestens folgende Aufgaben sicherstellen:<br/>&nbsp;&nbsp;&nbsp;&nbsp;a. Weiterentwicklungsbedarf sowie Statistiken über häufig vorkommende Probleme (fachlich, technisch) aus allen Verwaltungsebenen gebündelt für das BeLa bereitstellen<br/>&nbsp;&nbsp;&nbsp;&nbsp;b. Service Desk (First Level Support) und fachlichen sowie technischen Support sicherstellen (Second Level Support)<br/>&nbsp;&nbsp;&nbsp;&nbsp;c. Steuerung IT-Dienstleister im MiLa<br/>&nbsp;&nbsp;&nbsp;&nbsp;d. die Aufgaben des MiLa im Zuge der Anbindung (unter Berücksichtigung des Anbindungsleitfadens) an einen EfA-Onlinedienst<br/>&nbsp;&nbsp;&nbsp;&nbsp;e. ist Ansprechpartner für den Betriebsverantwortlichen
R6|Second Level Support (fachlich) MiLa|Das MiLa muss eine verantwortliche Stelle für den fachlichen Support (Second Level) des EfA-Onlinedienstes sicherstellen.
R7|Second Level Support (technisch) MiLa|Das MiLa muss eine zuständige Stelle für den technischen Support (Second Level) der landeseigenen Komponenten im Zusammenhang mit dem EfA-Onlinedienst und den Kontakt zu möglichen weiteren landesinternen IT-Dienstleistern (Third Level Support) sicherstellen.
R8|Service Desk / First Level Support MiLa|Das MiLa soll einen regional verfügbaren First Level Support über die 115 zur Verfügung stellen, der als Erstkontakt für Nutzende zur Verfügung steht (First Level Support). Sofern das MiLa die 115 nicht nutzen möchte, muss das MiLa den First Level Support über einen eigenen Service Desk bereitstellen.

## Mindestanforderungen an Support

![Schaubild zu Supportabläufen in der Betriebsphase je EfA-Onlinedienst](/images/efa-mindestanforderungen-betrieb/support.png)

| Nr. | Name |Anforderung |
|--|--|--|
S1|Bereitstellung First-Level-Support|Der Mitnutzungsverantwortliche soll einen First-Level-Support für Nutzende über die 115 zur Verfügung stellen, der Anfragen aufnimmt, idealerweise selbst abschließend bearbeitet und bei Bedarf an Second- und Third-Level-Support weitergibt. Sofern das MiLa die 115 nicht nutzen möchte, muss das MiLa den First-Level-Support über einen eigenen Service Desk bereitstellen.
S2|Verantwortlichkeiten Second- und Third-Level-Support|Sowohl der Betriebsverantwortliche als auch der Mitnutzungsverantwortliche müssen in einer standardisierten, vom Betriebsverantwortlichen vorbefüllten Kommunikationsmatrix für technische und fachliche Fragen Routing-Ziele (Kommunikationswege) für Tickets für Second- und Third-Level-Support zur Verfügung stellen, die von allen Beteiligten im MiLa und ggf. Kommunen genutzt werden müssen. Dabei können der Betriebsverantwortliche und der Mitnutzungsverantwortliche die zulässigen Kommunikationswege einschränken, um Effizienz und Kosten kontrollieren zu können.
S3|Wissensmanagement (FAQ; Workarounds; Kategorisierungen)|Der Betriebsverantwortliche muss FAQs und Schulungsunterlagen in strukturierten, für die telefonische Beauskunftung passenden Formaten mit Informationen für den First-Level-Support den Mitnutzungsverantwortlichen und Kommunen bereitstellen und stets aktuell halten. Das Format muss so geeignet sein, dass die Mitnutzungsverantwortlichen und Kommunen die Informationen an die eigenen Gegebenheiten anpassen und entsprechend erweitern können. Die Beauskunftung im First-Level-Support, so sie durch 115-Service Center erfolgt, stützt sich auf die Leistungsbeschreibungen im XZuFi-Standard und die dort hinterlegten Informationen gemäß der 115-Lotsenfunktion.
S4|Übergreifende Ticketverantwortung|Das mitnutzende Land muss eine Instanz etablieren, die eine Nachverfolgung der Tickets hinsichtlich ihrer Erledigung ermöglicht.
S5|Ticketsystem|Das Routing von Tickets über Organisations- und Bundesländergrenzen hinweg sollte über Ticketsysteme stattfinden. In jedem Fall muss eine strukturierte und nachhaltige Übergabe von Informationen sichergestellt werden. Das System für den Austausch muss für jedes Routingziel in der Kommunikationsmatrix festgelegt werden. Sofern keine geeignetere Lösung zur Verfügung steht, sollte das Routing über eine E-Mail-Schnittstelle erfolgen, dabei müssen die E-Mail-Empfangsadressen über das Verbindungsnetz zwischen Bund und Ländern erreichbar sein. Tickets können priorisiert und kategorisiert werden
S6|Reporting MiLa|Der Mitnutzungsverantwortliche muss dem Betriebsverantwortlichen Statistiken über häufig vorkommende Probleme und Rückfragen zur Verfügung stellen, um eine Weiterentwicklung des OD zu ermöglichen. Das Reporting soll regelmäßig und standardisiert erfolgen.
S7|Reporting BeLa|Der Betriebsverantwortliche muss Störungen und Fehler des OD zeitnah im Wissensmanagement veröffentlichen.
S8|Service Level|Die in der Kommunikationsmatrix hinterlegten Routing-Ziele für Second- und Third-Level-Anfragen in den BeLa müssen Service Level anbieten. Die Service Level müssen mindestens Service-, Reaktions- und Erledigungszeiten beinhalten.

## Glossar

| Begriff | Definition |
|--|--|
Anbindungsleitfaden| Der Anbindungsleitfaden beschreibt, was eine mitnutzende Behörde tun bzw. vorbereiten muss, damit der Dienst angeschlossen werden kann (auch organisatorisch). Wird vom BeLa zur Verfügung gestellt.
Betreibendes Land (BeLa)|Land, welches einen Onlinedienst federführend entwickelt und im Anschluss betreibt.
Betriebsverantwortlicher (BeV)| Steuert den Betrieb des EfA-Onlinedienstes kontinuierlich. Er ist u.a. verantwortlich für das operative Tagesgeschäft, die Koordination und Abstimmung mit den IT-Dienstleistern, Abrechnung und Nachhalten von Anschluss- und Betriebskosten, Koordinierung der vom Steuerungskreis beauftragten Weiterentwicklungen sowie die Vorbereitung der Sitzungen des Steuerungskreises.
Change Management| Das Change-Management als Bestandteil von IT-Betriebsprozessen dient der kontrollierten, standardisierten Steuerung geplanter Änderungen. Der Change Management-Prozess legt den Rahmen für Veränderungen fest, prüft und bewertet Änderungsanträge (Change Requests), genehmigt diese oder lehnt sie ab und stellt eine Qualitätssicherung nach der Durchführung sicher.
Digitalisierte Verwaltungsleistung| Eine OZG-Leistung gilt daher als online, wenn mindestens eine zugehörige Verwaltungsleistung den Reifegrad 2 erreicht hat, eine Online-Beantragung also grundsätzlich möglich ist, Nachweise jedoch noch nicht regelmäßig übermittelt werden können (und im Digitalisierungsprogramm Föderal in mindestens einer Kommune verfügbar ist). Oberstes Prinzip bei der Digitalisierung von Leistungen ist die Nutzerorientierung. Die Umsetzung ist dann erfolgreich, wenn die Onlinedienste von Bürger:innen sowie Unternehmen tatsächlich genutzt werden.
EfA - Service / EfA-Onlinedienst| "Einer für Alle" bedeutet, dass ein Land oder eine Allianz aus mehreren Ländern eine Leistung zentral entwickelt und betreibt – und diese anschließend anderen Länder und Kommunen zur Verfügung stellt, die den Dienst dann mitnutzen können. Hierfür müssen sie sich mittels standardisierter Schnittstellen anbinden. Die Kosten für Betrieb und Weiterentwicklung des Dienstes teilen sich die angeschlossenen Länder bzw. Kommunen.
Erledigungszeit (Lösungszeit) | Zeitraum, innerhalb dessen der OD-Bereitsteller die Störungs- bzw. Mängelbehebungsarbeiten erfolgreich abzuschließen hat.
First Level Support / Service Desk| Der First Level Support (auch Endanwendersupport) ist die erste Anlaufstelle für Bürgerinnen und Bürger bei Fragen zur Nutzung von Onlinediensten der Verwaltung. Der First Level Support dokumentiert und qualifiziert eingehende Anfragen und versucht diese durch die Zuhilfenahme eines Wissensmanagements fallabschließend zu bearbeiten. Ist eine fallabschließende Bearbeitung nicht möglich, kann eine Weiterleitung an den Second Level Support erfolgen. Im Rahmen des Betriebes und des Einsatzes von EfA-Onlinediensten soll die 115 als First Level Support etabliert werden.
Frequently Asked Questions (FAQ)|Frequently Asked Questions, kurz FAQ oder FAQs, englisch für häufig gestellte Fragen oder auch meistgestellte Fragen, sind eine Zusammenstellung von oft gestellten Fragen und den dazugehörigen Antworten zu einem Thema, in diesem Fall zu den Onlinediensten und den dazugehörigen Prozessen.
Governance|Governance – oft übersetzt als Regierungs-, Amts- bzw. Unternehmensführung bezeichnet allgemein das Steuerungs- und Regelungssystem im Sinn von Strukturen einer Organisation.
Kommunikationsmatrix|Die Kommunikationsmatrix ist ein Projektmanagementwerkzeug zur Dokumentation der Kommunikationswege im Rahmen eines Projekts oder einer Organisation. Innerhalb dieser Matrix stehen alle Kommunikationssender und Kommunikationsempfänger, welche für den Support der EfA-Onlinedienste interessant sind.
Mitnutzendes Land (MiLa)|Land, welches einen Onlinedienst als SaaS nutzt. Entscheidungsbefugt ist  ein Land erst, wenn es den OD bezahlt.
Mitnutzungsverantwortlicher (MiV)|Ist verantwortlich für die Vertretung der Interessen des MiLa hinsichtlich Support und Weiterentwicklung des EfA-Onlinedienstes
Nutzende|Sind Bürger:innen und Unternehmen
OD-Bereitsteller|Der IT-Dienstleister, der den Onlinedienst sowie die benötigte serverseitige Infrastruktur und IT-Services zur Verfügung stellt.
Online - Dienst (Synonym: Online-Leistung, Online-Verwaltungsleistung)|Ist ein Angebot (Antragsstrecke bzw. Online-Prozess nebst Schnittstellen) bzw. eine Dienstleistung, die über das Internet genutzt werden kann.
OZG-Leistung|„Der Begriff OZG-Leistung beschreibt ein Leistungsbündel bestehend aus bis zu mehreren Hundert einzelnen Verwaltungsleistungen aus dem "Leistungskatalog der öffentlichen Verwaltung" (LeiKa), die thematisch aus Nutzersicht zusammenhängen. Es gibt 575 OZG-Leistungen, davon werden 115 im Bundesprogramm und 460 im Digitalisierungsprogramm Föderal umgesetzt“.
Personalunion|Unter Personalunion versteht man die Ausübung verschiedener nicht miteinander verbundener Ämter oder Funktionen durch dieselbe Person.
Problem|Die gemeinsame unbekannte Ursache eines oder mehrerer sich wiederholender identischer oder ähnlicher Störungen (ITIL).
Qualitätssicherung (QS)|Qualitätssicherung oder Qualitätskontrolle ist ein Sammelbegriff für unterschiedliche Ansätze und Maßnahmen zur Sicherstellung festgelegter Qualitätsanforderungen.
Second Level Support|Der Second Level Support unterstützt den First Level Support durch Übernahme komplexerer Anfragen. Bei fachlichen Fragen ist dies Aufgabe des MiLa und bei technischen Fragen ist dies Aufgabe von MiLa und BeLa. Neu erarbeitete Lösungen werden in Wissensdatenbanken eingepflegt, um das Wissen für den First Level Support nutzbar zu machen. Übersteigt die Komplexität einer Anfrage das Know-how oder die technischen Möglichkeiten des Second Level Supports, so wird diese an den Third Level Support weitergeleitet („eskaliert“), z.B. Tickets, deren Lösung einen Eingriff in die Programmlogik oder in Daten der Datenbank erfordert.
Service Level Agreement (SLA)|Vereinbarung zwischen Dienstleistungserbringer und Auftraggeber, welche Leistungen in welcher Leistungsqualität und in welchem Leistungsspektrum (z.B. Zeit, Umfang) erbracht werden.
Servicezeit (Betreute Betriebszeit)|Zeiten, innerhalb derer der Second Level Support des OD-Bereitstellers anwesend und erreichbar ist und während derer Störungs- bzw. Mangelbehebungsarbeiten stattfinden.
Servicezeit (Betriebszeit)|Zeiten, in denen der Auftraggeber bzw. der Nachnutzer* Anspruch auf Bereitstellung der Leistung hat, der EfA-Onlinedienst also verfügbar ist und genutzt werden kann.
Software as a Service (SaaS)|Das SaaS-Modell basiert auf dem Grundsatz, dass Software von einem externen IT-Dienstleister über das Internet zur Nutzung bereitgestellt wird.  Betrieb und Wartung der Software werden als Dienstleistung (Service) angeboten und abgerechnet. Die Kunden greifen web-basiert auf die Software zu und benötigen keine eigene IT-Infrastruktur.
Steuerungskreis|Der Steuerungskreis trifft die strategischen Entscheidungen für Betrieb und Weiterentwicklung des Onlinedienstes sowie die Release- und die Kostenplanung. Er besteht z.B. aus Vertreter:innen der Fachreferate der mitnutzenden Länder, des zuständigen Bundesministeriums, Vertreter:innen der Bund-Länder-AGs und Vertreter(n) des betreibenden Landes.
Support|Im Kontext der OZG Onlinedienste sprechen wir beim Thema Support über den Kontakt zwischen Bürger:innen / Unternehmen und Verwaltung. Für manche wird der Begriff mit technischen Fragen assoziiert. In unserem Verständnis sowohl fachlich/inhaltliche sowie technische Fragen.
Third Level Support|Der Third Level Support setzt sich aus Spezialisten einzelner Fachabteilungen, des OD-Bereitstellers oder der beteiligten Hersteller zusammen und stellt so die höchste Eskalationsstufe innerhalb einer Support-Organisation dar.
Ticket System|Ein Ticket-System ist eine Software, um Empfang, Bestätigung, Klassifizierung, Bearbeitung und Lösung von Kundenanfragen zu handhaben. Als Anfragen werden eingehende Kundenanrufe, E-Mails und Ähnliches betrachtet.

## Fußnoten
