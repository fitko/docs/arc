---
title: Servicestandard für die OZG-Umsetzung
---

:::info Zusammenfassung
Der Servicestandard definiert 19 Qualitätsprinzipien und ist die offizielle Empfehlung von Bund und Ländern zur Digitalisierung von Verwaltungsleistungen für Bürger:innen und Unternehmen.

Als Empfehlung für die Digitalisierung von Verwaltungsleistungen ist der Servicestandard eine Hilfestellung für alle Beteiligten bei der Entwicklung und Optimierung digitaler Verwaltungsangebote im Rahmen der OZG-Umsetzung und darüber hinaus.
Die Anwendung des programmübergreifenden Servicestandards soll dabei sicherstellen, dass die Digitalisierung von Verwaltungsleistungen nutzer:innenfreundlich vonstattengeht.

Der Servicestandard wird ergänzt durch das zugehörige [Servicehandbuch des Nationalen Normenkontrollrats](https://servicehandbuch.de/), das weitere Unterstützung zur Umsetzung der Prinzipien des Servicestandards bietet (:warning: derzeit nicht verfügbar ).
:::

## Primärquellen
- [Der Servicestandard für die OZG-Umsetzung auf der Webseite des BMI](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/servicestandard/servicestandard-node.html)

## Weitere ergänzende Materialien
- [:warning: derzeit nicht verfügbar] Ergänzendes [Servicehandbuch des Nationalen Normenkontrollrats](https://servicehandbuch.de/) mit konkreten Hinweisen für die Umsetzung des Servicestandards
- Eine [Checkliste zum Servicestandard](https://github.com/digitalservicebund/public_documents/blob/main/templates/OZG-Servicestandard-Checklist.md) des [DigitalService](https://digitalservice.bund.de/)