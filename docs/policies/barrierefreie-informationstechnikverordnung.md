---
title: Barrierefreie-Informationstechnik-Verordnung 
---

:::info Zusammenfassung
Die [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) als Umsetzung des [§12 b des Behindertengleichstellungsgesetzes](https://www.gesetze-im-internet.de/bgg/BJNR146800002.html) beschreibt Anforderungen an die barrierefreie Gestaltung von Websites, Webanwendungen, mobilen Anwendungen, elektronisch unterstützten Verwaltungsabläufen und grafischen Programmoberflächen – also jegliche IT-Lösungen. 
Sie gilt für die öffentliche Verwaltung des Bundes.
:::

-----

# Barrierefreie-Informationstechnik-Verordnung (BITV 2.0)
Mit der [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) werden die Anforderungen an die barrierefreie Gestaltung von Websites, Webanwendungen, mobilen Anwendungen, elektronisch unterstützten Verwaltungsabläufen und grafischen Programmoberflächen – also jegliche IT-Lösungen - präzisiert. 
Sie gilt für die öffentliche Verwaltung des Bundes.

Die BITV 2.0 ist eine Verordnung und setzt den [§12 b des Behindertengleichstellungsgesetzes](https://www.gesetze-im-internet.de/bgg/BJNR146800002.html) um. 
Sie wurde vom BMAS (Bundesministerium für Arbeit und Soziales) erlassen und legt die technischen Standards zur barrierefreien Gestaltung und die dazugehörigen Fristen fest.

## Ziele
Ziel der [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) ist die Ermöglichung und Gewährleistung einer barrierefreien Gestaltung der IT-Lösungen öffentlicher Stellen des Bundes.

>**§ 1 BITV 2.0 - Ziele**  
>1. Die Barrierefreie-Informationstechnik-Verordnung dient dem Ziel, eine umfassend und grundsätzlich uneingeschränkt barrierefreie Gestaltung moderner Informations- und Kommunikationstechnik zu ermöglichen und zu gewährleisten.
>
>2. Informationen und Dienstleistungen öffentlicher Stellen, die elektronisch zur Verfügung gestellt werden, sowie elektronisch unterstützte Verwaltungsabläufe mit und innerhalb der Verwaltung, einschließlich der Verfahren zur elektronischen Aktenführung und zur elektronischen Vorgangsbearbeitung, sind für Menschen mit Behinderungen zugänglich und nutzbar zu gestalten.

Laut § 3 der [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) sind Angebote, Anwendungen und Dienste der Informationstechnik barrierefrei zu gestalten. 
Dies erfordert, dass sie wahrnehmbar, bedienbar, verständlich und robust sind. 
Gemäß der [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) wird vermutet, dass die Angebote, Anwendungen und Dienste barrierefrei sind, wenn sie harmonisierten Normen oder Teilen dieser Normen entsprechen.

## Anwendungsbereich (§ 2 BITV 2.0)
Die Verordnung gilt für:

- Webinhalte (Websites, Webanwendungen),
- mobile Anwendungen,
- elektronisch unterstützte Verwaltungsabläufe einschließlich der Verfahren zur elektronischen Vorgangsbearbeitung und elektronischen Aktenführung und
- grafische Programmoberflächen,
die von den öffentlichen Stellen zur Nutzung bereitgestellt werden.

Ausgenommen sind:

- Reproduktionen von Stücken aus Kulturerbe-Sammlungen, die nicht vollständig barrierefrei zugänglich gemacht werden können, und
- Archive, die weder Inhalte enthalten, die für aktive Verwaltungsverfahren benötigt werden, noch nach dem 23. September 2019 aktualisiert oder überarbeitet wurden, sowie
- Inhalte von Websites und mobilen Anwendung von Rundfunkanstalten.

## Anforderungen und anzuwendende Standards
[BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) nennt folgende Anforderungen an die barrierefreie IT:

1. [EN 301 549](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/gesetze-und-richtlinien/en301549/en301549-artikel.html) als verbindlicher europäischer Standard (§ 3 Absatz 2 [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html))
2. Stand der Technik (§ 3 Absatz 3 [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html))
3. Höchstmögliches Maß an Barrierefreiheit (§ 3 Absatz 4 [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html))
4. Informationen in Deutscher Gebärdensprache und in Leichter Sprache (§ 4 [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html))

## Weitere Informationen zu den genannten Anforderungen an die barrierefreie IT:

### 1. Stand der Technik
Nach § 3 Absatz 2 BITV 2.0 besteht (nur) eine Vermutung, dass Websites und mobile Anwendungen barrierefrei sind, wenn die Anforderungen aus dem Standard [EN 301 549](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267330&cms_lv3=18581552#doc18581552) in seiner jeweils gültigen Fassung eingehalten werden (Beweislastumkehr).

Dementsprechend sieht § 3 Absatz 3 BITV 2.0 vor, dass Websites und mobile Anwendungen (ergänzend) nach dem Stand der Technik barrierefrei zu gestalten sind, soweit Nutzeranforderungen durch die einzuhaltenden europäischen Standards nicht abgedeckt sind.

Die folgenden [DIN-ISO-Normen](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267328&cms_lv3=18581546#doc18581546) sind dabei einzuhalten:

- [DIN EN ISO 9241](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267328&cms_lv3=18581544#doc18581544) (Ergonomie der Mensch-System-Interaktion) ergänzt durch ISO 14915 (Software-Ergonomie für Multimedia-Benutzungsschnittstellen).
- [DIN EN ISO 14289](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267328&cms_lv3=18581544#doc18581544) (Barrierefreiheit von [PDFs](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267352&cms_lv3=18581622#doc18581622)).

Beispiel für Stand der Technik bei Benutzereingabe: [HTML 5](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267336&cms_lv3=18581574#doc18581574) kann das Zahlenfeld für die Eingabe von Ziffern anstelle der virtuellen Tastatur einblenden. 
Die besseren Eingabemöglichkeiten von [HTML 5](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267336&cms_lv3=18581574#doc18581574) sollten daher genutzt werden.

### 2. Höchstmögliches Maß an Barrierefreiheit
Nach § 3 Absatz 4 BITV 2.0 sollen öffentliche Stellen des Bundes bei der barrierefreien Gestaltung 
- für zentrale Navigations- und Einstiegsangebote 
- sowie Angebote, die eine Nutzerinteraktion ermöglichen, beispielsweise 
  - Formulare sowie 
  - die Durchführung von Authentifizierungs-, Identifizierungs- und Zahlungsprozessen 
  
ein höchstmögliches Maß an Barrierefreiheit verwirklichen.

Wie lässt sich ein höchstmögliches Maß an Barrierefreiheit verwirklichen?

Insbesondere für Startseiten (Home), Navigationsmöglichkeiten und Funktionen, die eine Interaktion ermöglichen, sollen nach technischer Möglichkeit auch die Erfolgskriterien der [WCAG 2.1](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267366&cms_lv3=18581680#doc18581680) mit der [Konformitätsstufe AAA](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267342&cms_lv3=18581600#doc18581600) beachtet werden. 
Es muss gute Gründe geben, wenn diese Erfolgskriterien nicht eingehalten werden.

### 3. Informationen in Deutscher Gebärdensprache und in Leichter Sprache
Nach § 4 BITV 2.0 sind auf der Startseite (Home) einer Website folgende Erläuterungen in [**Deutscher Gebärdensprache**](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/anforderungen-an-die-it/uebergreifende-anforderungen-web-und-app/leichte-sprache-gebaerdensprache/leichte-sprache-gebaerdensprache-node.html) und in [**Leichter Sprache**](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/anforderungen-an-die-it/uebergreifende-anforderungen-web-und-app/leichte-sprache-gebaerdensprache/was-ist-leichte-sprache/was-ist-leichte-sprache.html) bereitzustellen:

- Informationen zu den wesentlichen Inhalten der Website
- Hinweise zur Navigation
- Informationen zum wesentlichen Inhalt der Erklärung zur Barrierefreiheit
- Hinweise auf weitere in dem Web-Auftritt vorhandene Informationen in [Deutscher Gebärdensprache](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267322&cms_lv3=18581526#doc18581526) und in [Leichter Sprache](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267344&cms_lv3=18581606#doc18581606) (BITV 2.0, Anlage 2)

Informationen in [Deutscher Gebärdensprache](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267322&cms_lv3=18581526#doc18581526) und in [Leichter Sprache](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/lexikon/functions/bmi-lexikon.html?cms_lv2=18267344&cms_lv3=18581606#doc18581606) gehören heutzutage zum Standard.
Öffentliche Stellen sollten diese Vorgaben daher auch dann einhalten, wenn es hierzu keine ausdrückliche rechtliche Verpflichtung gibt.  
Gleichzeitig gibt es keine Verpflichtung, alle Informationen eines Internetauftritts in leichter Sprache und Gebärdensprache anzubieten.

### 4. Erklärung zur Barrierefreiheit
Die öffentlichen Stellen sind verpflichtet, für ihre Websites und mobilen Anwendungen jeweils eine detaillierte, klar verständliche und vollständige [**Erklärung zur Barrierefreiheit**](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/service/barrierefreiheit/erklaerung.html) zu erstellen und auf ihrer Website bzw. im App-Store zu veröffentlichen. 
Das gilt auch für Web-Anwendungen, die im Sinne der BITV zu den Websites gezählt werden.
Eine genauere Beschreibung der Anforderungen an eine Erklärung zur Barrierefreiheit finden Sie unter Anforderungen an die IT – Erklärung zur Barrierefreiheit.

Die BITV trat am 24.07.2002 in Kraft, ab 22.09.2011 in erneuerter Fassung als "BITV 2.0". In der letzten und aktuellen Fassung (weiterhin „BITV 2.0“) vom 25.05.2019 werden folgende Vorgaben berücksichtigt:

- die Vorgaben der [EU-Richtlinie 2016/2102](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32016L2102&rid=1) „über den barrierefreien Zugang zu den Websites und mobilen Anwendungen öffentlicher Stellen“
- die Vorgaben des [EU-Durchführungsbeschlusses 2018/1523](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32018D1523&from=DA) „zur Festlegung einer Mustererklärung zur Barrierefreiheit“
- die Vorgaben des [EU-Durchführungsbeschlusses 2018/1524](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32018D1524&from=EN) „zur Festlegung einer Überwachungsmethodik und der Modalitäten für die Berichterstattung“
- die Vorgaben des [EU-Durchführungsbeschlusses 2018/2048](https://eur-lex.europa.eu/legal-content/DE/TXT/PDF/?uri=CELEX:32018D2048&from=DE) „über die harmonisierte Norm für Websites und mobile Anwendungen"
- die Vorgaben des EU-Durchführungsbeschlusses (EU) 2021/1339 „über die Änderung der harmonisierten Norm für Websites und mobile Anwendungen“
- Konkrete Anforderungen (erstmals) mit Verweis auf die [Europäische Norm EN 301 549](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/gesetze-und-richtlinien/en301549/en301549-node.html)

## Primärquellen
https://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html
