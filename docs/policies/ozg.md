---
title: Onlinezugangsgesetz (OZG)
---

:::info Zusammenfassung
Das Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen (Onlinezugangsgesetz) verpflichtet Bund, Länder und Kommunen, bis Ende 2022 ihre Verwaltungsleistungen über Verwaltungsportale auch digital anzubieten.
:::

## Primärquellen
- [Gesetzestext auf gesetze-im-internet.de](https://www.gesetze-im-internet.de/ozg/)
- [Übersichtsseite des BMI zum Thema OZG und Digitalisierung](https://www.onlinezugangsgesetz.de/Webs/OZG/DE/startseite/startseite-node.html)
