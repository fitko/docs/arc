# Überblick

### Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen
Das Onlinezugangsgesetz (OZG) verpflichtet Bund, Länder und Gemeinden, bis spätestens Ende 2022 ihre Verwaltungsleistungen auch elektronisch online über Verwaltungsportale anzubieten. Diese Portale sollen dann miteinander zu einem Portalverbund verknüpft (§ 1 OZG) werden, um so den Zugang zu Verwaltungsleistungen zu beschleunigen und deren Nutzung zu vereinfachen.

- [Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen (Onlinezugangsgesetz)](./ozg.md)

### EfA-Mindestanforderungen
Die Digitalisierung der deutschen Verwaltungsleistungen kann nur arbeitsteilig gelingen. Als umfassende Form der Arbeitsteilung und Zusammenarbeit wurde hierfür das „Einer für Alle“-Prinzip etabliert. „Einer für Alle“-Services sind flächendeckend einsetzbare Lösungen, die einmal nutzerzentriert konzipiert und entwickelt, fachlich betreut und technisch betrieben werden. Hier werden die Mindestanforderungen an ein Online-Dienst zur elektronischen Abwicklung von Verwaltungsleistungen definiert, damit er als „EfA-konform“ gelten kann. 

- [EfA-Mindestanforderungen](./efa-mindestanforderungen.md)

### EfA-Mindestanforderungen Betrieb
Die EfA-Mindestanforderungen Betrieb definieren Vorgaben zu Rollen und Verantwortlichkeiten für Betrieb und Support von EfA-Online-Services nach dem „Einer für Alle“-Prinzip.

- [EfA-Mindestanforderungen Betrieb](./efa-mindestanforderungen-betrieb.md)

### Föderale IT-Architekturrichtlinien
Die vom Architekturboard des IT-Planungsrat definierten Architekturrichtlinien helfen Architekturentscheidungen bei der Entwicklung von Softwarelösungen effizient zu treffen und wiederkehrende Grundsatzdiskussionen zu vermeiden. Die verpflichtende Anwendung wurde vom IT-Planungsrat mit [Beschluss 2021/37](https://www.it-planungsrat.de/beschluss/beschluss-2021-37) verbindlich festgelegt.

- [Föderale IT-Architekturrichtlinien](./foederale-it-architekturrichtlinien.md)


### Eckpunkte Umsetzung des OZG mit Mitteln des Konjunkturpakets
Mit dem IT-Planungsrat [Beschluss 2020/39](https://www.it-planungsrat.de/beschluss/beschluss-2020-39) schafft die Bundesregierung mit einem Konjunkturprogramm einen neuen Handlungsrahmen, um schnell ein
flächendeckendes digitales Verwaltungsangebot in Deutschland zu aufzubauen – und dabei
Länder und Kommunen gezielt zu entlasten. Die Voraussetzungen für die Nutzung dieser Mittel sind im folgenden beschrieben:

- [Eckpunkte Umsetzung des OZG mit Mitteln des Konjunkturpakets](./eckpunkte-umsetzung-ozg.md)


### Servicestandard für die OZG-Umsetzung
Die Nutzerorientierung ist das oberste Prinzip bei der Verwaltungsdigitalisierung, denn schließlich ist die OZG-Umsetzung nur dann erfolgreich, wenn sowohl Bürgerinnen und Bürger als auch Unternehmen die Online-Services tatsächlich nutzen. Die Anwendung des programmübergreifenden Servicestandards soll sicherstellen, dass die Digitalisierung von Verwaltungsleistungen nutzerfreundlich vonstattengeht.

- [Servicestandard für die OZG-Umsetzung](./servicestandard-ozg-umsetzung.md)


### Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes
Bund und Länder streben die kooperative, einheitliche, zukunftsweisende und effiziente Umsetzung des OZGs an und haben sich aus diesem Grund zu diesem Verwalungsabkommen geeinigt. Es wird ein interdisziplinäres Arbeiten, eine agile Arbeitsweise, arbeitsteiliges Vorgehen und die konsequente Nutzerzentrierung zugrunde gelegt. Die Digitalisierung erfolgt nach dem Modell „Einer für Alle/Einer für Viele“. 

- [Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes](./verwaltungsabkommen-ozg.md)

### Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung
Um die Chancen zu nutzen, die sich aus einer stärkeren Vernetzung der IT-Systeme von Bund und Ländern ergeben können, ist es notwendig alle beteiligten Partner auf ein angemessenes Sicherheitsniveau zu bringen. In dieser Fortschreibung der Leitlinie aus dem Jahr 2013 soll sich nun verstärkt auf die Wirkung von Sicherheitsmaßnahmen, insbesondere auf die Frage einer lückenlosen Umsetzung von Sicherheitskonzepten, und deren Messbarkeit fokussiert werden.

- [Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung](./informationssicherheitsleitlinie.md)

### Barrierefreie-Informationstechnik-Verordnung
Die [BITV 2.0](http://www.gesetze-im-internet.de/bitv_2_0/BJNR184300011.html) als Umsetzung des [§12 b des Behindertengleichstellungsgesetzes](https://www.gesetze-im-internet.de/bgg/BJNR146800002.html) beschreibt Anforderungen an die barrierefreie Gestaltung von Websites, Webanwendungen, mobilen Anwendungen, elektronisch unterstützten Verwaltungsabläufen und grafischen Programmoberflächen – also jegliche IT-Lösungen. 
Sie gilt für die öffentliche Verwaltung des Bundes.

- [Barrierefreie-Informationstechnik-Verordnung](./barrierefreie-informationstechnikverordnung.md)
