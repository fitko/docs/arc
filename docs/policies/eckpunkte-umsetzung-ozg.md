---
title: Eckpunkte zur OZG-Umsetzung
---

:::info Zusammenfassung
Das Onlinezugangsgesetz definiert die Ziele für die Digitalisierung der deutschen Verwaltungslandschaft. 
Dieser angestrebte Wandel wird vom Bund mit einem Konjunkturpaket unterstützt. 
Das folgende Dokument setzt den Rahmen für den Abruf von Fördermitteln aus diesem Konjunkturpaket.
:::

-----

# Eckpunkte Umsetzung des OZG mit Mitteln des Konjunkturpakets

## Hintergrund und politischer Auftrag
**Die Digitalisierung der Verwaltungsleistungen in Deutschland hat seit Jahren hohe
Priorität für die Bundesregierung.** 
Mit dem Beginn der Umsetzung des Onlinezugangs-gesetzes (OZG) von 2017 haben sich Bund, Länder und Kommunen gemeinsam auf den Weg gemacht, ein Ziel zu erreichen: Alle Dienstleistungen des Staates auch online zur Verfügung zu stellen. 
Die Bundesregierung hat in ihrem 2018 beschlossenen OZG-Umsetzungskonzept dabei die Nutzerfreundlichkeit der digitalen Angebote als oberstes Prinzip der Digitalisierung bestimmt. 
Maßstab des Erfolgs der OZG-Umsetzung ist die tatsächliche Nutzung der digitalen Angebote. 
Das Potenzial für Bürger:innen, Unternehmen und die Verwaltung liegt im Milliardenbereich.

**Das Konjunkturprogramm der Bundesregierung schafft mit zusätzlichen Finanzmitteln in Höhe von drei Mrd. Euro einen neuen Handlungsrahmen, um schnell ein flächendeckendes digitales Verwaltungsangebot in Deutschland zu schaffen – und dabei Länder und Kommunen gezielt zu entlasten.** 
Damit verbunden ist der klar formulierte Anspruch, im Zuge der OZG-Umsetzung ein bundesweites digitales Angebot nach dem Modell „Einer für alle“ zu schaffen. 
Im Fokus aller Digitalisierungsbemühungen stehen dabei die Leistungsempfänger:innen – Bürger:innen und Unternehmen. 
Auch stärker in den Fokus rücken nun aber auch die staatlichen Leistungserbringer:innen – vor allem die Kommunen, die den weitaus größten Teil der Verwaltungsleistungen vor Ort erbringen. 
Nur durch einen Anschluss möglichst vieler Kommunen an die im Modell „Einer für alle“ an einer Stelle entwickelten und implementierten Online-Dienste ist eine flächendeckend digitale Verwaltungslandschaft mit hohem Standard schnell und für die Kommunen kostengünstig/-frei erreichbar.

**Wesentlicher Bestandteil der Umsetzung des OZG ist eine moderne technische Infrastruktur, über die Länder und Kommunen digitale und nutzerfreundliche Verwaltungsleistungen und so ein bundesweit nutzbares Angebot schaffen.** 
Moderne Lösungen setzen eine moderne Infrastruktur voraus. 
Diese ist derzeit noch nicht überall vorhanden. 
Die Digitalisierung nach dem „Einer für alle“-Modell setzt leistungsfähige digitale Plattformen in Bund und Ländern voraus. 
Diese müssen die kosteneffiziente Bereitstellung nutzerfreundlicher Services für Leistungsempfänger:innen ermöglichen (Front-End). 
Gleichzeitig müssen sie die Anschlussfähigkeit aller Länder und Kommunen an die Services gewährleisten sowie die Anbindung der Fachverfahren (Back-End). 
Nur so kann ein bundesweit vollständig digitales und nutzerorientiertes Angebot der Verwaltungsleistungen von Bund, Ländern und Kommunen ermöglicht werden. 
Die bestehende „Landschaft“ kann dabei zu einem entsprechend leistungsfähigen „Plattform-System“ ausgebaut werden, über das schnell und effizient „Einer für alle“-Services bereitgestellt werden können.

**Mit ganzheitlichem Blick auf eine Verwaltungsdigitalisierung für Nutzer:innen, die zuständige Behörden nachhaltig entlastet und technische Innovation in der deutschen Verwaltung befördert, ergeben sich somit drei zentrale Handlungsschwerpunkte:**
- **Digitale Infrastruktur** (Plattform-System, digitale Basiskomponenten/-dienste, moderne Register)
- **Digitale Verwaltungsleistungen** (mit standardisierten Schnittstellen, insb. zu Fachverfahren)
- **Digitalkompetentes Verwaltungspersonal** auf allen föderalen Ebenen

**Der Einsatz von Mitteln aus dem Konjunkturpaket folgt 6 klaren Grundprinzipien, dabei ist als übergeordnetes Prinzip stets die Erzielung eines positiven Konjunktureffekts zu beachten:**
- **Relevanz**   
Digitalisierung erfolgt anhand der in den Handlungsschwerpunkten festgelegten Priorisierung
- **Nutzerfreundlichkeit**  
Digitalisierung ganzer „Nutzerreisen“ (zusammenhängende Leistungen (Lebens- und Geschäftslagen)), Nutzung der Vorteile der Registermodernisierung, nutzerfreundliche Basisdienste
- **Geschwindigkeit**  
Zeitliche Vorgaben müssen von allen Umsetzungsbeteiligten eingehalten werden
- **"Einer für Alle"/ Wirtschaftlichkeit**  
Ausschließlicher Mitteleinsatz für Lösungen mit übergreifendem Beitrag zur effizienten und nutzerorientierten Umsetzung des OZG und „Einer für Alle“-Lösungen“ sowie Leistungen im Bundesprogramm
- **Innovation und nachhaltige technische Qualität**  
Einsatz der Mittel nur für nachhaltige und zukunftsfähige Lösungen
- **Offene Standards und Open Source**  
Offene Standards müssen bei der Realisierung und dem Betrieb der digitalen Angebote genutzt werden. Der Quellcode aus der Realisierung digitaler Angebote der Verwaltung (Eigenentwicklung) wird nach Möglichkeit als Open Source, d. h. in nachnutzbarer Form zur Verfügung gestellt. Bereits bestehende Lizenzmodelle bleiben davon unberührt.

## Handlungsschwerpunkte
### A. Digitale Infrastruktur
**Die Weiterentwicklung der heterogenen föderalen IT-Landschaft zu einem leistungsstarken Plattform-System der digitalen Verwaltung kann schnell Mehrwerte schaffen.** 
In dem Plattform-System können zügig bundesweite Online-Dienste für Bürger:innen und Unternehmen implementiert werden, die einheitliche, mindestens aber interoperable Basisdienste verwenden. 
Standardisierte Datenübermittlungswege ermöglichen eine schnelle, weitreichende, wettbewerbsoffene Anbindung von Fachverfahren und Registern für alle Verwaltungsebenen.
Auffindbar sind die Leistungen für Nutzer:innen vor allem über das zentrale Bundesportal und Portale der Länder.

![Zieldesign Plattform-System](/images/eckpunkte-umsetzung-ozg/01-Zieldesign-Plattform-System.png)

Im **Plattform-System** lassen sich daher **vier Ebenen oder Schichten** unterscheiden:
1. **Integrationsschicht** durch IT-Architekturvorgaben (standardisierte Schnittstellen und Verzeichnisse)  
Kern des Plattform-Systems:“ zwischen Frontend 2) +4) und Backend 3)
2. **Basis- und Online-Dienste** (Basiskomponenten und digitale Verwaltungsleistungen)
3. **Backend** (Fachverfahren und Register)5
4. **Portale** (vor allem Bundesportal und Landesportale)

#### 1. Integrationsschicht durch IT-Architekturvorgaben (standardisierte Schnittstellen und Verzeichnisse)  Kern des Plattform-Systems:“ zwischen Frontend 2) +4) und Backend 3) ####

Den Kern des Plattform-Systems bilden in erster Linie gemeinsame IT-Standards im Sinne von IT-Architekturvorgaben. 
Die Länder und ihre Kommunen sollen das Plattform-System dazu nutzen können, insbesondere an ihre Fachverfahren zügig und niedrigschwellig über standardisierte Schnittstellen des Bundes die Online-Dienste anschließen zu können.

Für Datenrouting und -transport („Datendrehscheibe“ von Online-Dienst in dezentrale Fachverfahren) sind unterschiedliche Wege gangbar, je nach den konkreten Rahmenbedingungen der EfA-Leistung. 
Wesentliches Kriterium ist die Anzahl der Empfänger, also der zuständigen Stellen, an die die Daten geroutet werden müssen. 
Darüber hinaus muss unterschieden werden, ob große landesspezifische Unterschiede bei den zuständigen Behörden und es eine stabile Zuständigkeitsverteilung bestehen.

Zeitnah kann die existierende Vernetzungs-Infrastruktur aus DVDV mit XTA und OSCI so ausgebaut werden, dass sie das Antragsdatenrouting für die Anwendungsfälle der nun
anstehenden EfA-Umsetzungsprojekte ermöglicht. 
Im DVDV werden Behörden durch eindeutige Schlüssel abgebildet, sodass ein EfA-Online-Dienst dort die technische Adresse der zuständigen Behörde ermitteln kann, an welche die Antragsdaten zu übermitteln sind. 
Sollten die konkreten Behörden noch nicht im DVDV erfasst sein, werden die bereits bestehenden Pflegenden Stellen der Länder damit beauftragt. 
Durch das Zertifikatesystem des DVDV sind auch die Zugriffsrechte von Behörden auf Daten bereits abgebildet.

Parallel arbeitet die FITKO aktuell zusammen mit IT-Dienstleistern und Vertretern aus Bund und Ländern an der Lösung „FIT-Connect“, bei der die Pflege von Diensteverzeichnissen entfällt.
FIT-Connect stellt dabei eine API bereit, über die Antragsdaten an den Zustelldienst von FIT-Connect übergeben werden, der die zuständige Behörde über das Online-Gateway im Plattform-System ermittelt und die Antragsdaten entsprechend zustellt. 
Geplant ist, dass der IT-Planungsrat zeitnah einen Beschluss zur Verwendung von FIT-Connect fasst, derzeit wird die Anwendung u. a. beim länderübergreifenden Rollout der Leistung „Wohngeld“ pilotiert.

Durch offene Schnittstellen bietet das Plattform-System darüber hinaus Anknüpfungspunkte für die kontinuierliche technische Innovation durch Einbeziehung von privaten Anbietern oder Start-Ups (auch Teil der FIT-Connect-Idee). 
Damit wird auch dem Architekturprinzip explizit “Einer für Alle” oder auch “Einer für Viele” Rechnung getragen.
Wann immer IT-Dienstleister OZG-fähigeOnline-Dienste entwickeln, werden diese als deutschlandweit verfügbarer Service angeboten werden (dazu vertieft in Handlungsschwerpunkt Nr. 2).

#### 2. Basis- und Online-Dienste
In einem Plattform-System müssen zudem die für Online-Dienste (werden im Handlungsschwerpunkt „2. Digitale Verwaltungsleistungen“ näher betrachtet) zentralen Basisdienste und -komponenten abgedeckt sein.
Hierzu zählen die sichere Identifizierung (Authentifizierung), die Erbringung von notwendigen Nachweisen sowie die Entrichtung von Gebühren und die Bekanntgabe von Bescheiden. 
Als digitale Basiskomponenten und -dienste sind entsprechend zentral bereitzustellen:

- **Nutzerkonto für Bürger und für Unternehmen** (basierend auf ELSTER), inkl. digitaler Rückkanal (Postfach). Angestrebt wird einheitliches Nutzerkonto, neben dem gleichwertige und interoperable Nutzerkonten zugelassen werden können, soweit sich dadurch das Nutzererlebnis nicht verschlechtert

- Weiterentwicklung eID Großteil der Bürgerinnen und Bürger verfügen für Identifizierung über elektronische Personalausweis, bis zum Oktober 2020 wird Vollausstattung erreicht; Infrastruktur zur sicheren Übertragung des Personalausweises und weiterer Identitätskarten auf das Mobiltelefon wird errichtet, um Kopplung des Personalausweises mit dem Mobiltelefon zu vereinfachen; künftig soll Personalausweis in einem einmaligen Vorgang übertragbar und anschließend Identifizierung gegenüber Verwaltungsdienstleistungen vom Mobiltelefon aus möglich sein; konkrete Handlungsstränge:
  - **weitere Komponenten:** Verbessertes PIN-Handling (Online - Anforderung neuer PIN-Brief, Freischaltung); Entfall Gebühren beim Freischalten und neu-Setzen der PIN; Reduktion der Verwaltungsgebühren bei Einsatz der Online-Ausweisfunktion
  - **neuer Anwendungsbereich:** Zugang in die Nutzerkonten für Bürger; eID auf dem Smartphone anwendbar; neue Einsatzbereiche im privatwirtschaftlichen Verkehr
  - **breite Bewerbung** der neuen Anwendungsfälle durch OZG-Umsetzung

- **Verbesserung der bundesweit verfügbaren Bezahlkomponente ePayBL** für Einsatz bei kostenpflichtigen Leistungen

Für Ausbaustufen können weitere Funktionen vorgesehen werden, wie etwa ein Statusmonitor-
Dienst (Statusmeldungen zur Dauer und zur Phase der Antragsbearbeitung), ein
Terminvergabe-Dienst, ein Dokumenten-/Datensafe, ein Signatur- und Siegelungsdienst oder
digitale Assistenzkomponenten in Form von Navigationshilfen und Chatbots.

#### 3. Backend
Für einen schnellen Rollout und eine nachhaltige Entlastung aller Verwaltungsebenen (insbesondere auch der Kommunen in den Ländern) wird es entscheidend sein, dass auch die Back Office-Prozesse, d. h. **Fachverfahren und Register**, effizient angeschlossen werden können. 
Hierzu gibt es für Fachverfahrenshersteller klar definierte Anforderungen und Standards, wie sie ihre Produkte an das Plattform-System anschließen können. 
Dies ermöglicht es Herstellern, ihre Fachverfahren einzubinden und bundesweit zu vermarkten, wie es bereits
bei etablierten Fachstandards wie XInneres, XKfz, XJustiz, Elster möglich ist.

Mit Blick auf die SDG-Verordnung und das Once Only-Prinzip sind auch digitale Register als Teile des Plattform-Systems zu sehen. 
Durch modernisierte Register werden digitale Verwaltungsleistungen für Nutzer:innen einfacher und schneller. 
Hierzu steuert das Digitalisierungsprogramm das Datencockpit als Transparenz-Tool für Bürger:innen bei, das beschleunigt umgesetzt wird, und die Registerabruf-Komponente, mit der Nachweise durch Nutzer:innen direkt im Online-Dienst aus Registern eingeholt werden können. 
Die wichtigsten Register werden dabei auf Basis von Interoperabilitätsstandards priorisiert digital angebunden.

#### 4. Portale
Zu dem Plattform-System zählen vor allem das Bundesportal und die Landesportale, die über das Online-Gateway („Suchen und Finden“) verknüpft sind. 
Daneben gibt es bereits eine Reihe von Themen- und Fachportalen, die ebenfalls den „Zutritt“ zu digitalen Verwaltungsleistungen für Nutzer:innen ermöglichen und den Plattformverbund von Bund und Ländern über entsprechende Verknüpfungen nutzerfreundlich ergänzen können. 
Für einzelne herausgehobene Nutzergruppen wie etwa Unternehmen oder Empfänger:innen von Sozialleistungen können hier beispielsweise spezielle Themenportale, z. B. ein „Unternehmensportal“ oder ein „Sozialportal“ die Auffindbarkeit von digitalen Verwaltungsleistungen und die nutzerfreundliche Navigation (auf Basis von Geschäfts- und Lebenslagen) zusätzlich erleichtern und in themenbezogene Informationen einbetten.
Für das Bundesportal bzw. im Zusammenspiel der Portale im Plattform-System können sich daraus die folgenden Handlungsschwerpunkte ergeben:
- **Etablierung lebens- und geschäftslagenbasierter Navigation: Bürger:innen und Unternehmen** sollen einfach auf Verwaltungsleistungen zugreifen können. Hierfür ist eine durchgängige lebens- bzw. geschäftslagenbasierte Navigation zu schaffen. 
Dies kann auch durch Weiterleitung auf bestehende/neue bundesweite Themenportale erfolgen (z.B. Familienportal, Sozialplattform NRW).  
- **Zügige Implementierung des Online-Gateway** notwendig, damit alle in einer Lebenslage befindlichen Leistungen, insb. die der Länder/Kommunen, für Nutzer:innen mit wenigen Klicks erreichbar sind.

### B. Digitale Verwaltungsleistungen
#### 1. Digitalisierungsprogramm Föderal
Das Digitalisierungsprogramm Föderal setzt seit Programmstart alle Kraft auf eine Umsetzung der digitalen Verwaltungsleistungen nach dem **Modell „Einer für Alle“ (EfA)**. 
Im Kern bedeutet dies, dass eine OZG-Leistung von einer Stelle aus entwickelt und betrieben wird und diese Leistung dann anderen Ländern oder dem Bund zur Nutzung zur Verfügung steht.

!["Einer für Alle"-Leistungen](/images/eckpunkte-umsetzung-ozg/02-EfA.png)

EfA-Leistungen haben eine Reihe von Vorzügen, insbesondere hinsichtlich **Nutzerfreundlichkeit**, **Umsetzungskapazität** der Verwaltung und **Ressourceneffizienz**.
Nutzer:innen können mit einem einzigen Online-Dienst in Deutschland ihr Anliegen erledigen – beispielsweise ihren BAföG-Antrag digital stellen. 
Dafür wird ein Online-Dienst für eine Verwaltungsleistung einmal attraktiv und nutzerfreundlich entwickelt und betrieben – anstatt 16 Mal (auf Landesebene), 400 Mal (auf Ebene der Kreise) oder noch häufiger.

Mit den zusätzlichen Mitteln des Bundes für die OZG-Umsetzung und der etablierten arbeitsteiligen Struktur des Digitalisierungsprogramms bietet sich die Chance, für § 4 OZG-Leistungen zielgerichtet, koordiniert und beschleunigt EfA-Leistungen zu realisieren, um die gesamtstaatlichen Potenziale zu heben. 
Das Digitalisierungsprogramm Föderal kann so die Bereitstellung von digitalen Leistungen durch Mittel für die Konzeption und Entwicklung von Leistungen sowie durch Verwendung der standardisierten Programmmethodik mit Nachdruck vorantreiben.

**Aus den Themenfeldern** können die entsprechenden Teile **bestehender Umsetzungs-/Leistungspakete für die Digitalisierung von Nutzerreisen (Lebens- und Geschäftslagen)** schnell beauftragt werden. 
Die Digitalisierung von aus Nutzersicht zusammenhängenden
Leistungen im Rahmen eines integrierten Umsetzungsprojektes – statt einzelner leistungsspezifischer Umsetzungsprojekte – bietet ein **großes Beschleunigungspotential und stärkt die Nutzerperspektive** weiter.

![Themenfelder](/images/eckpunkte-umsetzung-ozg/03-Themenfelder.png)

Es bestehen **zwei zentrale Wege, um die Mittel des Konjunkturpakets zur schnellen und umfangreichen Leistungsdigitalisierung einzusetzen**. 
Soweit dies möglich ist, wird der im Folgenden unter 1. beschriebene Weg gewählt. 
Dafür müssen die federführenden Länder gemäß Themenfeld-Aufteilung politische Leitungsunterstützung, die fachlichen Ressourcen und die notwendige IT-Dienstleister-Kapazität bereitstellen sowie sich auf bestimmte Umsetzungskriterien verpflichten. 
Sollte dieser Weg aufgrund des Fehlens der zuvor benannten Voraussetzungen oder auf Wunsch der Länder selbst nicht beschritten werden können, ist der unter 2. dargestellte Weg die zu wählende Alternative.

1. **Mit den Ländern:** Zurverfügungstellung der Mittel durch den Bund im Rahmen einer übergreifenden Kooperationsvereinbarung zwischen Bund und Ländern, die als Grundlage für bilaterale Verträge zwischen in dem im jeweiligen Themenfeld federführenden bzw. umsetzungskoordinierenden Bundesressort und Land dient
2. **Mit externen Dienstleistern:** Im Rahmen bestehender Vertragssituationen des Bundes und in neuen OZG-Rahmenverträgen; direkter Zugriff auf breites Spektrum an Dienstleistern ("freier Markt").

![Digitalisierungs- und Beauftragungswege](/images/eckpunkte-umsetzung-ozg/04-Beauftragungsweg.png)

Die finanzielle Unterstützung von Umsetzungsprojekten mit Mitteln des Konjunkturpakets im Programm Föderal muss – unabhängig vom gewählten Weg (s. oben) – an klare Voraussetzungen und Umsetzungskriterien für eine Digitalisierung nach dem EfA-Prinzip geknüpft sein.

**Voraussetzungen:**
1. Politische Unterstützung: Offizielle Übernahme der Verantwortung durch politische Leistungsebene (je nach Bundesressort oder Land z. B Staatsekretär, CIO oder Abteilungsleiter) zur flächendeckenden EfA-Umsetzung.
2. Fachliche Unterstützung: Feste Zusage von Ressourcen aus den Fachabteilungen (ggf. mit Unterstützung des Projektmanagements) zur Umsetzung der Leistungen.
3. Bereitstellung IT-DL-Kapazitäten: Zusage von verfügbaren und dann planbarer Kapazitäten durch Dienstleister

**Umsetzungskriterien:**
1. Nutzerfreundliche Umsetzung (Anwendung der Methode „Digitalisierungslabor“ bei komplexen Leistungen und Massenverfahren; Digitalisierung von Nutzerreisen zusammenhängend, wenn möglich)
2. Umsetzung in „Einer für Alle“-Lösungen, inklusive neutraler, bundesweit einsetzbarer Nutzer-Oberfläche (z. B. Logo des Landes oder der zuständigen Behörde zusätzlich anpassbar)
3. Entwicklung einer offenen Standard-Schnittstelle zur Anbindung von Fachverfahren unter Einbindung der Hersteller (FIT-Connect zu berücksichtigen)
4. Nutzung der programmgeschaffenen Standards (z. B. standardisierte Umsetzungsmethodik, dokumentiert im OZG-Leitfaden, oder zentral bereitgestellte Basiskomponenten), alternativ Bereitstellung einer mindestens gleichwertigen Lösung
5. Aktive Einbindung der föderalen Fachakteure, z.B. Bund-Länder-Arbeitsgruppen
6. Technischer Anschluss einer signifikanten Anzahl von Ländern und deren Kommunen (z. B. mindestens 9 Länder) an die entwickelte Lösung, so dass eine tatsächliche „Einer-für-Alle“-Umsetzung erfolgt und eine signifikante Flächendeckung erreicht ist.
7. Umsetzungszeitplan auf Basis der zeitlichen Leitplanken des Aufwandschätzmodells des BMI

Daraus folgt: Es kann **keine Finanzierung von mehreren inhaltsgleichen Umsetzungsprojekten** erfolgen. Das Schaffen der genannten Voraussetzung und Einhalten der Umsetzungskriterien ist verpflichtend, um Mittel aus dem Konjunkturprogramm zu erhalten.

Um die **Nachnutzung** der „Einer für Alle“ Lösungen zu ermöglichen, müssen die notwendigen rechtlichen Rahmenbedingungen zwischen den Ländern geschaffen werden.
Hierfür gibt es bisher als Hilfestellung die Blaupause Verwaltungsabkommen. 
Diese Option bleibt auch weiterhin bestehen. 
Um das Verfahren der Nachnutzung mit den Ländern weiter zu vereinfachen, wird als Alternative der FIT-Store etabliert. 
Der **FIT-Store ist der zentrale Weg**, über den die rechtliche Dimension der Nachnutzung in Zukunft gelöst wird.

#### 2. Digitalisierungsprogramm Bund
Für die Umsetzung des Digitalisierungsprogramms Bund insgesamt und vor allem **für die politische Zielsetzung einer Beschleunigung schafft das Konjunkturpaket die Voraussetzungen und verleiht der klaren Priorisierung der OZG-Umsetzung Nachdruck.**

Das Digitalisierungsprogramm Bund umfasst politische und organisatorische Maßnahmen, um die Umsetzung entscheidend voranzutreiben. 
Die Mittel des Konjunkturpakets werden daher für folgende Bereiche eingesetzt:
- **Ertüchtigung Infrastruktur/Fertigungsstraße:** Unterstützung der Entwicklung und Funktionalität, insb. mit dem Ziel, alle Bundesleistungen im Bundesportal beantragen zu können (Oberflächenintegration)
- **Unterstützung von Betrieb und Wartung** des Bundesportals bzw. der Fertigungsstraße
- **Verstärkte Unterstützung bei der Projektorganisation und -umsetzung** in den Ressorts und GB-Behörden

Zusätzlich unterstützt die Bereitstellung von Mitteln an die Ressorts die **(Weiter-)Entwicklung relevanter Back-End Verfahren**, insb. im Bereich der relevanten Schnittstellen

**Voraussetzungen:**
1. Politische Unterstützung auf Seiten der Ressorts, deutliche Priorisierung der Digitalisierungsprojekte
2. Fachliche Unterstützung: Feste Zusage von Ressourcen aus den Fachabteilungen (ggf. mit Unterstützung des Projektmanagements) zur Umsetzung der Leistungen.
3. Bereitstellung IT-DL-Kapazitäten: Bereitstellung von Mitteln verbunden mit Zusage von verfügbaren und dann planbarer Kapazitäten durch Dienstleister

**Umsetzungskriterien:**
1. Erfolgreiche Umsetzung und Online-Stellung des Bundesportals
2. Weitgehende Nutzung der Fertigungsstraße durch die Ressorts
3. Zentrale Umsetzung der für alle Ressorts relevanten Verwaltungsleistungen (z.B. IFG-Auskunft)
4. Entwicklung von Standard-Schnittstellen zur Anbindung von Standardkomponenten (z. B. E-Akte) und Fachverfahren
5. Nutzung der programmgeschaffenen Standards bei der Umsetzung der Online-Formulare

Daraus folgt: Es sollte **keine pauschale Finanzierung von Umsetzungsprojekten bei den Ressorts** geben. Das Schaffen der genannten Voraussetzung und Einhalten der Umsetzungskriterien ist verpflichtend, um Mittel aus dem Konjunkturprogramm zu erhalten.

#### 3. Zentrale Bereitstellung von sogenannten „Querschnittsleistungen“ durch BMI
Das BMI entwickelt und stellt digitale Verwaltungsleistungen bereit, die für andere Verwaltungsverfahren einen Nachweis- oder Querschnittscharakter aufweisen (z.B. Urkunden, Auskünfte, Bescheinigungen). Dazu zählen ausgewählte Leistungen des gleichnamigen OZG- Themenfeldes „Querschnittsleistungen“:
- Datencockpit als zentrale datenschutzkonformer Registerabruf-Komponente 
- Geburtsurkunde und -bescheinigung, exemplarischer Fall für zentrale Nachweisabruf-Komponente für § 4 OZG-Leistungen
- Meldebescheinigung und Registerauskunft
- Personalausweis
- Beglaubigungen
- Apostille und Legalisation
- Krankenversicherungsnachweis
- Führungszeugnis

#### 4. Kontinuierliche technische Innovation durch Einbeziehung von Start-Ups
Im Programm werden systematisch technische Innovationen insbesondere hinsichtlich der kontinuierlichen Verbesserung der Nutzerfreundlichkeit von Online-Verfahren einbezogen. 
Dazu gehört der Einsatz von KI in Form von Navigatoren zur Anliegens- und Leistungsklärung, Chatbots und oder andere Formen von digitalen Antragsassistenten. 
Für die Entwicklung und Verwendung solcher innovativer Assistenz-Tools werden u. a. Start-Ups eingebunden.

Deutschland – und insb. Berlin – verfügt über eine der leistungsfähigsten Start-up-Landschaften der Welt. 
Dieses Potenzial gilt es auch für die Umsetzung des OZG zu heben. 
BMI kann zentral Mittel für ein „Anreizprogramm Start-ups“ bereitstellen. 
Im offenen Angebotswettbewerb können sich Start-ups dabei auf die Förderung für die Entwicklung von bestimmten Basiskomponenten bewerben (z.B. Chatbot als Assistenz beim Leistungsantrag) oder auf die Digitalisierung kleiner Leistungen im Auftrag des BMI (z.B. Leistungen des Bundesprogramms).

-----

## Primärquellen
- [IT-PLR-Beschluss 2020/39: OZG-Umsetzung (Digitalisierung von Verwaltungsleistungen) Konjunkturpaket](https://www.it-planungsrat.de/beschluss/beschluss-2020-39)
