---
title: Föderales IT-Architekturboard
---

# Informationen zum Föderalen IT-Architekturboard

Das Föderale IT-Architekturboard (FIT-AB) ist ein mit Vertreter:innen aus Bund, Ländern und der Föderalen IT-Kooperation (FITKO) besetztes Gremium des IT-Planungsrates (IT-PLR). Es unterstützt und berät den IT-Planungsrat in strategischen Fragen bei der Entwicklung und Umsetzung von Maßnahmen der föderalen IT-Architektur.

Die folgenden Seiten beinhalten Informationen zur Struktur, Organisation und zu den Abläufen des FIT-AB. Darüber hinaus werden hier die wesentlichen Ergebnisse der Arbeit des Gremiums fortlaufend veröffentlicht. Das Informationsangebot dient einerseits der Dokumentation von zentralen Arbeitsgrundlagen und Prozessen, die für die Zusammenarbeit im und mit dem FIT-AB wichtig sind. Andererseits ist es ein Anliegen des FIT-AB, Transparenz herzustellen über die Aktivitäten des zentralen Gremiums des IT-Planungsrates für Fragen der föderalen IT-Architektur. Damit wird auch der herausgehobenen Bedeutung von Architekturfragen für die föderale Verwaltungsdigitalisierung Rechnung getragen. Das Informationsangebot richtet sich sowohl an Mitglieder des FIT-AB als auch an alle weiteren Anspruchsgruppen, deren Tätigkeit unmittelbar oder mittelbar die föderale IT-Architektur berührt (insb. Verantwortliche von architekturrelevanten Produkten und anderen Bausteinen der föderalen IT-Landschaft sowie Projekten, Vorhaben und Standards).

:::tip Hinweis
Informationen zu den Aufgaben und der Rolle der FITKO im Föderalen IT-Architekturboard findet sich auf der [Webseite der FITKO](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard).
:::
