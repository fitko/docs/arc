---
titel: "Sitzungsüberblick"
---

# Überblick Sitzungen

Auf den folgenden Unterseiten finden Sie einen Überblick über die [Sitzungstermine und -unterlagen](./sitzungstermine.md) für die Regel- und Sondersitzungen des FIT-AB sowie Informationen zur [Anmeldung von Tagesordnungspunkten](./top.md). 
Die Unterlagen aus den Status-Update-Sitzungen zu den laufenden FIT-AGs sind in den jeweiligen [Vorhabens-Tickets auf openCode verlinkt](https://gitlab.opencode.de/it-planungsrat/fit-ab/vorhaben/-/boards).

Im Folgenden finden Sie eine Übersicht über die verschiedenen [Sitzungsformate](#sitzungsformate) des FIT-AB.

# Sitzungsformate

Das FIT-AB verfügt über drei Sitzungsformate, die unterschiedlichen Zwecken dienen.

**Regelsitzungen** sind das zentrale Format für die formale Gremientätigkeit des FIT-AB. Hier werden strategische Fragestellungen diskutiert, Entscheidungen vorbereitet und Beschlüsse gefasst. In den Regelsitzungen findet auch der Austausch mit Anspruchsgruppen und Stakeholdern statt, deren Arbeit Auswirkungen auf die föderale Architektur hat. Es finden 5 bis 6 Regelsitzungen pro Jahr statt. Für Regelsitzungen gelten die auf der Seite [Anmeldung von Tagesordnungspunkten für Sitzungen des Architekturboards](../sitzungen/top.md) beschriebenen Einreichfristen.

**Sondersitzungen** können von FIT-AB-Mitgliedern einberufen werden, wenn akuter Entscheidungsbedarf besteht, der nicht bis zur nächsten Regelsitzung aufgeschoben werden kann. Dieses Format wird genutzt, wenn außerhalb einer Regelsitzung formale Beschlüsse gefasst werden müssen, die nicht im Umlaufverfahren stattfinden können. Für Sondersitzungen gelten die auf der Seite [Anmeldung von Tagesordnungspunkten für Sitzungen des Architekturboards](../sitzungen/top.md) beschriebenen Einreichfristen.

**Status-Updates** sind ein Instrument zur fortlaufenden Kontrolle und Steuerung laufender FIT-AGs. Auf den Status-Update-Sitzungen berichten die FIT-AGs zum Bearbeitungsstand ihrer Vorhaben. Es finden jährlich 5 bis 6 Status-Updates statt. Präsentierte Unterlagen müssen bis spätestens 3 Tagen vor der Sitzung eingereicht werden.

Weitere **Austausch- und Informationsformate** werden nach Bedarf initiiert, wenn keine formale Beschlussfassung angestrebt wird. Sie unterliegen nicht den formalen Einreichfristen der Regel- und Sondersitzungen.