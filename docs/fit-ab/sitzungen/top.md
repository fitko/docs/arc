---
title: "Sitzungsmanagement"
slug: "/fit-ab/top"
---

# Anmeldung von Tagesordnungspunkten für Sitzungen des Architekturboards

Die Vorbereitung von Sitzungen des FIT-AB wird über die Plattform openCode organisiert.
FIT-AB-Mitglieder haben die Möglichkeit, über den [Sitzungsmanagement-Tracker](https://gitlab.opencode.de/it-planungsrat/fit-ab/sitzungsmanagement/-/boards/1217) des Föderalen IT-Architekturboards auf openCode Sondersitzungen einzuberufen und Themen für die Tagesordnung von Regelsitzungen einzureichen (nur für FIT-AB-Mitglieder einsehbar).
Eine Anleitung zur initialen Registrierung auf openCode und zur Nutzung des Sitzungsmanagement-Trackers ist [hier einsehbar](https://nextcloud.fitko.de/f/1916748) (*nur für FIT-AB-Mitglieder*).

## Verfahren zur Anmeldung von TOPs

Neue Tagesordnungspunkte können im [Sitzungsmanagement-Tracker des Föderalen Architekturboards](https://gitlab.opencode.de/it-planungsrat/fit-ab/sitzungsmanagement/-/boards/1217) auf der openCode-Plattform angemeldet werden. Hierbei wird unterschieden zwischen Tagesordnungspunkten für Regelsitzungen und der Anmeldung einer Sondersitzung.

| [Neuen Tagesordnungspunkt für eine Regelsitzung anmelden](https://gitlab.opencode.de/it-planungsrat/fit-ab/sitzungsmanagement/-/issues/new) |
| ----- |

| [Anmeldung einer Sondersitzung](https://gitlab.opencode.de/it-planungsrat/fit-ab/sitzungsmanagement/-/issues/new?issuable_template=sondersitzung) |
| ----- |

## Timeline für Regelsitzungen

**3 Wochen vor Sitzung:** Frist zur Einreichung von TOP

**2 Wochen vor Sitzung:** Bekanntgabe der Agenda durch Vorsitz

**1 Woche vor Sitzung:** Dokumentenfreeze: Erforderliche Unterlagen müssen eingereicht sein

## Timeline für Sondersitzungen

**5 Tage vor Sitzung:** Dokumentenfreeze: Erforderliche Unterlagen müssen eingereicht sein

## Priorisierung von Tagesordnungspunkten

1. Sollten mehr Themen angemeldet werden, als in der verfügbaren Sitzungszeit behandelt werden können, entscheidet der Vorsitz über die Aufnahme von TOPs.
2. Der Vorsitz priorisiert bei der Zusammenstellung der Agenda TOPs, die einen Beschluss des FIT-AB erwirken sollen sowie TOPs,die durch den IT-PLR oder die AL-Runde initiiert wurden.
