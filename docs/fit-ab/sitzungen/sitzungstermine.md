---
titel: "Sitzungstermine und -unterlagen"
--- 

# Sitzungstermine und -unterlagen

Regelsitzungen des FIT-AB finden ca. 6 mal im Jahr statt. Außerhalb der Regelsitzungen können die Mitglieder nach Bedarf Sondersitzungen einberufen. Die Sitzungsunterlagen werden im Anschluss an die Sitzung auf dieser Seite veröffentlicht.

## Die Sitzungstermine 2025

### Regelsitzungen

|Datum|Unterlagen|
|-|-|
|27. und 28. Februar, Präsenzsitzung in Frankfurt (FITKO)|*Unterlagen der 24. Sitzung werden nach der Sitzung hier veröffentlicht*|
|28. April|*Unterlagen der 25. Sitzung werden nach der Sitzung hier veröffentlicht*|
|25. und 26. Juni, Präsenzsitzung in Berlin|*Unterlagen der 26. Sitzung werden nach der Sitzung hier veröffentlicht*|
|15. September|*Unterlagen der 27. Sitzung werden nach der Sitzung hier veröffentlicht*|
|10. und 11. November, Präsenzsitzung in Stuttgart|*Unterlagen der 28. Sitzung werden nach der Sitzung hier veröffentlicht*|


### Sondersitzungen

|Datum|Unterlagen|
|-|-|
|24. Februar: Postfach- und Kommunikationslösungen|*Unterlagen der Sondersitzung werden nach der Sitzung hier veröffentlicht*|