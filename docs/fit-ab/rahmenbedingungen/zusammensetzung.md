---
titel: "Zusammensetzung"
---

# Zusammensetzung des FIT-AB: Vorsitz und Mitglieder

Dem Föderalen IT-Architekturboard gehören aktuell folgende Mitglieder an:

- Vertreter:innen des Föderalen IT-Architekturmanagements der FITKO
- Verteter:innen des Bundes (vertreten durch das BMI)
- Vertreter:innen aus folgenden Bundesländern:
  - Freie Hansestadt Bremen
  - Brandenburg
  - Baden-Württemberg
  - Bayern
  - Hessen
  - Hansestadt Hamburg
  - Mecklenburg-Vorpommern
  - Niedersachsen
  - Nordrhein-Westfalen
  - Rheinland-Pfalz
  - Schleswig-Holstein
  - Sachsen
  - Sachsen-Anhalt
  - Thüringen

Die FITKO stellt den Vorsitz des Föderalen IT-Architekturboards. Das FIT-AB wird durch eine Geschäftsstelle (FIT-AB-Office) bei der Planung und Koordination der Gremienarbeit unterstützt (siehe [Kontakt](../kontakt.md)). Das FIT-AB-Office ist beim Vorsitz (FITKO) angesiedelt.
