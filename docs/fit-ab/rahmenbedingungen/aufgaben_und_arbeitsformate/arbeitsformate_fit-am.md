---
titel: "Arbeitsformate des FIT-AM"
---

# Arbeitsformate im Föderalen IT-Architekturmanagement
Die Bearbeitung von architekturrelevanten Aufgaben im Föderalen IT-Architekturmanagement erfolgt im Rahmen von vordefinierten Arbeitsformaten. Es werden drei wesentliche Arbeitsformate (AF) unterschieden (nähere Informationen zu den Arbeitsformaten finden sich in Kapitel 5.1 des Rahmenkonzept „Föderales IT-Architekturmanagement“):

#### Arbeitsformat 1 (AF-1): Bearbeitung als Teil der Gremientätigkeiten des FIT-AB
Dieses Arbeitsformat umfasst die formalen Gremientätigkeiten des Föderalen IT-Architekturboards in seiner unmittelbaren Eigenschaft als architektonisches Beratungsgremium des IT-Planungsrates und der AL-Runde. In diesem Arbeitsformat werden Aufgaben bearbeitet, die Beratung und/oder Beschlussfassung durch das FIT-AB erfordern. Die Befassung mit diesen Aufgaben erfolgt in einem der [Sitzungsformate des FIT-AB](../../sitzungen/index.md)

#### Arbeitsformat 2 (AF-2): Bearbeitung durch eine Arbeitsgruppe des Föderalen IT-Architekturmanagements (FIT-AG)
In diesem Arbeitsformat werden Aufgaben mit Projektcharakter und/oder hohem Beteiligungsbedarf von Bund und Ländern bearbeitet. Arbeitsgruppen werden durch einen Beschluss des FIT-AB gegründet und erhalten von diesem einen klar definierten Arbeitsauftrag (siehe [Prozess zur Planung von FIT-AGs](./fit-ags/prozess_planung.md)). Sie führen auf dieser Grundlage eine belastbare Umsetzungsplanung durch. Die Arbeit der FIT-AGs wird durch das FIT-AB und dessen Geschäftsstelle mithilfe von geeigneten Maßnahmen fortlaufend kontrolliert und gesteuert (siehe [Prozess zur Steuerung von FIT-AGs](./fit-ags/prozess_steuerung.md) ). Die Mitglieder des FIT-AB können sich freiwillig und nach eigenem Ermessen an FIT-AGs beteiligen. Die personelle Besetzung von FIT-AGs kann sich, muss sich aber nicht, mit der personellen Besetzung des FIT-AB decken.

#### Arbeitsformat 3 (AF-3): Bearbeitung als Teil der Linientätigkeiten des FITKO AM
Dieses Arbeitsformat ist für Aufgaben mit ständigen und/oder reaktiven Handlungsbedarfen sowie für Aufgaben mit starkem fachlich-technischen und/oder explorativem Charakter zu wählen. Die durch das FITKO AM als Teil seiner Linientätigkeiten wahrzunehmenden Aufgaben des FIT-AM (vgl. [Regelzuständigkeiten ](#Regelzuständigkeiten)) werden durch das FITKO AM und das FIT-AB in beidseitigem Einvernehmen  definiert und abgegrenzt.

#### Regelzuständigkeiten
Die Arbeitsteilung des FIT-AB und des FITKO AM im Föderalen IT-Architekturmanagement wird über die Zuordnung von Aufgaben zu Arbeitsformaten definiert: Jede wesentliche und mittelfristig stabile Aufgabe im Föderalen IT-Architekturmanagement wird im Regelfall in einem dem Aufgabentyp standardmäßig zugewiesenen Arbeitsformat bearbeitet. 

|Aufgaben|AF1: Gremienarbeit des FIT-AB|AF 2: Arbeitsgruppe des Föderalen IT-Architekturmanagements|AF3: Bearbeitung in Linientätigkeit des FITKO AM|
|-|-|-|-|
|**Aufgabenbereich 1: Anforderungsmanagement**||||
|A-1-I: Analyse von strategischen Zielen, Initiontiven und Vorhaben|-|-|x|
|A-1-II: Erhebung, Erfassung und Analyse von Anforderungen an die föderale IT-Architektur|-|-|x|
|A-1-III: Bewertung und Priorisierung von Anforderungen an die föderale IT-Architektur|x|-|-|
|**Aufgabenbereich 2: Weiterentwicklung der föderalen Architektur**|||
|A-2-I: Initiale Aufnahme von Ist-Zuständen der föderalen IT-Architektur|-|x|-|
A-2-II: Pflege und Dokumentation von Ist-Zuständen der föderalen IT-Architektur|-|-|x|
|A-2-III: Entwicklung und Dokumentation von Soll-Zuständen der föderalen IT-Architektur|-|x|-|
|A-2-IV: Entwicklung, Erprobung und Evaluation von Lösungsansätzen und -architekturen|-|-|x|
|A-2-V: Definition und Monitorung von Transitionsschritten der föderalen IT-Architektur|-|-|x|
|**Aufgabenbereich 3: Architekturvorgaben**|||
|A-3-I: Definition des Glossars|x|-|-|
|A-3-II: Vorschlag und Pflege eines Glossars|-|-|x|
|A-3-III: Entwicklung und Definition von Architekturvorgaben (Architekturrichtlinien, Standards, Architekturprinzipien, etc.)|-|x|-|
|A-3-IV: Evaluation von Architekturvorgaben(Architekturrichtlinien, Standards, Architekturprinzipien, etc.)|-|-|x|
|A-3-V: Entwicklung von Referenzarchitekturen und -modellen|-|x|-|
|A-3-VI: Pflege von Referenzarchitekturen und -modellen|-|-|x|
|**Aufgabenbereich 4: Architektur-Governance**|||
|A-4-I: Monitoring von föderalen Projekten, Standards, Produkten und relevanten Infrastrukturkomponente|-|-|x|
|A-4-II: Architekturreviews zur Sicherstellung der Einhaltung von Architekturvorgaben|-|-|x|
|**Aufgabenbereiche 5: Veröffentlichung und Kommunikation**|||
|A-5-I: Aufbereitung, Veröffentlichung und Pflege von öffentlichen Architekturinformationen|-|-|x|
|A-5-II: Schulung und Sensibilisierung von relevanten Zielgruppen|-|-|x|
|A-5-III: Stakeholder-Management|-|-|x|
|**Aufgabenbereich 6: Beratungsleistungen**|||
|A-6-I: Architektonische Beratung der Entscheidungsebene bei Projektentscheidungen und Beschlüssen|x|-|-|
|A-6-II: Bereitstellung von Architekturinformationen|-|-|x|
|A-6-III: Architektonische Unterstützung von Projekten, Standards, Produkten und föderalen Infrastrukturkomponenten|-|-|x|
|**Aufgabebereich 7: Methodik**|||
|A-7-I: Entwicklung von methodischen Standards und Werkzeugen|-|x|-|
|A-7-II: Pflege, Analyse, Überprüfung und Wahrung von methodischen Standards und Werkzeugen|-|-|x|
|A-7-III: Beschaffung, Konfiguration und Betrieb von Softwarewerkzeugen für das Föderale IT-Architekturmmanagement|-|-|x|
[Abbildung 3: Regelzuständigkeiten im FIT-AM]
