---
titel: "Aufgaben des FIT-AM"
---

# Aufgaben des Föderalen IT-Architekturmanagements

Die Entwicklung und Steuerung der föderalen IT-Architektur kann in folgende Aufgabenbereiche und Aufgaben heruntergebrochen werden.
Diese Abgrenzungen setzen einen Rahmen für die operative Architekturarbeit und bilden die Grundlage der Aufgabenverteilung zwischen FIT-AB und FITKO-AM im Föderalen IT-Architekturmanagement.
Detailliertere Informationen zum Gegenstand und zur Bedeutung jeder einzelnen Aufgabe finden sich in Kapitel 3 des Rahmenkonzept [„Föderales IT-Architekturmanagement“](https://www.it-planungsrat.de/fileadmin/beschluesse/2024/Beschluss2024-26_F%C3%B6derales_IT-Architekturmanagement_Rahmenkonzept_Version_2.0.pdf).

Nachfolgend wird das Tätigkeitsfeld des Föderalen IT-Architekturmanagements in konkrete Aufgabenbereiche und aus diesen abgeleitete Aufgaben heruntergebrochen. Folgende Aufgabenbereiche werden innerhalb des FIT-AM unterschieden:

- [Aufgabenbereich 1 (A-1): Anforderungsmanagement](#aufgabenbereich-1-a-1-anforderungsmanagement)
- [Aufgabenbereich 2 (A-2): Weiterentwicklung der föderalen IT-Architektur](#aufgabenbereich-2-weiterentwicklung-der-föderalen-architektur)
- [Aufgabenbereich 3 (A-3): Architekturvorgaben](#aufgabenbereich-3-architekturvorgaben)
- [Aufgabenbereich 4 (A-4): Architektur-Governance](#aufgabenbereich-4-architektur-governance)
- [Aufgabenbereich 5 (A-5): Veröffentlichung und Kommunikation](#aufgabenbereich-5-veröffentlichung-und-kommunikation)
- [Aufgabenbereich 6 (A-6): Beratungsleistungen](#aufgabenbereich-6-beratungsleistungen)
- [Aufgabenbereich 7 (A-7): Methodik](#aufgabenbereich-7-methodik)

### Aufgabenbereich 1 (A-1): Anforderungsmanagement
Der Aufgabenbereich "Anforderungsmanagement" umfasst die folgenden Aufgaben: 
- **A-1-I**: Analyse der geltenden strategischen Ziele, Beschlüsse, Initiativen und Vorhaben, insbesondere jener des IT-Planungsrates - und Ableitung der        
resultierenden strategischen Anforderungen an die föderale IT-Architektur. Dies schafft Klarheit zu den Rahmenbedingungen des Föderalen IT-Architekturmanagements und ermöglicht zielgerichtetes Enterprise-Architekturmanagement.
- **A-1-II**: Identifikation, Erhebung, Erfassung und Dokumentation von strategischen Anforderungen an die föderale IT-Architektur aus einschlägigen Quellen (z. 
B. bestehende Anforderungserhebungen bspw. bei Produkten oder sonstigen föderalen Basiskomponenten, Anspruchsträger im föderalen Kontext; geltende Beschlüsse, Gesetze und Verordnungen; Good Practices, Industriestandards und Entwicklung im Stand der Technik). Zudem werden die erfassten Anforderungen analysiert und aufbereitet, um ihre Bedeutung, Relevanz und Auswirkungen auf die Architektur zu verstehen und zu bewerten. Dies legt die Grundlage für eine ganzheitliche und fundierte Bewertung und Priorisierung von Anforderungen.  
- **A-1-III**: Bewertung der erfassten Anforderungen an die föderale IT-Architektur auf Basis der Vorqualifizierung aus A-1-II sowie geltenden 
Architekturvorgaben. Priorisierung der bewerteten Anforderungen, um sicherzustellen, dass die Ressourcen im FIT-AM effizient eingesetzt und die wichtigsten Bedarfe priorisiert bearbeitet werden.

### Aufgabenbereich 2: Weiterentwicklung der föderalen Architektur
Der Aufgabenbereich "Weiterentwicklung der föderalen IT-Architektur" umfasst die folgenden Aufgaben:
- **A-2-I**: Initiale und strukturierte Erfassung und Aufbereitung der aktuellen Bebauung der föderalen IT-Architektur oder einzelner Teilbereiche derselben (IST-Zustand).
- **A-2-II**: Kontinuierliche Aktualisierung und Pflege der Dokumentation des Ist-Zustandes und der dieser zugrundeliegenden Methoden und Prozesse, um nachhaltig aktuelle und korrekte Darstellungen der sich verändernden IT-Architektur zu gewährleisten.
- **A-2-III**: Die Entwicklung der anzustrebenden föderalen SOLL-Architektur basierend auf identifizierten und bewerteten Anforderungen. Arbeitsergebnisse werden in Form von angemessenen Architektur-Artefakten dokumentiert und dargestellt (Architekturvisionen, Zielbilder, Zielarchitekturen, Bebauungspläne, etc.). Dies ist der operative Kern der planerischen Architekturarbeit und dient als wesentliche Grundlage für Entscheidungen des IT-Planungsrates zur Gestaltung der föderalen IT-Architektur.
- **A-2-IV**: Entwurf, Entwicklung, Erprobung und Evaluation von Lösungsansätzen und -architekturen. Wo angemessen, werden im Rahmen des FIT-AM hierfür eigenständig Proof-of-Concept (PoC) Implementationen zu vielversprechenden Lösungsansätzen entwickelt. Im Rahmen des FIT-AM werden so fortlaufend alternative, oder experimentelle Architektur- und Lösungsansätze, Technologien, Methoden und andere Innovationen mit potenziellem Mehrwert für die Weiterentwicklung der föderalen IT-Architektur identifiziert, bewertet und gefördert. Im Sinne der Nachhaltigkeit und Zukunftsfähigkeit der föderalen IT-Architektur umfasst dies auch die Beobachtung und Bewertung aktueller technischer, fachlicher und anderweitiger Trends und Entwicklungen.
- **A-2-V**: Definition von Zwischenzuständen der föderalen IT-Architektur im Laufe von Transitionsprozessen sowie die Entwicklung von Plänen und Maßnahmen zur 
schrittweisen Umsetzung von beschlossenen Soll-Zuständen der föderalen IT-Transitionsschritten, um sicherzustellen, dass die angestrebte Architektur erreicht wird und etwaige Abweichungen rechtzeitig erkannt und korrigiert werden können. Veränderungsprozesse werden im Rahmen des FIT-AM so operationalisiert und begleitet.

### Aufgabenbereich 3: Architekturvorgaben
Der Aufgabenbereich "Architekturvorgaben" umfasst die folgenden Aufgaben:
- **A-3-I**: Verbindlicher Beschluss von Definitionen und Abkürzungen architekturrelevanter Begrifflichkeiten für das Glossar des FIT-AM. 
- **A-3-II**: Identifikation und Erfassung von architekturrelevanten Fachbegriffen sowie Erarbeitung von Definitionen, Abkürzungen und Einordnungen für ein übergreifendes Glossar des FIT-AM. Regelmäßige Aktualisierung und Erweiterung des Glossars mittels eines strukturierten Redaktionsprozesses. Das Glossar des FIT-AM stellt eine gemeinsame und öffentlich zugängliche Wissensbasis und Nachschlagewerk dar. Es begünstigt die einheitliche Verwendung von Begrifflichkeiten, und damit semantische Interoperabilität und Diskussion über architekturrelevante Sachverhalte. 
- **A-3-III**: Entwicklung und Definition von Architekturvorgaben5 für die föderale IT-
Architektur (inkl. Architekturrichtlinien, Architekturprinzipien, Standards, etc.). Die Verbindlichkeit der Architekturvorgaben ergibt sich aus den jeweiligen
Beschlussdokumenten. Das FIT-AB definiert Vorgaben der föderalen IT-Architektur und beschließt diese verbindlich. Vorgaben und deren Änderungen außerhalb des durch den IT-Planungsrats gesteckten Rahmens legt es dem IT-PLR zur Beschlussfassung vor. Architekturvorgaben stellen das wesentliche Regelungsinstrument des FIT-AM dar und bilden die Grundlage für Architektur-Governance (vgl. A-4)
- **A-3-IV**: Regelmäßige Prüfung und Bewertung geltender Architekturvorgaben, insbesondere um sicherzustellen, dass diese den aktuellen Anforderungen entsprechen und bei Bedarf geändert oder fortgeschrieben werden.
- **A-3-V**: Entwicklung und Dokumentation von Referenzarchitekturen und -modellen, die insbesondere als architektonische Blaupausen für die Umsetzung von IT-gestützten Fähigkeiten der öffentlichen Verwaltung und die Ausgestaltung der entsprechenden IT-Lösungen dienen.
- **A-3-VI**: Kontinuierliche Pflege von Referenzarchitekturen und -modellen, beispielsweise durch Aktualisierungen und Anpassungen, um sicherzustellen, dass sie
sich verändernden technologischen Trends und Anforderungen entsprechen.

### Aufgabenbereich 4: Architektur-Governance
Der Aufgabenbereich "Architektur-Governance" umfasst die folgenden Aufgaben:
- **A-4-I**: Kontinuierliches Monitoring von föderalen Projekten, Standards, Produkten und Infrastrukturkomponenten auf Architekturrelevanz und Einhaltung von Regelungen zur föderalen IT-Architektur (beispielsweise verbindlichen Architekturvorgaben oder strategischen Zielzuständen der föderalen IT-Architektur). Werden Abweichungen identifiziert, werden im Rahmen des FIT-AM entsprechende Maßnahmen zur Behebung, Eskalation oder weiterführenden Evaluationen eingeleitet.
- **A-4-II**: Durchführung von dedizierten Architekturreviews für einzelne föderale Projekte, Standards, Produkte und Infrastrukturkomponenten. Deren Architekturentscheidungen und -artefakte werden im Hinblick auf ihre Konformität insbesondere mit den geltenden Architekturvorgaben und Standards evaluiert. Werden Abweichungen identifiziert, werden im Rahmen des FIT-AM entsprechende Maßnahmen zur Behebung, Eskalation oder weiterführenden Evaluationen eingeleitet. Das FIT-AB kann eine solche Prüfung durch einen Beschluss einseitig initiieren.

### Aufgabenbereich 5: Veröffentlichung und Kommunikation 
Der Aufgabenbereit "Veröffentlichung und Kommunikation" umfasst die folgenden Aufgaben:
- **A-5-I**: Aufbereitung und Veröffentlichung von Architekturinformationen in zweck- und zielgruppenorientierten, leicht zugänglichen Darstellungen und Kanälen. Regelmäßige Aktualisierung und Pflege der veröffentlichten Informationen.
- **A-5-II**: Schulung und Sensibilisierung von relevanten Zielgruppen für Sachverhalte der föderalen IT-Architektur und des Föderalen IT-Architekturmanagements. Hierfür werden geeignete Schulungs- und Kommunikationsmaßnahmen entwickelt und durchgeführt. Ein wesentliches Ziel ist es, alle Ebenen der Verwaltung für die Bedeutung und Auswirkungen der föderalen IT-Architektur und einschlägiger Architekturvorgaben auf ihre tägliche Arbeit und ihre Projekte zu sensibilisieren.
- **A-5-III**: Stakeholder Management im Wirkungsbereich des Föderalen IT-Architekturmanagements. Dies umfasst insbesondere die Identifikation relevanter Stakeholder sowie den Aufbau und die Pflege von guten Arbeitsbeziehungen zu zentralen Stakeholdern. Darüber hinaus wird ein regelmäßiger Austausch mitStakeholdern als Grundlage für Anforderungsmanagement- und Beteiligungsprozesse im FIT-AM etabliert; Änderungen in der föderalen IT-Architektur werden an die
betroffenen Stakeholder und Zielgruppen kommuniziert und klare und verständliche Informationen über die Gründe für die Änderungen und deren Auswirkungen auf die
betroffenen Parteien bereitgestellt.

### Aufgabenbereich 6: Beratungsleistungen 
Der Aufgabenbereich "Beratungsleistungen" umfasst die folgenden Aufgaben: 
- **A-6-I**: Architektonische Beratung der strategischen Entscheidungsebene, insbesondere des IT-Planungsrates und der Abteilungsleiterrunde, insbesondere bei Projektentscheidungen und Beschlüssen. Architektonische Handlungsoptionen werden klar und strukturiert aufgezeigt und bewertet, und so strategische 
Entscheidungen mit Auswirkung auf die föderale IT-Architektur unterstützt. 
- **A-6-II**: Bedarfs- und zielgruppengerechte Aufbereitung, Bereitstellung und Präsentation von deskriptiven und analytischen Architekturinformationen, um deren
effektive Nutzung und Anwendung auf architekturrelevante Fragestellungen zu ermöglichen. 
- **A-6-III**: Beratung und Unterstützung von Projekten, Standards, Produkten und föderalen Infrastrukturkomponenten zu architektonischen Fragestellungen. Inhaltlicher Gegenstand der Beratung kann die Einhaltung von Architekturvorgaben und Standards sein, beispielsweise in Form des Reviews von in Projekten geplanten Lösungsarchitekturen, aber auch unmittelbare Unterstützung bei der Gestaltung von Lösungsarchitekturen. Darüber hinaus kann Beratung zur Nutzung von Standards und Schnittstellen zur Integration mit der bestehenden Architektur und zur Anwendung von architektonischen Methoden und Verfahren erfolgen.

Formate und Umfang von Beratungsleistungen richten sich fallabhängig an den herrschenden Anforderungen und den verfügbaren Ressourcen des FIT-AM aus. Dabei ist es stets ein Ziel, auch den nachhaltigen Wissens- und Kapazitätsaufbau zu Fragen der IT-Architektur in den beratenen Organisationen und Projekten zu fördern.

### Aufgabenbereich 7: Methodik
Der Aufgabenbereich "Methodik" umfasst die folgenden Aufgaben:
- **A-7-I**: Entwicklung von methodischen Standards für das Föderale IT-Architekturmanagement (z.B. methodische Richt- und Leitlinien, methodische Rahmenwerke, Modellierungskonventionen und Metamodelle). Entwicklung und Bereitstellung von methodischen Werkzeugen, Vorlagen und Hilfsmitteln zur Unterstützung von Architekturarbeit im FIT-AM.
- **A-7-II**: Kontinuierliche Pflege und Analyse, von methodischen Standards und Werkzeugen, um sicherzustellen, dass sie den aktuellen Anforderungen und Entwicklungen entsprechen. Überprüfung und Wahrung von methodischen Standards und Werkzeugen in deren Anwendung in der föderalen Architekturarbeit.
- **A-7-III**: Auswahl, Beschaffung, Bereitstellung, Konfiguration und Betrieb von Softwarewerkzeugen für das Föderale IT-Architekturmanagement, insbesondere zur
Modellierung, Analyse, Dokumentation und Verwaltung der IT-Architektur sowie für die Kommunikation und Zusammenarbeit im FIT-AM. Schulung und Unterstützung der
Stakeholder bei der effektiven Nutzung der bereitgestellten Werkzeuge und Methoden.