---
titel: "Übersicht über Aufgaben und Arbeitsformate des FIT-AB"
---

# Übersicht Aufgaben und Arbeitsformate

In diesem Bereich finden Sie eine Übersicht über die Aufgaben im Handlungsfeld des FIT-AM, über die wesentlichen Arbeitsformate, mit denen die Aufgaben bearbeitet werden, sowie über die Arbeitsteilung und Regelzuständigkeiten zwischen FIT-AB und FITKO-AM.