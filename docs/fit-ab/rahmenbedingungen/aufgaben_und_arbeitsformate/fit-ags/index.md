---
titel: "Derzeit bestehende FIT-AGs"
---

# FIT-AGs

Arbeitsgruppen des Föderalen IT-Architekturmanagements (FIT-AGs) sind ergebnisorientierte, operativ leistungsfähige und zweckgebundene Projektgruppen zur Umsetzung der im FIT-AB beschlossenen Vorhaben. FIT-AGs ermöglichen Architekt:innen, Anforderungsträger:innen und Expert:innen aus Bund, Ländern und der FITKO die Kollaboration zu architekturrelevanten Fragestellungen im Kontext des Föderalen IT-Architekturmanagements. Zur Herstellung eines gemeinsamen Verständnis über Umfang und Inhalt der Arbeit von FIT-AGs erfolgt die Planung von FIT-AGs sowie die Kontrolle und Steuerung laufender FIT-AGs anhand formalisierter Prozesse durch das FIT-AB.