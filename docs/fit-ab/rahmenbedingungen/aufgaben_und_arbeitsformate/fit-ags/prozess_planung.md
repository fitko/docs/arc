---
title: "Prozess zur Planung von FIT-AGs"
---

import ProzessSvg from '/images/abbildungen-fit-ab/hlp1.svg'

# Prozess zur Planung von FIT-AGs

FIT-AGs durchlaufen vor dem Start der Umsetzung eines Vorhabens einen Planungsprozess. Der Planungsprozess lässt sich grob in zwei Phasen unterteilen.

In der ersten Phase wird die inhaltliche Planung des Vorhabens erarbeitet. Hier geht es im Wesentlichen um die Definition der Problemstellung, der zu erarbeitenden Ergebnisse und zu erzielenden Mehrwerte. Der entsprechende Steckbrief wird von den Initiatoren der FIT-AG erstellt, im FIT-AB abgestimmt und von diesem per Beschluss abgenommen.

In der zweiten Phase wird von der Federführung der FIT-AG eine **Umsetzungsplanung** erstellt und mit den Mitarbeitenden der FIT-AG abgestimmt. Hier werden in einem entsprechenden Steckbrief projektmanagementspezifische Eckdaten des Vorhabens definiert (wie z.B. Dauer der Bearbeitung und Meilensteine).

Während des Planungsprozesses durchlaufen die FIT-AGs je nach Planungsstand/ Reifegrad drei Status:

- „In Abstimmung“ (Entwurf)
- „Abgestimmt“ (Umsetzungsplanung läuft)
- „Ready“

Mit der aktiven Umsetzung eines Vorhabens können FIT-AGs erst beginnen, wenn der gesamte Planungsprozess durchlaufen ist (Status „Ready“). Die aktuellen Vorhaben des FIT-AB finden sich unter [Überblick über die FIT-AGs](../../../undertakings.md).  

Das FIT-AB-Office steuert und koordiniert den Planungsprozess in Abstimmung mit den Initiatoren von FIT-AGs.

Die unten stehende Abbildung ist eine High-Level-Sicht auf den Planungsprozess. Sie gibt eine vereinfachte/ reduzierte Darstellung der Prozessschritte wieder.

<ProzessSvg />
[Abbildung 2: High-Level-Prozess Planung von FIT-AGs]

Der detaillierte Prozessablauf mit visualisierten und verschriftlichten Prozessschritten kann [hier](https://nextcloud.fitko.de/f/1921349) heruntergeladen werden.


:::info
In der Darstellung sind Links eingebettet, auf die nur FIT-AB-Mitglieder Zugriff haben.
:::

