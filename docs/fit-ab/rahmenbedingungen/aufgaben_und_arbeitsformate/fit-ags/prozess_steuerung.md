---
titel: "Prozess zur Steuerung von FIT-AGs"
---
# Prozess zur Steuerung von FIT-AGs

import ProzessSvg2 from '/images/abbildungen-fit-ab/hlp2.svg'

Die Arbeit von FIT-AGs, die sich in der aktiven Umsetzung eines Vorhabens befinden, wird vom FIT-AB fortlaufend kontrolliert und gesteuert. Dies geschieht im Rahmen von Status-Updates, die dem FIT-AB einen Abgleich ermöglichen zwischen den Planungen (siehe [Prozess zur Planung von FIT-AGs](prozess_planung.md)) und dem tatsächlichen Bearbeitungsstand von laufenden Vorhaben. Der Status-Update ist ein Sitzungsformat, in dem Vertreterinnen/ Vertreter aus laufenden Vorhaben entlang einer Reporting-Vorlage dem FIT-AB zum Umsetzungsstand der jeweiligen Vorhaben berichten. Bei Abweichungen von den Planungen können je nach Sachlage unterschiedliche Maßnahmen ergriffen werden, um nachzusteuern.

Die untenstehende Abbildung ist eine High-Level-Sicht auf den Prozess für Status-Updates. Sie gibt eine vereinfachte/ reduzierte Darstellung der Prozessschritte wieder. 

<ProzessSvg2 />
[Abbildung 3: High-Level-Prozess Status-Updates]


Der detaillierte Prozessablauf mit visualisierten und verschriftlichten Prozessschritten kann [hier](https://nextcloud.fitko.de/f/1921431) heruntergeladen werden.

:::info
In der Darstellung sind Links eingebettet, auf die nur FIT-AB-Mitglieder Zugriff haben. 
:::
