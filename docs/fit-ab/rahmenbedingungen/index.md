---
titel: "Rahmenbedingungen"
---

# Überblick Rahmenbedingungen

Auf den folgenden Unterseiten werden die wesentlichen fachlichen, organisatorischen und prozessualen Arbeitsgrundlagen des FIT-AB zusammengefasst. 

In [**Gründung und Handlungsrahmen**](./handlungsrahmen.md)  finden Sie einen Überblick über organisatorische Eckdaten und fachlich-konzeptuelle Grundlagen zum Gegenstands- und Zuständigkeitsbereich des FIT-AB und eine Einordnung des FIT-AB in die Domäne des Föderalen IT-Architekturmanagements (FIT-AM).

Die Unterseite [Architekturrelevanz](./architectural-relevance.md) definiert den Begriff „Architekturrelevanz“, wie er im Kontext der Sitzungsvorbereitung von architekturrelevanten Beschlüssen des IT-Planungsrates verwendet wird.

In [**Zusammensetzung**](./zusammensetzung.md) wird die aktuelle Struktur (Vorsitz, Mitglieder, Geschäftsstelle) dargestellt.

Es folgt eine Übersicht über die [**Aufgaben des FIT-AB**](./aufgaben_und_arbeitsformate/aufgaben_fit-am.md) im Kontext der Disziplin des Föderalen IT-Architekturmanagements (FIT-AM), den [**Arbeitsformaten**](./aufgaben_und_arbeitsformate/arbeitsformate_fit-am.md), mit denen diese Aufgaben bearbeitet werden sowie der Aufgabenteilung zwischen dem FIT-AB und dem Architekturmanagement der FITKO (FITKO-AM). In diesem Zusammenhang werden die Arbeitsgruppen des [**Föderalen IT-Architekturmanagements**](./aufgaben_und_arbeitsformate/fit-ags/index.md) (FIT-AGs) als wesentliches Arbeitsformat im Föderalen IT-Architekturmanagement beschrieben, die Prozesse zur [**Planung**](./aufgaben_und_arbeitsformate/fit-ags/prozess_planung.md) und [**Steuerung**](./aufgaben_und_arbeitsformate/fit-ags/prozess_steuerung.md) von FIT-AGs werden im Detail dargestellt. 


:::info 
An einigen Stellen führen Links zu den Arbeitswerkzeugen des FIT-AB (z.B. Nextcloud), auf die nur FIT-AB-Mitglieder Zugriff haben. Diese Links sind entsprechend gekennzeichnet.
:::
