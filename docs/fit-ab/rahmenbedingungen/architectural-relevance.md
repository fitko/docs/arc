---
title: "Definition: „Architekturrelevanz“"
slug: /fit-ab/architectural-relevance
---

# Definition des Begriffs „Architekturrelevanz“

Um das vom IT-Planungsrat erteilte Mandat zur Dokumentation, Analyse, Planung und Umsetzung der föderalen IT-Architektur zu erfüllen, ist eine enge Zusammenarbeit zwischen dem FIT-AB und allen Prozessen und Stakeholdern erforderlich, deren Wirken die föderale IT-Architektur beeinflusst. 

:::warning Frühzeitige Einbindung des Föderalen IT-Architekturboards

Entscheidungsvorlagen des IT-Planungsrates mit Auswirkungen auf die föderale IT-Architektur erfordern eine Einbindung des Föderalen IT-Architkturboards **bis eine Woche vor Sitzung der AL-Vorbesprechung**.
Um bewerten zu können, ob Entscheidungsvorlagen diese Architekturrelevanz aufweisen, dient die folgende Definition des Begriffs „Architekturrelevanz“.

Bei architekturrelevanten Beschlussvorlagen wird eine **Abstimmung mit dem Föderalen Architekturboard bereits vor Einreichung des Beschluss-Steckbriefs** in den IT-Planungsrat **dringend empfohlen**.

:::

## Architekturrelevanz von Entscheidungsvorlagen des IT-Planungsrates
Entscheidungsvorlagen für den IT-Planungsrat besitzen Architekturrelevanz für die föderale IT-Architektur und erfordern eine Einbindung des föderalen IT-Architekturboards, wenn durch die angestrebte Entscheidung eine oder mehrere der folgenden Kriterien erfüllt werden.
- Durch die Entscheidung werden Projekte initiiert oder adressiert, die **neue IT-Lösungen** schaffen oder **bestehende IT-Lösungen verändern**, die **über mehrere oder alle Ressorts / Fachbereiche hinweg** (bspw. Umwelt, Finanzen und Wirtschaft) in einer Vielzahl von Prozessen und Leistungsangeboten der Verwaltung eingebunden werden.
  - Solche IT-Lösungen können **zentrale Funktionen bspw. für Endnutzende** (bspw. Antragssteller:innen, Behördenmitarbeiter:innen oder Lieferanten) oder **für behördliche Fachsysteme** bereitstellen oder auch die jeweiligen Behörden dabei unterstützen, neue oder angepasste Fachsysteme für die Digitalisierung von Prozesse zu entwickeln.
  - Die Architekturrelevanz ist **unabhängig davon gegeben, ob** diese IT-Lösungen in Bund, Ländern und Kommunen **verpflichtend einzusetzen** sind, diese IT-Lösungen die bestehenden dezentralen Lösungen ersetzen oder zunächst nur optionale Zusatzangebote darstellen. Es ist zudem **nicht relevant, ob diese IT-Lösungen als ein Produkt des IT-Planungsrats bereitstellt werden** oder zum Beispiel von Bund oder Ländern für die restliche föderale Verwaltung bereitgestellt werden.
  - **Beispiele** für die hier genannten IT-Lösungen können **Basiskomponenten** wie z.B.  ein **Nutzendenkonto** oder eine **Signaturkomponente** sein, aber auch **Entwicklungswerkzeuge** (SDKs), **Low-Code-Plattformen** oder föderale gemeinsame genutzte **Portalanwendungen** wie Rechnungseingangsportale oder Verwaltungsportale.
- Durch die Entscheidung werden **Standards geschaffen oder verändert**, die über mehrere oder alle Ressorts / Fachbereiche die technische Kommunikation zwischen Fachsystemen entlang von föderalen Prozessketten (bspw. zwischen Bund und Ländern oder zwischen Kommunen oder Landkreisen) verändern oder zentrale Geschäfts- bzw. Datenobjekte standardisieren, die in einer großen Anzahl von Prozessen oder IT-Lösungen genutzt werden.
- Durch die Entscheidung werden **verpflichtende Vorgaben beschlossen**, die eine **erhebliche technische Anpassung von IT-Planungsratsprodukten** oder von **IT-Systemen von Bund, Ländern, Kommunen oder Verwaltungspartnern** zur Folge haben.