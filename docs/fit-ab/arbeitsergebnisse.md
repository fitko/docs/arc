---
titel: "Arbeitsergebnisse des FIT-AB"
---
import useBaseUrl from '@docusaurus/useBaseUrl';

# Arbeitsergebnisse des FIT-AB

Auf dieser Seite werden Arbeitsergebnisse des FIT-AB veröffentlicht.
Die regelmäßig veröffentlichten Arbeitsergebnisse umfassen sowohl Liefergegenstände aus den vom FIT-AB formell beschlossenen [Vorhaben, die in Form von Arbeitsgruppen des Föderalen IT-Architekturmanagements (FIT-AGs) bearbeitet werden](./undertakings.md), sowie sonstige Arbeitsergebnisse des FIT-AB mit Ausnahme von Artefakten, die der unmittelbaren Entscheidungsvorbereitung in einem dem FIT-AB übergeordneten Gremium dienen.

**Übersicht**

- [Arbeitsergebnisse aus den Vorhaben des FIT-AB](#vorhaben)
  - [Föderale IT-Architekturrichtlinien](#föderale-it-architekturrichtlinien-architekturrichtlinien)
  - [Zielarchitektur Postfach- und Kommunikationslösungen](#zapuk)
  - [Pilotierung Messenger-Kommunikationskanal für die G2C-Kommunikation](#g2c-kommunikation)
  - [EfA-Mindestanforderungen](#efa-mindestanforderungen)
  - [Dokumentation der föderalen IT-Landschaft](#föderale-it-landschaft)
  - [Orientierung für die Parametrisierung](#efa-parametrisierung)
  - [Fähigkeitenlandkarte](#fähigkeitenlandkarte)
- [Projekte, die durch das föderale Architekturboard begleitet werden](#projekte)
  - [OZG-Hub](#ozg-hub)
  - [Bezahldienstschnittstelle](#bezahldienstschnittstelle)
  - [Deutsche Verwaltungscloud Strategie (DVS)](#dvs)
  - [FIT-Connect](#fit-connect)

## Arbeitsergebnisse aus den Vorhaben des FIT-AB {#vorhaben}

### Föderale IT-Architekturrichtlinien {#architekturrichtlinien}
Das Architekturboard definiert die föderalen IT-Architekturrichtlinien und entwickelt diese weiter. Die Richtlinien gelten für alle laufenden und neuen Projekte und Vorhaben, die Einfluss auf die föderale IT-Landschaft haben.

Darüber hinaus richten sie sich an private und öffentliche IT-Dienstleister:innen sowie Verantwortliche in Bund und Ländern. Ferner sollen die Richtlinien den IT-Planungsrat dabei unterstützen, die IT-Architektur von Bund und Ländern aktiv zu steuern. Sie dienen als Basis für weitere, domänenspezifische Richtlinien, die sukzessive erarbeitet werden.

- [Zu den föderalen IT-Architekturrichtlinien](https://docs.fitko.de/fit/policies/foederale-it-architekturrichtlinien/)

### Zielarchitektur Postfach- und Kommunikationslösungen {#zapuk}

Alle Arbeits- und Zwischenergebnisse aus dem Vorhaben werden fortlaufend über ein [öffentlich zugängliches openCode-Repository](https://gitlab.opencode.de/it-planungsrat/fit-ab/zapuk) veröffentlicht.
Auf der openCode-Plattform erfolgt im März und April 2025 zudem ein öffentlicher Kommentierungs- und Konsultationsprozess zu den Arbeitsständen.

- [Zielarchitektur Postfach- und Kommunikationslösungen auf openCode](https://gitlab.opencode.de/it-planungsrat/fit-ab/zapuk)

### Pilotierung Messenger-Kommunikationskanal für die G2C-Kommunikation {#g2c-kommunikation}

Alle Arbeits- und Zwischenergebnisse aus dem Vorhaben werden fortlaufend über ein [öffentlich zugängliches openCode-Repository](https://gitlab.opencode.de/fitko/g2x-matrix-pilot) veröffentlicht.
Dies soll einen offenen Beteiligungsprozess im Sinne eines Open Development Process schon vor Projektstart ermöglichen.
Das Vorhaben freut sich über weiteres Feedback und Anregungen über den Issue-Tracker des Projekts auf der openCode-Plattform!

- [Pilotierung Messenger-Kommunikationskanal für die G2C-Kommunikation auf openCode](https://gitlab.opencode.de/fitko/g2x-matrix-pilot)

### EfA-Mindestanforderungen

Die EfA-Mindestanforderungen definieren Vorgaben zur Umsetzung von Online-Services nach dem „Einer für Alle“-Prinzip. Die EfA-Mindestanforderungen sind ein Nachnutzungsstandard im Sinne des [§ 6 Nr. 3d des Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes](https://docs.fitko.de/fit/policies/verwaltungsabkommen-ozg/#p6).

Gemäß Beschluss der Abteilungsleiterrunde vom 8.12.2020 [leider nicht-öffentlich] werden die EfA-Mindestanforderungen durch das Architekturboard des IT-Planungsrats fortgeschrieben. Die Beschlussfassung über Fortschreibungen erfolgt durch die Abteilungsleiterrunde.

- [Zu den EfA-Mindestanforderungen](https://docs.fitko.de/fit/policies/efa-mindestanforderungen/)

### Dokumentation der föderalen IT-Landschaft {#föderale-it-landschaft}

Aufbauend auf den föderalen IT-Architekturrichtlinien hat das föderale IT-Architekturboard die aktuelle IT-Landschaft auf der föderalen Ebene beschrieben.
Die IT-Landschaft wurde zum einen als Poster aufbereitet, zum anderen in einem umfangreichen Begleitdokument beschrieben.

Die Dokumentation der IT-Landschaft ist, neben den Architekturrichtlinien, ein weiterer wichtiger Baustein auf dem Weg zur Etablierung eines föderalen IT-Architekturmanagements.

- <a href={useBaseUrl("/assets/pdf/fit-ab_dokumente/Dokumentation_IT-Landschaft_1.0.pdf")} target="_blank" rel="noopener noreferrer">Dokumentation der föderalen IT-Landschaft herunterladen (Version 1.0) (2 MB, pdf)</a>
- <a href={useBaseUrl("/assets/pdf/fit-ab_dokumente/20220610_Foederale_IT-Landschaft_Poster_v1.01.pdf")} target="_blank" rel="noopener noreferrer">Poster der föderalen IT-Landschaft herunterladen (Version 1.01) (1 MB, pdf)</a>

### Orientierung für die Parametrisierung {#efa-parametrisierung}

Um die bundesweite Nachnutzung von Online-Diensten zu ermöglichen, die nach dem "Einer für Alle"-Prinzip (EfA) erstellt wurden, gibt das Architekturboard eine Orientierung für deren Parametrisierung.
Durch die Verwendung von Variablen (Parametern) soll die Flexibilität der EfA-Online-Dienste sichergestellt werden.

Dies soll standardisiert und gleichartig für möglichst viele Dienste der öffentlichen Verwaltung erfolgen.
Die Orientierung gibt Hinweise, wie die Parametrisierung von Diensten erfolgen soll, wie die Parameter definiert, gepflegt und abgerufen werden und welche Parameter verwendet werden sollen.
Sie richtet sich an alle an der OZG-Umsetzung beteiligte Personen und Institutionen.

Die Orientierung für die Parametrisierung wurde als Dokument in der Version 0.80 bereitgestellt.
Die Veröffentlichung in diesem frühen Stadium erfolgt, um Anmerkungen und Anregungen der interessierten Fachöffentlichkeit zu diskutieren.

- <a href={useBaseUrl("/assets/pdf/fit-ab_dokumente/20230227_FITKO-Orientierung_Parametrisierung_v0.80.pdf")} target="_blank" rel="noopener noreferrer">Orientierung Parametrisierung (Version0.80) (Version 1.01) (1 MB, pdf)</a>
- [Dokumentation zur EfA-Parametrisierung auf dem Föderalen Entwicklungsportal](https://docs.fitko.de/efa-parametrisierung/)

### Fähigkeitenlandkarte

Die föderale IT-Fähigkeitenlandkarte ist eine strukturierte Zusammenstellung von Fähig­keiten, die vom föderalen IT-Architekturboard als notwendig für die Erarbeitung und Umsetzung einer föderalen IT-Strategie – und damit für die Weiterentwicklung der föde­ralen IT-Landschaft – erachtet werden. Die Fähigkeiten sind hierbei als grundsätzlich er­forderliche Vermögen der Gesamtheit der daran beteiligten Akteure zu verstehen, losge­löst von ihrer konkreten Implementierung. Die Beschreibung der einzelnen Fähigkeiten er­folgte im Hinblick auf die besonderen Herausforderungen des föderalen Kontextes. Anhand der gegebenen Beschreibungen kann eine Analyse vorgenommen werden, inwie­fern die genannten Fähigkeiten bereits in der Gruppe der beteiligten Organisationen (fö­derale IT-Gruppe) vorhanden sind und für welche Fähigkeiten ein Entwicklungsbedarf be­steht. Hierfür und für ein entsprechendes weiteres Vorgehen wird im Dokument ein grundsätzlicher Verfahrensvorschlag aufgeführt.

- <a href={useBaseUrl("/assets/pdf/fit-ab_dokumente/Fähigkeitenlandkarte_v1.0.pdf")} target="_blank" rel="noopener noreferrer">Fähigkeitenlandkarte zur Entwicklung und planvollen Umsetzung einer föderalen IT-Strategie Deutschlands (Version 1.0) (1 MB, pdf)</a>

:::info In Arbeit
Arbeitsergebnisse aus weiteren Vorhaben werden an dieser Stelle noch ergänzt.
:::

## Projekte, die durch das föderale Architekturboard begleitet werden {#projekte}

### OZG-Hub

Der OZG-Hub ist ein integratives System, das ermöglichen soll, Online-Dienste (Antragsverfahren) schnell und standardisiert zu erstellen und online verfügbar zu machen. Der OZG-Hub bietet dafür alle erforderlichen Werkzeuge in einer Umgebung.
Die Anbindung an alle relevanten Schnittstellen ist möglich und erfolgt über eine zentrale Komponente.

### Bezahldienstschnittstelle

Wenn Bürger:innen online einen Antrag an die Verwaltung stellen, können Gebühren anfallen. Diese Gebühren könnten idealerweise direkt online bezahlt werden, wie es im Internet üblich ist. Damit sich nicht jeder Online-Dienst um Schnittstellen zu Bezahldiensten (z. B. Paypal) kümmern muss, soll eine zentrale Schnittstelle entwickelt werden, die von allen anderen nachgenutzt werden kann.

- [Standard XBezahldienste auf der Informationsplattform für Föderale IT-Standards](https://docs.fitko.de/fit-standards/xbezahldienste/)

### Deutsche Verwaltungscloud Strategie (DVS) {#dvs}

Der IT-Planungsrat hat in seiner 32. Sitzung die Arbeitsgruppe „Cloud Computing und Digitale Souveränität“ beschlossen.
In seiner 33. Sitzung hat er mit [Beschluss 2020/54](https://www.it-planungsrat.de/beschluss/beschluss-2020-54) das Konzept der „Deutschen Verwaltungscloud Strategie (DVS)“ verabschiedet.
Das Vorhaben erarbeitet u. a. offene Standards und Schnittstellen, um kritische Abhängigkeiten aufzulösen als Beitrag zur Stärkung der digitalen Souveränität.

### Projekt FIT-Connect

Ein zentraler Baustein der föderalen IT-Architektur ist FIT-Connect.
Im Auftrag des IT-Planungsrats erarbeitete die FITKO mit diesem Projekt eine Plattform zur Vernetzung der an der OZG-Leistungserbringung beteiligten Akteur:innen.
Das umfasst den Aufbau einer einheitlichen Infrastruktur, Möglichkeiten für unkomplizierten Datenaustausch und die Bereitstellung einer Entwicklungsumgebung.
Aus dem Projekt FIT-Connect gingen mit [Beschluss 2022/32](https://www.it-planungsrat.de/beschluss/beschluss-2022-32) zwei Produkte des IT-Planungsrates hervor:

- [Produkt FIT-Connect](https://www.fitko.de/produktmanagement/fit-connect)
- [Produkt Föderales Entwicklungsportal](https://www.fitko.de/produktmanagement/foederales-entwicklungsportal)
