---
title: "Überblick über die FIT-AGs"
slug: /fit-ab/unterdakings
---

# Überblick über die FIT-AGs

Vorhaben, die in Form von Arbeitsgruppen des Föderalen IT-Architekturmanagements (FIT-AGs) bearbeitet werden, durchlaufen von der initialen Idee bis zur Umsetzung verschiedene Phasen eines definierten Planungs- und Abstimmungsprozesses. Auf einem Kanban-Board der openCode-Plattform wird eine Übersicht darüber geführt, welche Vorhaben sich in welchem Status befinden.

| [Zur Übersicht der Vorhaben auf der openCode-Plattform](https://gitlab.opencode.de/it-planungsrat/fit-ab/vorhaben/-/boards/) |
| ----- |

Auf dem Board befinden sich nicht nur Vorhaben, die aktuell in der Umsetzung sind, sondern auch Vorhaben, die sich noch im Abstimmungsprozess befinden. Die einzelnen Tickets zu den jeweiligen Vorhaben beinhalten die wesentlichen inhaltlichen und organisatorischen Eckdaten in Form eines Steckbriefes. 
Darüber hinaus sind in den Tickets die Unterlagen aus den Status-Updates verlinkt (siehe [„Sitzungsformate“](./sitzungen/index.md) bzw. [„Prozess zur Steuerung von FIT-AGs“](./rahmenbedingungen/aufgaben_und_arbeitsformate/fit-ags/prozess_steuerung.md), sodass zu jedem Vorhaben der aktuelle Arbeitsstand nachvollzogen werden kann.