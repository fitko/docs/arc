---
titel: "In Kontakt treten"
---

# Kontakt
IT-Architekturmanagement im föderalen Kontext soll und muss offen sein für den bedarfsorientierten Austausch mit Wissensträgern aus Wissenschaft, Wirtschaft, Verwaltung und Zivilgesellschaft.

Wenn Sie mit dem FIT-AB zu fachlichen Themen des föderalen IT-Architekturmanagements in den Austausch treten möchten, können Sie sich an das FIT-AB Office wenden.

| [Kontakt aufnehmen mit dem FIT-AB-Office](mailto:architekturboard-office@lists.fitko.net) |
| ----- |

Insbesondere bei architekturrelevanten Beschlussvorlagen für den IT-Planungsrat wird eine Abstimmung mit dem Föderalen Architekturboard bereits vor Einreichung des Beschluss-Steckbriefs in den IT-Planungsrat dringend empfohlen (siehe Definition des Begriffs [„Architekturrelevanz“](./rahmenbedingungen/architectural-relevance.md)). Auch für eine Koordination der Abstimmung oder Fragen zur Architekturrelevanz können Sie sich an das FIT-AB-Office wenden.