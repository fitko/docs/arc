---
sidebar_class_name: hidden
slug: /
---

# Dokumentation zur Föderalen IT

Diese Online-Dokumentation wird entwickelt und betrieben von der Föderalen IT-Kooperation (FITKO).  
Sie vermittelt einen Überblick über verschiedene Themen im Kontext der Föderalen IT. Sie unterstützt bei der Entwicklung von IT-Systemen von Bund, Ländern und Kommunen.

Die Online-Dokumentation umfasst derzeit die folgenden Themenbereiche:

<div className="container fit-pages">
  <div className="row">
    <div className="panel-wrap col col--4 arc">
      <a href="policies/">
        <div className="panel">
          <div className="panel__text">
            <h4>Vorgaben/&#8203;Richtlinien zur Föderalen IT</h4>
            <p>Gesetze, IT-Planungsrats&shy;beschüsse und Leitlinien im Umfeld der Föderalen IT.</p>
          </div>
        </div>
      </a>
    </div>
    <div className="panel-wrap col col--4 arc">
      <a href="fit-ab/">
        <div className="panel">
          <div className="panel__text">
            <h4>Föderales IT-Architektur&shy;board</h4>
            <p>Begriffsdefinitionen, Vorgehens&shy;weisen und Vorhaben.</p>
          </div>
        </div>
      </a>
    </div>
    <div className="panel-wrap col col--4 stdb">
      <a href="fit-sb/">
        <div className="panel">
          <div className="panel__text">
            <h4>Föderales IT-Standardisierungs&shy;board</h4>
            <p>Dokumentation der defi&shy;nierten Prozesse und ver&shy;wendeten Werkzeuge.</p>
          </div>
        </div>
      </a>
    </div>
  </div>
</div>

:::info 🏗️
Diese Dokumentation befindet sich derzeit noch im Aufbau.

Wir freuen uns immer über Feedback im [dafür eingerichteten öffentlichen Issue-Tracker](https://docs.fitko.de/feedback).
Wir schätzen auch kleinere Ergänzungen und Korrekturen - sehr gerne auch in Form von Merge-Requests im [zugrundeliegenden Git-Repository auf der openCode-Plattform](https://gitlab.opencode.de/fitko/docs/arc)!
Bitte informiere uns bei größeren Änderungen vorab, um Doppelarbeiten zu vermeiden.
:::
