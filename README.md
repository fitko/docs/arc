# Dokumentation zur Föderalen IT-Architektur

Diese Docusaurus-Site enthält derzeit die Online-Dokumentation für folgende Themenbereiche:

- **Vorgaben/​Richtlinien zur Föderalen IT**  
  Gesetze, IT-Planungsrats­beschüsse und Leitlinien im Umfeld der Föderalen IT.  
  Verzeichnis der zugehörigen Dateien: `/docs/policies`
- **Föderales IT-Architektur­board**  
  Begriffsdefinitionen, Vorgehens­weisen und Vorhaben.  
  Verzeichnis der zugehörigen Dateien: `/docs/fit-ab`
- **Föderales IT-Standardisierungs­board**  
  Dokumentation der defi­nierten Prozesse und ver­wendeten Werkzeuge.  
  Verzeichnis der zugehörigen Dateien: ` /docs/fit-sb`

## Schnellstart

1. Build der Docusaurus-Seite erstellen:
```bash
$ yarn install
$ yarn build
```

2. Docker-Container starten:
```bash
$ docker-compose up --build
```

Die Seite ist dann unter `http://localhost:8090` erreichbar.

## Sidebar-Dateien

Um potenzielle Fehlerquellen zu minimieren und Merge-Konflikte zu vermeiden, wurde die sonst üblicherweise verwendete Datei `sidebar.js` ebenfalls gesplittet.

Die Sidebar-Dateien befinden sich im Verzeichnis `src/sidebars` und sind entsprechend der drei Bereiche benannt (`policies.js`, `FIT-AB.js`, `FIT-SB.js`).

## Lokale Apache-Testumgebung für Docusaurus

Diese Docker-Konfiguration ermöglicht das lokale Testen von Docusaurus-Builds inklusive .htaccess-Regeln.

### Voraussetzungen

- Node.js
- Optional: Yarn Package manager (npm kann auch verwendet werden)
- Docker & Docker Compose (beides in Docker Desktop enthalten)
- Docusaurus Build im `build/` Verzeichnis
- .htaccess-Datei im `static/` Verzeichnis

### Projektstruktur

```
.
├── build/             # Docusaurus Build-Output
├── static/
│   └── .htaccess      # Apache-Konfiguration
├── docker-compose.yml
├── Dockerfile
└── my-httpd.conf      # Apache-Hauptkonfiguration
```

### Entwicklung

- Änderungen an der `.htaccess` und am am Build-Output werden durch das Volume-Mounting sofort wirksam

### Steuerung

```bash
# Container im Vordergrund starten
docker-compose up --build

# Container im Hintergrund starten
docker-compose up -d --build

# Container stoppen
docker-compose down
```
